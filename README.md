# ![Logo](/src/main/icons/linux/128.png) EHR Data Profiling Tool   

This application is a tool designed for profiling and visualizing descriptive statistics of large datasets, specifically Electronic Health Records. 
Developed in Python using Pandas and PyQt5 for my dissertation project at The University of Leeds.

[![](http://img.youtube.com/vi/lbMUcmVF58o/0.jpg)](http://www.youtube.com/watch?v=lbMUcmVF58o "")

## Installation

To install on Linux (Debian / Ubuntu)

[Download Linux .deb Installer](/Installer/EHR-DATA-PROFILING-TOOL.deb)

### Cloning

To get the software on your computer without using an installer follow these instructions.
Clone to chosen location.
```
git clone git@gitlab.com:sc16kg/ehr-data-profiling-tool.git
```
Create a virtual environment.
```
python3 -m venv env
```
Activate the virtual environment.
```
# On Linux
source env/bin/activate
```
Navigate to the requirements folder and install the requirements
```
cd requirements
pip install -r base.txt
```
Navigate to the base project directory and run.
```
cd ..
fbs run
```
Alternatively freeze the application
```
fbs freeze
```
And then run
```
./target/EHR-DATA-PROFILING-TOOL/EHR-DATA-PROFILING-TOOL
```

## User Interface

![Application Main Window](/Screenshots/Readme_Screenshots/Table.png)*Application With Data Loaded*

![Filter Dialog](/Screenshots/Readme_Screenshots/Filter.png)*Filter Dialog*

![Combine Dialog](/Screenshots/Readme_Screenshots/Combine.png)*Combine Dialog*

![Describe Dialog](/Screenshots/Readme_Screenshots/Describe.png)*Describe Dialog*

![Sample Dialog](/Screenshots/Readme_Screenshots/Sample.png)*Sample Dialog*

![Visualization Dialog](/Screenshots/Readme_Screenshots/VisualizationDialog.png)*Visualization Dialog*

![Printed Visualization](/Screenshots/Readme_Screenshots/EHRVisualization.png)*Printed Visualization*

![Help Dialog](/Screenshots/Readme_Screenshots/Help.png)*Help Dialog*
