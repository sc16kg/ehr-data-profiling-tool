import pyautogui

def test_open(terminal):
    pyautogui.click(x=1500, y=1159)
    pyautogui.click(terminal)
    ans = input("Did Open Dialog Open?")
    if ans == 'y':
        return 0
    else:
        return 1

def test_menu(terminal):
    pyautogui.click(x=1236, y=873)
    pyautogui.click(terminal)
    ans = input("Did Menu Open?")
    if ans == 'y':
        return 0
    else:
        return 1

def test_close_tab(terminal):
    pyautogui.click(x=1244, y=829)
    pyautogui.click(terminal)
    ans = input("Did Tab Close and application return to start menu?")
    if ans == 'y':
        return 0
    else:
        return 1

def test_open_describe(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=900)
    # pyautogui.click(x=1277, y=1339)
    pyautogui.click(terminal)
    ans = input("Did Describe Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1960, y=995)
        return 0
    else:
        return 1

def test_open_visualization(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=930)
    pyautogui.click(terminal)
    ans = input("Did Visualization Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1964, y=1089)
        return 0
    else:
        return 1

def test_open_compare(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=960)
    pyautogui.click(terminal)
    ans = input("Did Compare Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1591, y=1146)
        return 0
    else:
        return 1

def test_open_filter(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=990)
    pyautogui.click(terminal)
    ans = input("Did Filter Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1595, y=1102)
        return 0
    else:
        return 1

def test_open_trim(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=1020)
    pyautogui.click(terminal)
    ans = input("Did trim Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1590, y=1137)
        return 0
    else:
        return 1

def test_open_sample(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=1050)
    pyautogui.click(terminal)
    ans = input("Did sample Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1512, y=1218)
        return 0
    else:
        return 1

def test_open_formatting(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=1080)
    pyautogui.click(terminal)
    ans = input("Did formatting Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1474, y=1272)
        return 0
    else:
        return 1

def test_open_convert(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=1110)
    pyautogui.click(terminal)
    ans = input("Did convert Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1611, y=1072)
        return 0
    else:
        return 1

def test_open_rename(terminal):
    pyautogui.click(x=1260, y=870)
    pyautogui.click(x=1260, y=1140)
    pyautogui.click(terminal)
    ans = input("Did rename Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1437, y=1320)
        return 0
    else:
        return 1

def test_open_help(terminal):
    pyautogui.click(x=1226, y=791)
    pyautogui.click(terminal)
    ans = input("Did Help Dialog Open?")
    if ans == 'y':
        pyautogui.click(x=1760, y=1044)
        return 0
    else:
        return 1

def test_quit(terminal):
    pyautogui.click(x=1278, y=791)
    pyautogui.click(terminal)
    ans = input("Did the application close?")
    if ans == 'y':
        return 0
    else:
        return 1

if __name__ == "__main__":
    step = input("Type anything to record terminal location.")
    pos = pyautogui.position()
    successful = 0
    failed = 0
    tests = [test_open_describe,
             test_open_visualization,
             test_open_trim,
             test_open_sample,
             test_open_compare,
             test_open_convert,
             test_open_filter,
             test_open_formatting,
             test_open_rename,
             test_open_help,
             test_close_tab,
             test_quit]
    for test in tests:
        result = test(pos)
        if result == 0:
            successful += 1
        else:
            failed += 1
    print("======== TESTS COMPLETE =======")
    print("{percent}% Success".format(percent=(successful/(successful+failed)*100)))
    print("Successful: {s}".format(s=successful))
    print("Failed: {f}".format(f=failed))



