import unittest

from math import ceil, floor
import pandas as pd
from pandas._testing import assert_frame_equal, assert_series_equal
import numpy as np
from random import sample
from xlrd import XLRDError


class TestOpenFile(unittest.TestCase):
    def open_file(self, filepath):
        if len(filepath) > 0:
            if filepath.split(".")[1].upper() == "CSV":
                name = filepath.split("/")
                name = name[len(name) - 1]
                name = name.split(".")[0]
                try:
                    df = pd.read_csv(filepath)
                    if not df.empty:
                        return 0
                    else:
                        return 1
                except Exception as e:
                    return 1
            if filepath.split(".")[1].upper() == "XLSX":
                try:
                    xlsx = pd.ExcelFile(filepath)
                    df = pd.read_excel(xlsx, sheet_name=None)
                    for key in df:
                        if not df[key].empty:
                            return 0
                        else:
                            return 1
                except XLRDError:
                    return 1
            else:
                return 1
        else:
            return 1

    def test_excel(self):
        result = self.open_file("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.xlsx")
        assert result == 0

    def test_csv(self):
        result = self.open_file("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/DTypes.csv")
        assert result == 0

    def test_empty_xlsx(self):
        result = self.open_file("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/empty.xlsx")
        assert result == 1

    def test_empty_csv(self):
        result = self.open_file("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/empty.csv")
        assert result == 1

    def test_open_illegal_file(self):
        result = self.open_file("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/test.png")
        assert result == 1

    def test_no_file(self):
        result = self.open_file("")
        assert result == 1


class TestSaveTab(unittest.TestCase):
    def save_tab(self, filepath, number_of_tabs):
        number_of_tabs = number_of_tabs
        current_tab = 0
        if number_of_tabs > 0:
            if len(filepath) > 0:
                if ".xlsx" in filepath:
                    return 0
                elif ".csv" in filepath:
                    return 0
                elif "." in filepath:
                    return 1
                else:
                    return 0
            else:
                return 1
        else:
            return 1

    def test_save_excel(self):
        result = self.save_tab("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.xlsx", 1)
        assert result == 0

    def test_save_csv(self):
        result = self.save_tab("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/DTypes.csv", 1)
        assert result == 0

    def test_save_no_type(self):
        result = self.save_tab("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/testnotype", 1)
        assert result == 0

    def test_save_wrong_type(self):
        result = self.save_tab("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/test.png", 1)
        assert result == 1

    def test_no_filename(self):
        result = self.save_tab("", 1)
        assert result == 1

    def test_no_tabs(self):
        result = self.save_tab("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.xlsx", 0)
        assert result == 1


class TestSaveAll(unittest.TestCase):
    def save_all(self, filepath, number_of_tabs):
        number_of_tabs = number_of_tabs
        if number_of_tabs > 0:
            if len(filepath) > 0:
                try:
                    if ".xlsx" not in filepath:
                        filepath = "{name}.xlsx".format(name=filepath)
                    elif ".xlsx" in filepath:
                        pass
                    elif "." in filepath:
                        raise TypeError
                    return filepath
                except TypeError:
                    return 1
            else:
                return 1
        else:
            return 1

    def test_save_excel(self):
        result = self.save_all("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.xlsx", 1)
        assert result == "/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.xlsx"

    def test_save_csv(self):
        result = self.save_all("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.csv", 1)
        assert result == "/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.csv.xlsx"
        # Valid to append .xlsx to end of filename because only the last . is counted as the extension separator

    def test_save_no_filename(self):
        result = self.save_all("", 1)
        assert result == 1

    def test_save_no_tabs(self):
        result = self.save_all("/home/kieran/Documents/ehr-data-profiling-tool/Data Sets/Test/Test.csv", 0)
        assert result == 1


class TestPrint(unittest.TestCase):
    def print(self, info, index, columns):
        self.info = info
        self.index = index
        self.columns = columns
        #  If the table is empty print out a chunk from 0,0 to 50,50
        if self.info["Rows"] == 0 and self.info["Columns"] == 0:
            self.info["Rows"] += self.index if self.index < 50 else 50
            self.info["Columns"] += self.columns if self.columns < 50 else 50
            self.info["Printed Columns"] = self.info["Columns"]
            self.info["Printed Rows"] = self.info["Rows"]
            return self.info
        else:
            row_limit = self.info["Rows"] if self.index > self.info["Rows"] else self.index
            col_limit = self.info["Columns"] if self.columns > self.info["Columns"] else self.columns
        #  Table not empty and the row number doesn't match the amount of printed rows
        if self.info["Rows"] != self.info["Printed Rows"]:
            self.info["Printed Rows"] = self.info["Rows"]
            return self.info
        #  Table not empty and the column number doesn't match the amount of printed columns
        if self.info["Columns"] != self.info["Printed Columns"]:
            self.info["Printed Columns"] = self.info["Columns"]
            return self.info

    def test_empty_table(self):
        info = {"Rows": 0, "Columns": 0, "Printed Columns": 0, "Printed Rows": 0}
        result = self.print(info, 100, 100)
        assert result == {"Rows": 50, "Columns": 50, "Printed Columns": 50, "Printed Rows": 50}

    def test_empty_less_than_50(self):
        info = {"Rows": 0, "Columns": 0, "Printed Columns": 0, "Printed Rows": 0}
        result = self.print(info, 45, 45)
        assert result == {"Rows": 45, "Columns": 45, "Printed Columns": 45, "Printed Rows": 45}

    def test_empty_less_than_50_rows(self):
        info = {"Rows": 0, "Columns": 0, "Printed Columns": 0, "Printed Rows": 0}
        result = self.print(info, 45, 100)
        assert result == {"Rows": 45, "Columns": 50, "Printed Columns": 50, "Printed Rows": 45}

    def test_update_rows(self):
        info = {"Rows": 100, "Columns": 50, "Printed Columns": 50, "Printed Rows": 50}
        result = self.print(info, 45, 45)
        assert result == {"Rows": 100, "Columns": 50, "Printed Columns": 50, "Printed Rows": 100}

    def test_update_columns(self):
        info = {"Rows": 50, "Columns": 100, "Printed Columns": 50, "Printed Rows": 50}
        result = self.print(info, 45, 45)
        assert result == {"Rows": 50, "Columns": 100, "Printed Columns": 100, "Printed Rows": 50}


class TestSample(unittest.TestCase):
    def sample(self, spin_value, radio_status, tab_status, data, destination):
        self.radio_status = radio_status
        self.spin_value = spin_value
        self.tab_status = tab_status
        self.data = data
        self.destination = destination
        length = len(self.data.index)
        try:
            if spin_value > 0:
                if not self.radio_status["Start"] == 1 and not self.radio_status["End"] == 1 and not self.radio_status[
                                                                                                         "Random"] == 1:
                    return 1
                else:
                    if not self.tab_status == "Existing Tab":
                        if self.radio_status["Start"] == 1:
                            data = self.data.iloc[0:self.spin_value, :]
                            return data
                        if self.radio_status["End"] == 1:
                            data = self.data.iloc[length - self.spin_value:length, :]
                            return data
                    else:
                        if len(self.data.columns) != len(self.destination.columns):
                            raise ValueError("Destination columns don't match source columns")
                        if self.radio_status["Start"] == 1:
                            for row in range(self.spin_value):
                                self.destination = self.destination.append(self.data.iloc[row, :])
                            return self.destination
                        if self.radio_status["End"] == 1:
                            for row in range(self.spin_value):
                                self.destination = self.destination.append(self.data.iloc[length - spin_value + row, :])
                            return self.destination
                    if self.radio_status["Random"] == 1:
                        variables = sample(range(1, length), self.spin_value)
                        for row in variables:
                            self.destination = self.destination.append(self.data.iloc[row, :])
                        return self.destination
            else:
                return 1
        except ValueError as e:
            return 1

    def test_start_existing_tab(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 1, "End": 0, "Random": 0}
        result = self.sample(5, radio_status, "Existing Tab", data, destination)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "a", "b", "c", "d", "e"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_end_existing_tab(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 0, "End": 1, "Random": 0}
        result = self.sample(5, radio_status, "Existing Tab", data, destination)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 6, 7, 8, 9, 10],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "f", "g", "h", "i", "j"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_random_existing_tab(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 0, "End": 0, "Random": 1}
        result = self.sample(5, radio_status, "Existing Tab", data, destination)
        assert len(result.index) == 15

    def test_no_selection(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 0, "End": 0, "Random": 0}
        result = self.sample(5, radio_status, "Existing Tab", data, destination)
        assert result == 1

    def test_start_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 1, "End": 0, "Random": 0}
        result = self.sample(5, radio_status, "New Tab", data, destination)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5], "col 2": ["a", "b", "c", "d", "e"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_end_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 0, "End": 1, "Random": 0}
        result = self.sample(5, radio_status, "New Tab", data, destination)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [6, 7, 8, 9, 10], "col 2": ["f", "g", "h", "i", "j"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_random_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 0, "End": 0, "Random": 1}
        result = self.sample(5, radio_status, "New Tab", data, destination)
        assert len(result.index) == 15

    def test_none_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 0, "End": 0, "Random": 0}
        result = self.sample(5, radio_status, "New Tab", data, destination)
        assert result == 1

    def test_mismatch_columns(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        d2 = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
              "col 3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d2)
        radio_status = {"Start": 1, "End": 0, "Random": 0}
        result = self.sample(5, radio_status, "Existing Tab", data, destination)
        assert result == 1

    def test_no_spin_value(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 1, "End": 0, "Random": 0}
        result = self.sample(0, radio_status, "Existing Tab", data, destination)
        assert result == 1

    def test_negative_spin_value(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 1, "End": 0, "Random": 0}
        result = self.sample(-5, radio_status, "Existing Tab", data, destination)
        assert result == 1

    def test_max_spin_value(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        destination = pd.DataFrame(data=d)
        radio_status = {"Start": 1, "End": 0, "Random": 0}
        result = self.sample(10, radio_status, "Existing Tab", data, destination)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                       "j"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True


class TestFilter(unittest.TestCase):
    def filter(self, radio_status, data, operator, column_1, column_2, value, column_compare):
        self.radio_status = radio_status
        data_source = data
        if self.radio_status["Number"] == 1:
            #  Comparing two columns
            if column_compare == True:
                if operator == 'Greater Than':
                    self.filtered_data = data_source[data_source[column_1] > data_source[column_2]]
                elif operator == 'Less Than':
                    self.filtered_data = data_source[data_source[column_1] < data_source[column_2]]
                elif operator == 'Greater Than or Equal To':
                    self.filtered_data = data_source[data_source[column_1] >= data_source[column_2]]
                elif operator == 'Less Than or Equal To':
                    self.filtered_data = data_source[data_source[column_1] <= data_source[column_2]]
                elif operator == 'Equal To':
                    self.filtered_data = data_source[data_source[column_1] == data_source[column_2]]
                if self.filtered_data.empty:
                    return 1
                else:
                    return self.filtered_data
            #  Comparing column to value
            else:
                #  Try block catches ValueErrors when non int values are tried to be cast to ints
                try:
                    if operator == 'Greater Than':
                        self.filtered_data = data_source[data_source[column_1] > float(value)]
                    elif operator == 'Less Than':
                        self.filtered_data = data_source[data_source[column_1] < float(value)]
                    elif operator == 'Greater Than or Equal To':
                        self.filtered_data = data_source[data_source[column_1] >= float(value)]
                    elif operator == 'Less Than or Equal To':
                        self.filtered_data = data_source[data_source[column_1] <= float(value)]
                    elif operator == 'Equal To':
                        self.filtered_data = data_source[data_source[column_1] == float(value)]
                    if self.filtered_data.empty:
                        return 1
                    else:
                        return self.filtered_data
                except ValueError as e:
                    return 1
        #  Filter is being applied to a string column
        elif self.radio_status["String"] == 1:
            if operator == "Contains":
                if value.upper() == "NAN":
                    self.filtered_data = data_source[data_source[column_1].isna()]
                else:
                    data_source = data_source.dropna()
                    if "Sensitive" in radio_status:
                        self.filtered_data = data_source[data_source[column_1].str.contains(value, case=True)]
                    else:
                        self.filtered_data = data_source[data_source[column_1].str.contains(value, case=False)]
            if operator == "Doesn't Contain":
                if value.upper() == "NAN":
                    self.filtered_data = data_source[data_source[column_1].notna()]
                else:
                    data_source = data_source.dropna()
                    if "Sensitive" in radio_status:
                        self.filtered_data = data_source[~data_source[column_1].str.contains(value, case=True)]
                    else:
                        self.filtered_data = data_source[~data_source[column_1].str.contains(value, case=False)]
            if operator == "Equal To":
                if value.upper() == "NAN":
                    self.filtered_data = data_source[data_source[column_1].isna()]
                else:
                    data_source = data_source.dropna()
                    if "Sensitive" in radio_status:
                        self.filtered_data = data_source[data_source[column_1].str.match(value, case=True)]
                    else:
                        self.filtered_data = data_source[data_source[column_1].str.match(value, case=False)]
            if operator == "Not Equal To":
                if value.upper() == "NAN":
                    self.filtered_data = data_source[data_source[column_1].notna()]
                else:
                    data_source = data_source.dropna()
                    if "Sensitive" in radio_status:
                        self.filtered_data = data_source[~data_source[column_1].str.match(value, case=True)]
                    else:
                        self.filtered_data = data_source[~data_source[column_1].str.match(value, case=False)]
            if operator == "Starts With":
                data_source = data_source.dropna()
                self.filtered_data = data_source[data_source[column_1].str.startswith(value)]
            if operator == "Ends With":
                data_source = data_source.dropna()
                self.filtered_data = data_source[data_source[column_1].str.endswith(value, na=False)]
            if self.filtered_data.empty:
                return 1
            else:
                return self.filtered_data
        #  Filter is being applied to date column
        elif self.radio_status["Date"] == 1:
            if operator == "After":
                self.filtered_data = data_source[
                    data_source[column_1] > value]
            elif operator == "Before":
                self.filtered_data = data_source[
                    data_source[column_1] < value]
            elif operator == "Between":
                self.filtered_data = data_source[data_source[column_1] > value]
                self.filtered_data = self.filtered_data[data_source[column_1] < radio_status["Date2"]]
            elif operator == "Between Inclusive":
                self.filtered_data = data_source[data_source[column_1] >= value]
                self.filtered_data = self.filtered_data[data_source[column_1] <= radio_status["Date2"]]
            if self.filtered_data.empty:
                return 1
            else:
                return self.filtered_data

    def test_num_value_greater_than(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than", "col 1", "None", 5, False)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [6, 7, 8, 9, 10],
             "col 2": ["f", "g", "h", "i", "j"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_value_less_than(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than", "col 1", "None", 5, False)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4],
             "col 2": ["a", "b", "c", "d"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_value_equal_to(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "None", 5, False)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [5],
             "col 2": ["e"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_value_greater_equal_to(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than or Equal To", "col 1", "None", 5, False)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [5, 6, 7, 8, 9, 10],
             "col 2": ["e", "f", "g", "h", "i", "j"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_value_less_equal_to(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than or Equal To", "col 1", "None", 5, False)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5],
             "col 2": ["a", "b", "c", "d", "e"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_value_greater_than_with_nan(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, np.nan],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than", "col 1", "None", 5, False)
        assert len(result.index) == 5

    def test_num_value_less_than_with_nan(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, np.nan],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than", "col 1", "None", 5, False)
        assert len(result.index) == 4

    def test_num_value_equal_to_with_nan(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, np.nan],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "None", 5, False)
        assert len(result.index) == 1

    def test_num_value_greater_equal_to_with_nan(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, np.nan],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than or Equal To", "col 1", "None", 5, False)
        assert len(result.index) == 6

    def test_num_value_less_equal_to_with_nan(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, np.nan],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than or Equal To", "col 1", "None", 5, False)
        assert len(result.index) == 5

    def test_num_value_greater_than_max(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than", "col 1", "None", 10, False)
        assert result == 1

    def test_num_value_greater_equal_than_max(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than or Equal To", "col 1", "None", 11, False)
        assert result == 1

    def test_num_value_less_than_min(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than", "col 1", "None", 1, False)
        assert result == 1

    def test_num_value_less_equal_than_min(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than or Equal To", "col 1", "None", 0, False)
        assert result == 1

    def test_num_value_not_equal_to(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "None", 0, False)
        assert result == 1

    def test_num_column_equal_to(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [0, 3, 1, 4, 5, 4, 10, 9, 5, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", 0, True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [4, 5],
             "col 2": [4, 5]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_column_greater_than(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [0, 3, 1, 4, 5, 4, 10, 9, 5, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than", "col 1", "col 2", 0, True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 3, 6, 9],
             "col 2": [0, 1, 4, 5]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_column_greater_than_equal(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [0, 3, 1, 4, 5, 4, 10, 9, 5, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than or Equal To", "col 1", "col 2", 0, True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 3, 4, 5, 6, 9],
             "col 2": [0, 1, 4, 5, 4, 5]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_column_less_than(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [0, 3, 1, 4, 5, 4, 10, 9, 5, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than", "col 1", "col 2", 0, True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [2, 7, 8, 10],
             "col 2": [3, 10, 9, 11]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_column_less_than_equal(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [0, 3, 1, 4, 5, 4, 10, 9, 5, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than or Equal To", "col 1", "col 2", 0, True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [2, 4, 5, 7, 8, 10],
             "col 2": [3, 4, 5, 10, 9, 11]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_num_column_none_greater_than(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than", "col 1", "col 2", 0, True)
        assert result == 1

    def test_num_column_none_greater_than_equal(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Greater Than or Equal To", "col 1", "col 2", 0, True)
        assert result == 1

    def test_num_column_none_less_than(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than", "col 2", "col 1", 0, True)
        assert result == 1

    def test_num_column_none_less_than_equal(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Less Than or Equal To", "col 2", "col 1", 0, True)
        assert result == 1

    def test_num_column_none_equal(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], "col 2": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", 0, True)
        assert result == 1

    def test_str_contains_nan(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Contains", "col 1", "col 2", "NaN", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        assert len(result.index == 1)

    def test_str_contains_val(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Contains", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a"],
             "col 2": [1]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_contains_val_sensitive(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Contains", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a"],
             "col 2": [1]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_contains_val_sensitive_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Contains", "col 1", "col 2", "A", True)
        assert result == 1

    def test_str_contains_val_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Contains", "col 1", "col 2", "f", True)
        assert result == 1

    def test_str_doesnt_contain_nan(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Doesn't Contain", "col 1", "col 2", "NaN", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e"],
             "col 2": [1, 2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_doesnt_contain_val(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Doesn't Contain", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["b", "c", "d", "e"],
             "col 2": [2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_doesnt_contain_val_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Doesn't Contain", "col 1", "col 2", "f", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e"],
             "col 2": [1, 2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_doesnt_contain_val_sensitive(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Doesn't Contain", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["b", "c", "d", "e"],
             "col 2": [2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_doesnt_contain_val_sensitive_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Doesn't Contain", "col 1", "col 2", "A", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e"],
             "col 2": [1, 2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_equal_to_nan(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", "NaN", True)
        assert len(result.index) == 1

    def test_str_equal_to_val(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a"],
             "col 2": [1]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_equal_to_val_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", "f", True)
        assert result == 1

    def test_str_equal_to_sensitive(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a"],
             "col 2": [1]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_equal_to_sensitive_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Equal To", "col 1", "col 2", "A", True)
        assert result == 1

    def test_str_not_equal_to_nan(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Not Equal To", "col 1", "col 2", "NaN", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e"],
             "col 2": [1, 2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_not_equal_to_val(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Not Equal To", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["b", "c", "d", "e"],
             "col 2": [2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_not_equal_to_val_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Not Equal To", "col 1", "col 2", "f", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e"],
             "col 2": [1, 2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_not_equal_to_sensitive(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Not Equal To", "col 1", "col 2", "a", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["b", "c", "d", "e"],
             "col 2": [2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_str_not_equal_to_sensitive_not(self):
        d = {"col 1": ["a", "b", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Sensitive": True}
        result = self.filter(radio_status, data, "Not Equal To", "col 1", "col 2", "A", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e"],
             "col 2": [1, 2, 3, 4, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_starts_with_val(self):
        d = {"col 1": ["aba", "bab", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Starts With", "col 1", "col 2", "ab", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["aba"],
             "col 2": [1]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_starts_with_val_not(self):
        d = {"col 1": ["ab", "bab", "c", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Starts With", "col 1", "col 2", "acab", True)
        assert result == 1

    def test_ends_with_val(self):
        d = {"col 1": ["aba", "bab", "cba", "d", np.nan, "e"], "col 2": [1, 2, 3, 4, 5, 6]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0}
        result = self.filter(radio_status, data, "Ends With", "col 1", "col 2", "ab", True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["bab"],
             "col 2": [2]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_date_after(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1}
        result = self.filter(radio_status, data, "After", "col 1", "None", np.datetime64("2000-01-01T00:00"), True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_after_not(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1}
        result = self.filter(radio_status, data, "After", "col 1", "None", np.datetime64("2006-01-01T00:00"), True)
        assert result == 1

    def test_date_before(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1}
        result = self.filter(radio_status, data, "Before", "col 1", "None", np.datetime64("2006-01-01T00:00"), True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_date_before_not(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1}
        result = self.filter(radio_status, data, "Before", "col 1", "None", np.datetime64("2000-01-01T00:00"), True)
        assert result == 1

    def test_date_between(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1, "Date2": np.datetime64("2006-01-01T00:00")}
        result = self.filter(radio_status, data, "Between", "col 1", "None", np.datetime64("2000-01-01T00:00"), True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00")],
             "col 2": [2, 3, 4, 5, 6]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_date_between_not(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1, "Date2": np.datetime64("2000-03-01T00:00")}
        result = self.filter(radio_status, data, "Between", "col 1", "None", np.datetime64("2000-02-01T00:00"), True)
        assert result == 1

    def test_date_between_inclusive(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1, "Date2": np.datetime64("2006-01-01T00:00")}
        result = self.filter(radio_status, data, "Between Inclusive", "col 1", "None",
                             np.datetime64("2000-01-01T00:00"), True)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_between_inclusive_not(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1, "Date2": np.datetime64("2000-03-01T00:00")}
        result = self.filter(radio_status, data, "Between Inclusive", "col 1", "None",
                             np.datetime64("2000-02-01T00:00"), True)
        assert result == 1


class TestCompare(unittest.TestCase):
    def compare(self, radio_status, data, operator, column_1, column_2):
        if radio_status["Number"] == 1:
            col_1 = data[column_1]
            col_2 = data[column_2]
            if operator == '+':
                string = "{col_1} + {col_2}".format(col_1=column_1, col_2=column_2)
                series = col_1 + col_2
            elif operator == '-':
                string = "{col_1} - {col_2}".format(col_1=column_1, col_2=column_2)
                series = col_1 - col_2
            elif operator == '/':
                string = "{col_1} / {col_2}".format(col_1=column_1, col_2=column_2)
                series = col_1 / col_2
            elif operator == '*':
                string = "{col_1} * {col_2}".format(col_1=column_1, col_2=column_2)
                series = col_1 * col_2
        elif radio_status["String"] == 1:
            string = "{col_1} & {col_2}".format(col_1=column_1,
                                                col_2=column_2)
            col_1 = data[column_1].apply(str)
            col_2 = data[column_2].apply(str)
            if operator == 'Concat':
                series = col_1 + col_2
            if operator == 'Concat With Space Between':
                space_series = pd.Series(" ", index=col_1.index)
                series = col_1 + space_series + col_2
        elif radio_status["Date"] == 1:
            if operator == "Time Between":
                col_1 = pd.to_datetime(data[column_1])
                col_2 = pd.to_datetime(data[column_2])
                series = col_2 - col_1
                string = "Time Between {col_1} and {col_2}".format(col_1=column_1, col_2=column_2)
        if not radio_status["Existing"]:
            return series
        else:
            data = data.assign(operation_temp_name=series)
            if string not in data.columns:
                data = data.rename(columns={"operation_temp_name": string})
            else:
                counter = 0
                for col in data.columns:
                    if string in col:
                        counter += 1
                new_string = "{string} ({val})".format(string=string, val=counter)
                data = data.rename(columns={"operation_temp_name": new_string})
            return data

    def test_addition_existing(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": True}
        result = self.compare(radio_status, data, "+", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 1 + col 2": [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_addition_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": False}
        result = self.compare(radio_status, data, "+", "col 1", "col 2")
        desired_result = pd.Series(data=[2, 4, 6, 8, 10, 12, 14, 16, 18, 20])
        assert_series_equal(result, desired_result) == True

    def test_subtraction_existing(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": True}
        result = self.compare(radio_status, data, "-", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 1 - col 2": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_subtraction_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": False}
        result = self.compare(radio_status, data, "-", "col 1", "col 2")
        desired_result = pd.Series(data=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        assert_series_equal(result, desired_result) == True

    def test_divide_existing(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": True}
        result = self.compare(radio_status, data, "/", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 1 / col 2": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_divide_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": False}
        result = self.compare(radio_status, data, "/", "col 1", "col 2")
        desired_result = pd.Series(data=[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        assert_series_equal(result, desired_result) == True

    def test_multiply_existing(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": True}
        result = self.compare(radio_status, data, "*", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 1 * col 2": [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_multiply_new(self):
        d = {"col 1": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 1, "String": 0, "Date": 0, "Existing": False}
        result = self.compare(radio_status, data, "*", "col 1", "col 2")
        desired_result = pd.Series(data=[1, 4, 9, 16, 25, 36, 49, 64, 81, 100])
        assert_series_equal(result, desired_result) == True

    def test_concat_existing(self):
        d = {"col 1": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Existing": True}
        result = self.compare(radio_status, data, "Concat", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 1 & col 2": ["aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_concat_new(self):
        d = {"col 1": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Existing": False}
        result = self.compare(radio_status, data, "Concat", "col 1", "col 2")
        desired_result = pd.Series(data=["aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj"])
        assert_series_equal(result, desired_result) == True

    def test_concat_space_existing(self):
        d = {"col 1": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Existing": True}
        result = self.compare(radio_status, data, "Concat With Space Between", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 1 & col 2": ["a a", "b b", "c c", "d d", "e e", "f f", "g g", "h h", "i i", "j j"]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_concat_space_new(self):
        d = {"col 1": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
             "col 2": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 1, "Date": 0, "Existing": False}
        result = self.compare(radio_status, data, "Concat With Space Between", "col 1", "col 2")
        desired_result = pd.Series(data=["a a", "b b", "c c", "d d", "e e", "f f", "g g", "h h", "i i", "j j"])
        assert_series_equal(result, desired_result) == True

    def test_date_between_new(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1, "Existing": False}
        result = self.compare(radio_status, data, "Time Between", "col 1", "col 2")
        desired_result = pd.Series(data=[np.timedelta64(0, 'D'),
                                         np.timedelta64(365, 'D'),
                                         np.timedelta64(-365, 'D'),
                                         np.timedelta64(365, 'D'),
                                         np.timedelta64(0, 'D'),
                                         np.timedelta64(365, 'D'),
                                         np.timedelta64(0, 'D'),
                                         ])
        assert_series_equal(result, desired_result) == True

    def test_date_between_existing(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")]}
        data = pd.DataFrame(data=d)
        radio_status = {"Number": 0, "String": 0, "Date": 1, "Existing": True}
        result = self.compare(radio_status, data, "Time Between", "col 1", "col 2")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "Time Between col 1 and col 2": [np.timedelta64(0, 'D'),
                                              np.timedelta64(365, 'D'),
                                              np.timedelta64(-365, 'D'),
                                              np.timedelta64(365, 'D'),
                                              np.timedelta64(0, 'D'),
                                              np.timedelta64(365, 'D'),
                                              np.timedelta64(0, 'D'),
                                              ]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True


class TestNumberTrim(unittest.TestCase):
    def trim(self, operator, data, column, value):
        if operator == 'Round':
            data[column] = data[column].round(value)
            return data
        elif operator == 'Round Up':
            for i, val in enumerate(data[column]):
                data[column].iat[i] = ceil(val)
            return data
        elif operator == 'Round Down':
            for i, val in enumerate(data[column]):
                data[column].iat[i] = floor(val)
            return data

    def test_round_zero(self):
        d = {"col 1": [1.5, 2.5, 3.1123, 4.1235, 5.555, 6.9, 7.78, 8.5, 9.123, 10.1231231],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        result = self.trim("Round", data, "col 1", 0)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        #   Python Round Function Explains The Behaviour
        r = {"col 1": [2.0, 2.0, 3.0, 4.0, 6.0, 7.0, 8.0, 8.0, 9.0, 10.0],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_round_one(self):
        d = {"col 1": [1.5, 2.5, 3.1123, 4.1235, 5.555, 6.9, 7.78, 8.5, 9.123, 10.1231231],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        result = self.trim("Round", data, "col 1", 1)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        #   Python Round Function Explains The Behaviour
        r = {"col 1": [1.5, 2.5, 3.1, 4.1, 5.6, 6.9, 7.8, 8.5, 9.1, 10.1],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_round_six(self):
        d = {"col 1": [1.5, 2.5, 3.1123, 4.1235, 5.555, 6.9, 7.78, 8.5, 9.123, 10.1231231],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        result = self.trim("Round", data, "col 1", 6)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        #   Python Round Function Explains The Behaviour
        r = {"col 1": [1.5, 2.5, 3.1123, 4.1235, 5.555, 6.9, 7.78, 8.5, 9.123, 10.123123],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_round_up(self):
        d = {"col 1": [1.5, 2.5, 3.1123, 4.1235, 5.555, 6.9, 7.78, 8.5, 9.123, 10.1231231],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        result = self.trim("Round Up", data, "col 1", 0)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        #   Python Round Function Explains The Behaviour
        r = {"col 1": [2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_round_down(self):
        d = {"col 1": [1.5, 2.5, 3.1123, 4.1235, 5.555, 6.9, 7.78, 8.5, 9.123, 10.1231231],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        data = pd.DataFrame(data=d)
        result = self.trim("Round Down", data, "col 1", 0)
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        #   Python Round Function Explains The Behaviour
        r = {"col 1": [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0],
             "col 2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True


class TestStringTrim(unittest.TestCase):
    def trim(self, operator, data, trim_input, column, value, replace):
        if operator == "Trim":
            split_input = trim_input.split(":")
            start = int(split_input[0])
            end = int(split_input[1])
            if start < 0 or end < 0:
                return 1
            elif end < start:
                return 1
            else:
                data[column] = data[column].str.slice(start=start, stop=end)
                return data
        elif operator == "Remove":
            data[column] = data[column].str.replace(value, '', regex=False)
            return data
        elif operator == "Replace":
            data[column] = data[column].str.replace(value, replace, regex=False)
            return data

    def test_trim_valid(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", data, "1:3", "col 1", "", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["bc", "bc", "bc", "bc"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_equal(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", data, "1:1", "col 1", "", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["", "", "", ""],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_start_negative(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", data, "-1:1", "col 1", "", "")
        assert result == 1

    def test_trim_both_negative(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", data, "-2:-1", "col 1", "", "")
        assert result == 1

    def test_trim_end_less_than_start(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", data, "3:1", "col 1", "", "")
        assert result == 1

    def test_trim_alpha(self):
        pass
        # This is covered by a raised exception to a warning dialog

    def test_remove_valid(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Remove", data, "", "col 1", "a", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["bcdefg", "bcdefg", "bcdefg", "bcdefg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_remove_comma(self):
        d = {"col 1": ["abcdefg,", "ab,cdefg", "a,bcdefg", "a,bcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Remove", data, "", "col 1", ",", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_remove_punctuation(self):
        d = {"col 1": ["abc,!#defg,", "ab,!#cdefg", "a,bcd,!#efg", "a,bc,!#defg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Remove", data, "", "col 1", ",!#", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcdefg,", "abcdefg", "a,bcdefg", "a,bcdefg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_remove_number(self):
        d = {"col 1": ["abcd12efg", "12abcdefg", "abcdefg12", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Remove", data, "", "col 1", "12", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_remove_empty(self):
        d = {"col 1": ["abcd12efg", "12abcdefg", "abcdefg12", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Remove", data, "", "col 1", "", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcd12efg", "12abcdefg", "abcdefg12", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_replace_valid(self):
        d = {"col 1": ["abcdefg", "abcdefg", "abcdefg", "abcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Replace", data, "", "col 1", "a", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["bcdefg", "bcdefg", "bcdefg", "bcdefg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_replace_comma(self):
        d = {"col 1": ["abcdefg,", "ab,cdefg", "a,bcdefg", "a,bcdefg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Replace", data, "", "col 1", ",", "1")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcdefg1", "ab1cdefg", "a1bcdefg", "a1bcdefg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_replace_punctuation(self):
        d = {"col 1": ["abc,!#defg,", "ab,!#cdefg", "a,bcd,!#efg", "a,bc,!#defg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Replace", data, "", "col 1", ",!#", "1")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abc1defg,", "ab1cdefg", "a,bcd1efg", "a,bc1defg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_replace_number(self):
        d = {"col 1": ["abcd12efg", "12abcdefg", "abcdefg12", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Replace", data, "", "col 1", "12", " ")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcd efg", " abcdefg", "abcdefg ", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_replace_empty(self):
        d = {"col 1": ["abcd12efg", "12abcdefg", "abcdefg12", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        data = pd.DataFrame(data=d)
        result = self.trim("Replace", data, "", "col 1", "", "")
        result.index = pd.RangeIndex(start=0, stop=len(result.index), step=1)
        r = {"col 1": ["abcd12efg", "12abcdefg", "abcdefg12", "abcd21efg"],
             "col 2": [1, 2, 3, 4]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True


class TestDateTrim(unittest.TestCase):
    def trim(self, operation, mode, data, column):
        if operation == "Trim":
            if mode == "DateTime":
                data[column] = pd.to_datetime(data[column])
                return data
            if mode == "Year":
                data[column] = data[column].dt.year
                return data
            if mode == "Month":
                data[column] = data[column].dt.month
                return data
            if mode == "Day":
                data[column] = data[column].dt.day
                return data
            if mode == "Time":
                data[column] = data[column].dt.time
                return data
            if mode == "Hour":
                data[column] = data[column].dt.hour
                return data
            if mode == "Minute":
                data[column] = data[column].dt.minute
                return data
            if mode == "Second":
                data[column] = data[column].dt.second
                return data
        elif operation == "Convert":
            if mode == "Week of The Year":
                data[column] = data[column].dt.weekofyear
                return data
            if mode == "Day of The Year":
                data[column] = data[column].dt.dayofyear
                return data
            if mode == "Day of Week":
                data[column] = data[column].dt.dayofweek
                return data
            if mode == "Quarter":
                data[column] = data[column].dt.quarter
                return data

    def test_trim_datetime(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", "DateTime", data, "col 1")
        r = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_year(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-01-01T00:00"),
                       np.datetime64("2002-01-01T00:00"),
                       np.datetime64("2003-01-01T00:00"),
                       np.datetime64("2004-01-01T00:00"),
                       np.datetime64("2005-01-01T00:00"),
                       np.datetime64("2006-01-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", "Year", data, "col 1")
        r = {"col 1": [2000, 2001, 2002, 2003, 2004, 2005, 2006],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_month(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-01T00:00"),
                       np.datetime64("2002-06-01T00:00"),
                       np.datetime64("2003-09-01T00:00"),
                       np.datetime64("2004-10-01T00:00"),
                       np.datetime64("2005-11-01T00:00"),
                       np.datetime64("2006-12-01T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", "Month", data, "col 1")
        r = {"col 1": [1, 3, 6, 9, 10, 11, 12],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_day(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-11T00:00"),
                       np.datetime64("2002-06-21T00:00"),
                       np.datetime64("2003-09-30T00:00"),
                       np.datetime64("2004-10-10T00:00"),
                       np.datetime64("2005-11-20T00:00"),
                       np.datetime64("2006-12-07T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", "Day", data, "col 1")
        r = {"col 1": [1, 11, 21, 30, 10, 20, 7],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_time(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-11T00:00"),
                       np.datetime64("2002-06-21T00:00"),
                       np.datetime64("2003-09-30T00:00"),
                       np.datetime64("2004-10-10T00:00"),
                       np.datetime64("2005-11-20T00:00"),
                       np.datetime64("2006-12-07T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", "Time", data, "col 1")
        r = {"col 1": ["00:00:00",
                       "00:00:00",
                       "00:00:00",
                       "00:00:00",
                       "00:00:00",
                       "00:00:00",
                       "00:00:00"],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert len(data.index) == 7
        # assert_frame_equal(result, desired_result) == True

    def test_trim_hour(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-11T00:00"),
                       np.datetime64("2002-06-21T00:00"),
                       np.datetime64("2003-09-30T00:00"),
                       np.datetime64("2004-10-10T00:00"),
                       np.datetime64("2005-11-20T00:00"),
                       np.datetime64("2006-12-07T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Trim", "Hour", data, "col 1")
        r = {"col 1": [0, 0, 0, 0, 0, 0, 0],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_trim_minute(self):
        def test_trim_hour(self):
            d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                           np.datetime64("2001-03-11T00:00"),
                           np.datetime64("2002-06-21T00:00"),
                           np.datetime64("2003-09-30T00:00"),
                           np.datetime64("2004-10-10T00:00"),
                           np.datetime64("2005-11-20T00:00"),
                           np.datetime64("2006-12-07T00:00")],
                 "col 2": [1, 2, 3, 4, 5, 6, 7]}
            data = pd.DataFrame(data=d)
            result = self.trim("Trim", "Minute", data, "col 1")
            r = {"col 1": [0, 0, 0, 0, 0, 0, 0],
                 "col 2": [1, 2, 3, 4, 5, 6, 7]}
            desired_result = pd.DataFrame(data=r)
            assert_frame_equal(result, desired_result) == True

    def test_trim_second(self):
        def test_trim_hour(self):
            d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                           np.datetime64("2001-03-11T00:00"),
                           np.datetime64("2002-06-21T00:00"),
                           np.datetime64("2003-09-30T00:00"),
                           np.datetime64("2004-10-10T00:00"),
                           np.datetime64("2005-11-20T00:00"),
                           np.datetime64("2006-12-07T00:00")],
                 "col 2": [1, 2, 3, 4, 5, 6, 7]}
            data = pd.DataFrame(data=d)
            result = self.trim("Trim", "Second", data, "col 1")
            r = {"col 1": [0, 0, 0, 0, 0, 0, 0],
                 "col 2": [1, 2, 3, 4, 5, 6, 7]}
            desired_result = pd.DataFrame(data=r)
            assert_frame_equal(result, desired_result) == True

    def test_convert_woy(self):
        def test_trim_hour(self):
            d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                           np.datetime64("2001-03-11T00:00"),
                           np.datetime64("2002-06-21T00:00"),
                           np.datetime64("2003-09-30T00:00"),
                           np.datetime64("2004-10-10T00:00"),
                           np.datetime64("2005-11-20T00:00"),
                           np.datetime64("2006-12-07T00:00")],
                 "col 2": [1, 2, 3, 4, 5, 6, 7]}
            data = pd.DataFrame(data=d)
            result = self.trim("Convert", "Week of The Year", data, "col 1")
            r = {"col 1": [5, 6, 4, 1, 6, 6, 3],
                 "col 2": [1, 2, 3, 4, 5, 6, 7]}
            desired_result = pd.DataFrame(data=r)
            assert_frame_equal(result, desired_result) == True

    def test_convert_doy(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-11T00:00"),
                       np.datetime64("2002-06-21T00:00"),
                       np.datetime64("2003-09-30T00:00"),
                       np.datetime64("2004-10-10T00:00"),
                       np.datetime64("2005-11-20T00:00"),
                       np.datetime64("2006-12-07T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Convert", "Day of The Year", data, "col 1")
        r = {"col 1": [1, 70, 172, 273, 284, 324, 341],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_convert_dow(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-11T00:00"),
                       np.datetime64("2002-06-21T00:00"),
                       np.datetime64("2003-09-30T00:00"),
                       np.datetime64("2004-10-10T00:00"),
                       np.datetime64("2005-11-20T00:00"),
                       np.datetime64("2006-12-07T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Convert", "Day of Week", data, "col 1")
        r = {"col 1": [5, 6, 4, 1, 6, 6, 3],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True

    def test_convert_quarter(self):
        d = {"col 1": [np.datetime64("2000-01-01T00:00"),
                       np.datetime64("2001-03-11T00:00"),
                       np.datetime64("2002-06-21T00:00"),
                       np.datetime64("2003-09-30T00:00"),
                       np.datetime64("2004-10-10T00:00"),
                       np.datetime64("2005-11-20T00:00"),
                       np.datetime64("2006-12-07T00:00")],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        data = pd.DataFrame(data=d)
        result = self.trim("Convert", "Quarter", data, "col 1")
        r = {"col 1": [1, 1, 2, 3, 4, 4, 4],
             "col 2": [1, 2, 3, 4, 5, 6, 7]}
        desired_result = pd.DataFrame(data=r)
        assert_frame_equal(result, desired_result) == True


class TestCopyRow(unittest.TestCase):
    def copy_row(self, rows, info, source_columns, dest_columns):
        self.rows = rows
        existing = True
        try:
            if not existing:
                return 0
            else:
                if source_columns != dest_columns:
                    raise ValueError("Destination columns don't match source columns")
            for row in self.rows:
                pass
            if info["Rows"] != 0:
                info["Rows"] += len(self.rows)
                return info["Rows"]
            else:
                return 0
        except ValueError as e:
            return 1

    def test_valid_copy(self):
        info = {"Rows": 50}
        result = self.copy_row([0, 1, 2, 3, 4], info, 10, 10)
        assert result == 55

    def test_valid_empty_rows(self):
        info = {"Rows": 0}
        result = self.copy_row([0, 1, 2, 3, 4], info, 10, 10)
        assert result == 0

    def test_source_smaller(self):
        info = {"Rows": 50}
        result = self.copy_row([0, 1, 2, 3, 4], info, 9, 10)
        assert result == 1

    def test_source_bigger(self):
        info = {"Rows": 50}
        result = self.copy_row([0, 1, 2, 3, 4], info, 10, 9)
        assert result == 1
