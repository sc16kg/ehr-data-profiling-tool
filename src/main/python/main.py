from math import ceil, floor
from random import sample
from string import ascii_letters, digits
from sys import exit

import pandas as pd
import pyqtgraph as pg
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QFont, QIcon, QPixmap, QPainter
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QAction, QApplication, QMenu, QFileDialog, QInputDialog, \
    QHBoxLayout, QTabWidget, QTableWidget, QTableWidgetItem, QMessageBox, QDialog, QPushButton, \
    QGridLayout, QCheckBox, QLineEdit, QRadioButton, QSpinBox, QSizePolicy, QSpacerItem, QLabel, \
    QStyle, QComboBox, QStatusBar, QDateTimeEdit, QTextBrowser, QColorDialog, QGroupBox, QFrame, QTreeWidget, \
    QTreeWidgetItem
from fbs_runtime.application_context import cached_property
from fbs_runtime.application_context.PyQt5 import ApplicationContext
from numpy import histogram, int64, float64
from qtconsole.qt import QtGui, QtCore
from xlrd import XLRDError


class MainWindow(QMainWindow):
    def __init__(self, context):
        """
        Main window initialization
        """
        super(MainWindow, self).__init__()
        self.setWindowTitle("EHR Data Profiling Tool")
        self.resize(1400, 700)
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.context = context
        self.main_box = QHBoxLayout()
        self.start_buttons = True

        self.menu_items = []

        self.start_menu = QGridLayout()
        self.data_container = QVBoxLayout()

        font = QFont()
        font.setPointSize(16)
        font.setWeight(75)
        font.setBold(True)

        header_font = QFont()
        header_font.setPointSize(36)
        header_font.setWeight(75)

        self.open_icon = context.open_img

        self.header_label = QLabel("EHR DATA PROFILING TOOL")
        self.header_label.setFont(header_font)
        self.header_label.setAlignment(Qt.AlignCenter)
        self.start_menu.addWidget(self.header_label, 0, 0, 1, 7)

        self.open_button = QPushButton()
        self.open_button.setIcon(self.open_icon)
        self.open_button.setIconSize(QSize(256, 256))
        self.open_button.setToolTip("Open File")
        self.open_button.clicked.connect(self.open_file)
        self.start_menu.addWidget(self.open_button, 2, 1, 1, 1)

        self.open_label = QLabel("Open File")
        self.open_label.setAlignment(Qt.AlignCenter)
        self.open_label.setFont(font)
        self.start_menu.addWidget(self.open_label, 3, 1, 1, 1)

        self.h_spacer_1 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.start_menu.addItem(self.h_spacer_1, 2, 0, 1, 1)

        self.h_spacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.start_menu.addItem(self.h_spacer_2, 2, 2, 1, 1)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.start_menu.addItem(self.v_spacer_1, 1, 1, 1, 1)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.start_menu.addItem(self.v_spacer_2, 4, 1, 1, 1)

        self.help_icon = context.help_img

        self.help_button = QPushButton()
        self.help_button.setIcon(self.help_icon)
        self.help_button.setIconSize(QSize(256, 256))
        self.help_button.setToolTip("Help")
        self.help_button.clicked.connect(HelpDialog)
        self.start_menu.addWidget(self.help_button, 2, 3, 1, 1)

        self.help_label = QLabel("Help")
        self.help_label.setAlignment(Qt.AlignCenter)
        self.help_label.setFont(font)
        self.start_menu.addWidget(self.help_label, 3, 3, 1, 1)

        self.h_spacer_2r = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.start_menu.addItem(self.h_spacer_2r, 2, 4, 1, 1)

        self.v_spacer_1r = QSpacerItem(40, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.start_menu.addItem(self.v_spacer_1r, 1, 3, 1, 1)

        self.v_spacer_2r = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.start_menu.addItem(self.v_spacer_2r, 4, 3, 1, 1)

        self.quit_icon = context.quit_img

        self.quit_button = QPushButton()
        self.quit_button.setIcon(self.quit_icon)
        self.quit_button.setIconSize(QSize(256, 256))
        self.quit_button.setToolTip("Quit")
        self.quit_button.clicked.connect(self.close)
        self.start_menu.addWidget(self.quit_button, 2, 5, 1, 1)

        self.quit_label = QLabel("Quit Application")
        self.quit_label.setAlignment(Qt.AlignCenter)
        self.quit_label.setFont(font)
        self.start_menu.addWidget(self.quit_label, 3, 5, 1, 1)

        self.h_spacer_2rr = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.start_menu.addItem(self.h_spacer_2rr, 2, 6, 1, 1)

        self.v_spacer_1rr = QSpacerItem(40, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.start_menu.addItem(self.v_spacer_1rr, 1, 5, 1, 1)

        self.v_spacer_2rr = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.start_menu.addItem(self.v_spacer_2rr, 4, 5, 1, 1)

        self.main_box.addLayout(self.start_menu)

        self.setup_status_bar()
        self.setup_menu()

        self.central_widget.layout = self.setup_layout()

    def setup_tabs(self):
        """
        Creates the tab widgets
        """
        self.data_tabs = QTabWidget()
        self.data_tabs.setTabsClosable(True)
        self.data_tabs.setDocumentMode(False)
        self.data_tabs.setTabPosition(QTabWidget.North)
        self.data_tabs.setMovable(True)
        self.data_container.addWidget(self.data_tabs)
        self.data_tabs.setHidden(self.data_tabs.isHidden())
        self.data_tabs.tabCloseRequested.connect(self.close_data_tab)
        self.data_tabs.currentChanged.connect(self.status_table_data)

    def setup_status_bar(self):
        """
        Creates the status bar
        """
        self.status_bar = QStatusBar()
        self.status_bar.addPermanentWidget(QLabel("Kieran Gray & University of Leeds 2020"))
        self.table_dimensions = QLabel()
        self.setStatusBar(self.status_bar)

    def setup_menu(self):
        """
        Creates the main menu bar.
        Called once a data file has been opened
        """
        menu_bar = self.menuBar()
        menu_bar.setFont(QFont('Helvetica', 12, 30))

        self.quit = QAction("&Quit")
        self.quit.triggered.connect(self.close)

        self.help = QAction("&Help")
        self.help.triggered.connect(HelpDialog)

        self.file_menu = menu_bar.addMenu('&File')
        self.help_menu = menu_bar.addAction(self.help)
        self.quit_menu = menu_bar.addAction(self.quit)

        open_action = QAction("Open Data File", self)
        open_action.setIcon(self.style().standardIcon(getattr(QStyle, 'SP_DirOpenIcon')))
        open_action.setShortcut("Ctrl+O")
        open_action.triggered.connect(self.open_file)
        self.file_menu.addAction(open_action)

        self.save_tab_action = QAction("Save Tab As...", self)
        self.save_tab_action.setIcon(self.style().standardIcon(getattr(QStyle, 'SP_DriveFDIcon')))
        self.save_tab_action.setShortcut("Ctrl+S")
        self.save_tab_action.triggered.connect(self.save_tab)
        self.file_menu.addAction(self.save_tab_action)
        self.menu_items.append(self.save_tab_action)

        self.save_all_action = QAction("Save All As...", self)
        self.save_all_action.setIcon(self.style().standardIcon(getattr(QStyle, 'SP_DriveFDIcon')))
        self.save_all_action.triggered.connect(self.save_all)
        self.file_menu.addAction(self.save_all_action)
        self.menu_items.append(self.save_all_action)

    # noinspection PyTypeChecker
    def hide_title(self):
        """
        Hides the title menu buttons
        """
        self.header_label.setVisible(False)
        self.help_button.setVisible(False)
        self.open_button.setVisible(False)
        self.quit_button.setVisible(False)
        self.help_label.setVisible(False)
        self.open_label.setVisible(False)
        self.quit_label.setVisible(False)
        self.start_menu.setParent(None)

    def save_tab(self):
        """
        Function to allow user to save the data from one tab
        """
        number_of_tabs = len(self.data_tabs)
        current_tab = self.data_tabs.widget(self.data_tabs.currentIndex())
        if number_of_tabs > 0:
            filepath, _ = QFileDialog.getSaveFileName(None, "Save Data File", "", "Data Files (*.csv *.xlsx)")
            if len(filepath) > 0:
                if ".xlsx" in filepath:
                    with pd.ExcelWriter(filepath) as writer:
                        current_tab.data.to_excel(writer, sheet_name=current_tab.name)
                elif ".csv" in filepath:
                    current_tab.data.to_csv(filepath)
                elif "." in filepath:
                    warning_dialog("Not a Supported Filetype. Supported Types Are: '.csv' and '.xlsx'")
                else:
                    filepath = "{name}.csv".format(name=filepath)
                    current_tab.data.to_csv(filepath)
        else:
            warning_dialog("No Data To Save")

    def save_all(self):
        """
        Saves all of the tabs as an excel spreadsheet
        """
        number_of_tabs = len(self.data_tabs)
        if number_of_tabs > 0:
            filepath, _ = QFileDialog.getSaveFileName(None, "Save Data File", "", "Data Files (*.csv *.xlsx)")
            if len(filepath) > 0:
                try:
                    if ".xlsx" not in filepath:
                        filepath = "{name}.xlsx".format(name=filepath)
                    elif ".xlsx" in filepath:
                        pass
                    elif "." in filepath:
                        raise TypeError
                    with pd.ExcelWriter(filepath) as writer:
                        for i in range(len(self.data_tabs)):
                            self.data_tabs.widget(i).data.to_excel(writer, sheet_name=self.data_tabs.widget(i).name)
                except TypeError:
                    warning_dialog("Not a Supported Filetype. Save all only supports type: '.xlsx'")
        else:
            warning_dialog("No Data To Save")

    def enable_menu_items(self, enable):
        """
        Function to set the menu items as enabled when a file has been opened
        """
        if enable:
            if self.start_buttons:
                self.hide_title()
                self.setup_tabs()
                self.start_buttons = False
        for item in self.menu_items:
            if enable:
                item.setEnabled(True)
            else:
                item.setEnabled(False)

    def open_file(self):
        """
        Function to allow user to select and open files.
        Once file is opened tabs are created and tables printed to hold the data.
        """
        filepath, _ = QFileDialog.getOpenFileName(None, "Select Data File",
                                                  "",
                                                  "Data Files (*.csv *.xlsx)")
        if len(filepath) > 0:
            QApplication.setOverrideCursor(Qt.WaitCursor)
            if filepath.split(".")[1].upper() == "CSV":
                name = filepath.split("/")
                name = name[len(name) - 1]
                name = name.split(".")[0]
                try:
                    df = pd.read_csv(filepath)
                    if not df.empty:
                        self.enable_menu_items(True)
                        tab = CustomTab(name, "data")
                        tab.set_data(df)
                        self.status_table_data(self.data_tabs.currentIndex())
                        tab.print()
                    else:
                        QApplication.restoreOverrideCursor()
                        warning_dialog("Selected Data Source is Empty")
                except Exception as e:
                    warning_dialog(str(e))
            if filepath.split(".")[1].upper() == "XLSX":
                try:
                    xlsx = pd.ExcelFile(filepath)
                    df = pd.read_excel(xlsx, sheet_name=None)
                    for key in df:
                        if not df[key].empty:
                            self.enable_menu_items(True)
                            tab = CustomTab(key, "data")
                            tab.set_data(df[key])
                            self.status_table_data(self.data_tabs.currentIndex())
                            tab.print()
                        else:
                            QApplication.restoreOverrideCursor()
                            warning_dialog("Selected Data Source is Empty.")
                except XLRDError:
                    warning_dialog("Unexpected format, or corrupt file")
            QApplication.restoreOverrideCursor()

    def close_data_tab(self, index):
        """
        Removes a given data tab
        :param index: The index location of the individual tab within the QTabWidget() Object
        """
        self.data_tabs.removeTab(index)
        if len(self.data_tabs) == 0:
            #  If there are no tabs left make the QTabWidget() Invisible
            self.data_tabs.setVisible(False)
        if self.data_tabs.widget(0) is None:
            self.enable_menu_items(False)

    def status_table_data(self, index):
        """
        Sets the dataframe size indicator at the bottom left of the main window to the current dataframe dimensions
        :param index: the tab index
        """
        if self.data_tabs.widget(index) is not None:
            tab = self.data_tabs.widget(index)
            rows = len(tab.data.index)
            columns = len(tab.data.columns)
            string = "Rows: {rows} x Columns: {columns}".format(rows=str(rows), columns=str(columns))
            self.table_dimensions.setText(string)
            self.status_bar.addWidget(self.table_dimensions)
            self.header_label.setVisible(False)
        else:
            self.table_dimensions.setText("")
            self.header_label.setVisible(True)
            self.help_button.setVisible(True)
            self.open_button.setVisible(True)
            self.quit_button.setVisible(True)
            self.help_label.setVisible(True)
            self.open_label.setVisible(True)
            self.quit_label.setVisible(True)

    def setup_layout(self):
        """
        Setup for the main layout
        """
        self.main_box.addLayout(self.data_container)
        self.central_widget.setLayout(self.main_box)
        return self.main_box


class CustomTab(QWidget):
    def __init__(self, name, mode):
        """
        Custom Tab to include necessary functions for storing, managing, and printing data
        :param name: Name text to be displayed on the tab
        :param mode: data or var
        """
        super(CustomTab, self).__init__()
        self.name = name
        self.data = pd.DataFrame()
        self.data_backup = pd.DataFrame()
        self.info = {"Rows": 0, "Columns": 0, "Printed Rows": 0, "Printed Columns": 0, "Sorting": {}, "Row Height": 40}
        self.column_formatting = {}
        self.column_list = []
        self.active_filters = []
        self.layout = QVBoxLayout()
        self.menu_layout = QHBoxLayout()

        #  Variables for the search functionality
        self.current_item = 0
        self.found_items = 0
        self.found_list = []

        self.menu = QComboBox()
        self.menu.addItems(
            ["Menu", "Describe", "Visualizations", "Compare/Combine", "Filter", "Trim", "Sample", "Formatting",
             "Convert Data Types", "Rename Tab", "Copy Data to New Tab", "Reset Data"])
        self.menu.setItemIcon(0, app_context.menu_icon)
        self.menu.setItemIcon(1, app_context.describe_icon)
        self.menu.setItemIcon(2, app_context.bar_chart_icon)
        self.menu.setItemIcon(3, app_context.layers_icon)
        self.menu.setItemIcon(4, app_context.filter_icon)
        self.menu.setItemIcon(5, app_context.trim_icon)
        self.menu.setItemIcon(6, app_context.clipboard_icon)
        self.menu.setItemIcon(7, app_context.formatting_icon)
        self.menu.setItemIcon(8, app_context.convert_icon)
        self.menu.setItemIcon(9, app_context.rename_icon)
        self.menu.setItemIcon(10, app_context.copy_icon)
        self.menu.setItemIcon(11, app_context.refresh_icon)
        self.menu.currentIndexChanged.connect(self.menu_controller)
        self.menu.setCurrentIndex(0)

        self.menu_layout.addWidget(self.menu)
        self.menu_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.menu_layout.addSpacerItem(self.menu_spacer)

        self.menu_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.menu_layout.addSpacerItem(self.menu_spacer)

        self.search_bar = QLineEdit()
        self.menu_layout.addWidget(self.search_bar)
        self.search_bar.textChanged.connect(self.reset_search)

        self.search_button = QPushButton()
        self.search_button.setText("Search")
        self.search_button.setIcon(app_context.search_icon)
        self.menu_layout.addWidget(self.search_button)
        self.search_button.clicked.connect(self.search)

        self.search_label = QLabel()
        self.menu_layout.addWidget(self.search_label)
        self.search_label.setVisible(False)

        self.search_prev = QPushButton()
        self.search_prev.setText("Prev")
        self.menu_layout.addWidget(self.search_prev)
        self.search_prev.clicked.connect(lambda value, val=-1: self.search_increment(val))
        self.search_prev.setVisible(False)

        self.search_next = QPushButton()
        self.search_next.setText("Next")
        self.menu_layout.addWidget(self.search_next)
        self.search_next.clicked.connect(lambda value, val=1: self.search_increment(val))
        self.search_next.setVisible(False)

        self.layout.addLayout(self.menu_layout)
        self.reset_table()
        self.setAutoFillBackground(True)
        if mode == "data":
            self.mode = "data"
            win.data_tabs.addTab(self, self.name)
            win.data_tabs.setHidden(False)

    def set_data(self, data):
        """
        Setter for the tab data. Necessary to get the required column formatting dict entries
        :param data: the dataframe to be set as tab data
        """
        self.data = data
        for column in data.columns:
            self.column_formatting[column] = {"Red Negatives": False, "Rounding": 3}
            self.column_list.append(column)
        self.data_backup = self.data.copy()

    def copy_to_new_tab(self):
        """
        Copies the whole dataframe to a new tab
        """
        tab = CustomTab("Copy of {name}".format(name=self.name), "data")
        tab.set_data(self.data)
        tab.print()

    def menu_controller(self, index):
        """
        Menu controller for main tab menu
        :param index: the current index of the menu dropdown
        """
        if index == 1:
            DescribeDialog(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 2:
            VisualizationDialog()
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 3:
            CompareCombineDialog(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 4:
            FilterDialog(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 5:
            TrimDialog(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 6:
            SampleDialog(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 7:
            TableFormatting(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 8:
            ConvertDialog(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 9:
            RenameTab(self)
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 10:
            self.copy_to_new_tab()
            self.menu.setCurrentIndex(0)
            QApplication.restoreOverrideCursor()
        if index == 11:
            self.set_data(self.data_backup)
            self.active_filters = []
            self.reset_print_area()
            self.menu.setCurrentIndex(0)

    def reset_table(self):
        """
        Resets the table and it's headers and scrollbars
        """
        self.table = QTableWidget()
        self.v_scrollbar = self.table.verticalScrollBar()
        self.h_scrollbar = self.table.horizontalScrollBar()
        self.v_header = self.table.verticalHeader()
        self.h_header = self.table.horizontalHeader()
        self.make_table()

    def reset_search(self, text):
        """
        When the search bar input is empty reset all the gui elements for it and clear any search result data
        :param text: the current text in the search line edit
        """
        if text == "":
            self.found_list = []
            self.found_items = 0
            self.current_item = 0
            self.table.clearSelection()
            self.search_next.setVisible(False)
            self.search_prev.setVisible(False)
            self.search_label.setVisible(False)

    # noinspection PyUnboundLocalVariable
    def print(self):
        """
        Function to print the main data table in 50x50 chunks to prevent long wait times
        """
        #  If the table is empty print out a chunk from 0,0 to 50,50
        if self.info["Rows"] == 0 and self.info["Columns"] == 0:
            self.info["Rows"] += len(self.data.index) if len(self.data.index) < 50 else 50
            self.info["Columns"] += len(self.data.columns) if len(self.data.columns) < 50 else 50
            for j in range(0, self.info["Columns"]):
                self.table.insertColumn(j)
                formatting = self.column_formatting[self.column_list[j]]
                for i in range(0, self.info["Rows"]):
                    if j == 0:
                        self.table.insertRow(i)
                    val = self.data.iat[i, j]
                    self.table.setItem(i, j, QTableWidgetItem(str(val)))
                    self.table.item(i, j).setBackground(QtGui.QColor(125, 125, 125, 50))
                    self.table.item(i, j).setTextAlignment(Qt.AlignCenter)
                    if isinstance(val, (int, int64, float, float64)):
                        self.table.item(i, j).setText(format(val, '.' + str(formatting["Rounding"]) + 'f'))
                        if formatting["Red Negatives"]:
                            if val < 0:
                                self.table.item(i, j).setForeground(QtGui.QColor(255, 0, 0, 255))
                    self.table.setRowHeight(i, self.info["Row Height"])
            self.info["Printed Columns"] = self.info["Columns"]
            self.info["Printed Rows"] = self.info["Rows"]
        else:
            row_limit = self.info["Rows"] if len(self.data.index) > self.info["Rows"] else len(self.data.index)
            col_limit = self.info["Columns"] if len(self.data.columns) > self.info["Columns"] else len(
                self.data.columns)
        #  Table not empty and the row number doesn't match the amount of printed rows
        if self.info["Rows"] != self.info["Printed Rows"]:
            for j in range(0, col_limit):
                formatting = self.column_formatting[self.column_list[j]]
                for i in range(self.info["Printed Rows"], row_limit):
                    if j == 0:
                        self.table.insertRow(i)
                    val = self.data.iat[i, j]
                    self.table.setItem(i, j, QTableWidgetItem(str(val)))
                    self.table.item(i, j).setBackground(QtGui.QColor(125, 125, 125, 50))
                    self.table.item(i, j).setTextAlignment(Qt.AlignCenter)
                    if isinstance(val, (int, int64, float, float64)):
                        self.table.item(i, j).setText(format(val, '.' + str(formatting["Rounding"]) + 'f'))
                        if formatting["Red Negatives"]:
                            if val < 0:
                                self.table.item(i, j).setForeground(QtGui.QColor(255, 0, 0, 255))
                            else:
                                self.table.item(i, j).setForeground(QtGui.QColor(255, 255, 255, 255))
                    self.table.setRowHeight(i, self.info["Row Height"])
            self.info["Printed Rows"] = self.info["Rows"]
        #  Table not empty and the column number doesn't match the amount of printed columns
        if self.info["Columns"] != self.info["Printed Columns"]:
            for j in range(self.info["Printed Columns"], col_limit):
                self.table.insertColumn(j)
                formatting = self.column_formatting[self.column_list[j]]
                for i in range(0, row_limit):
                    val = self.data.iat[i, j]
                    self.table.setItem(i, j, QTableWidgetItem(str(val)))
                    self.table.item(i, j).setBackground(QtGui.QColor(125, 125, 125, 50))
                    self.table.item(i, j).setTextAlignment(Qt.AlignCenter)
                    if isinstance(val, (int, int64, float, float64)):
                        self.table.item(i, j).setText(format(val, '.' + str(formatting["Rounding"]) + 'f'))
                        if formatting["Red Negatives"]:
                            if val < 0:
                                self.table.item(i, j).setForeground(QtGui.QColor(255, 0, 0, 255))
                            else:
                                self.table.item(i, j).setForeground(QtGui.QColor(255, 255, 255, 255))
                    self.table.setRowHeight(i, self.info["Row Height"])
            self.info["Printed Columns"] = self.info["Columns"]
        self.update_headers()

    def make_table(self):
        """
        Function to setup a new table
        """
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.h_header.setContextMenuPolicy(Qt.CustomContextMenu)
        self.h_header.doubleClicked.connect(self.header_context_menu)
        self.v_header.setContextMenuPolicy(Qt.CustomContextMenu)
        self.v_header.doubleClicked.connect(self.vertical_header_context_menu)
        self.v_scrollbar.valueChanged.connect(
            lambda value, scrollbar=self.v_scrollbar: self.v_scroll_changed(value, scrollbar))
        self.h_scrollbar.valueChanged.connect(
            lambda value, scrollbar=self.h_scrollbar: self.h_scroll_changed(value, scrollbar))
        self.h_header.customContextMenuRequested.connect(self.header_context_menu)
        self.v_header.customContextMenuRequested.connect(self.vertical_header_context_menu)
        self.layout.addWidget(self.table)
        self.setLayout(self.layout)

    def vertical_header_context_menu(self, position):
        """
        Context menu for the vertical (side) table header
        :param position: the location where the menu has been requested
        """
        items = self.table.selectedIndexes()
        rows = []
        for item in items:
            row = self.table.verticalHeaderItem(item.row()).text()
            if int(row) not in rows:
                rows.append(int(row))
        if len(items) > 0:
            menu = QMenu()
            del_rows = menu.addAction("Delete Rows")
            del_row = menu.addAction("Delete Row")
            copy_row = menu.addAction("Copy Row")
            copy_rows = menu.addAction("Copy Rows")
            set_col_labels = menu.addAction("Set as Column Labels")
            reset_index = menu.addAction("Reset Index")
            if len(self.data.index) == 1:
                set_col_labels.setVisible(False)
            else:
                set_col_labels.setVisible(True)
            if len(rows) == 1:
                del_rows.setVisible(False)
                del_row.setVisible(True)
                copy_row.setVisible(True)
                copy_rows.setVisible(False)
                set_col_labels.setVisible(True)
            elif len(rows) > 1:
                del_rows.setVisible(True)
                del_row.setVisible(False)
                copy_rows.setVisible(True)
                copy_row.setVisible(False)
                set_col_labels.setVisible(False)
            position.setX(position.x() - 30)
            position.setY(position.y() + 10)
            action = menu.exec(self.table.viewport().mapToGlobal(position))
            if action == del_row:
                if len(self.data.index) == 1:
                    win.close_data_tab(win.data_tabs.currentIndex())
                else:
                    row = self.table.verticalHeaderItem(items[0].row()).text()
                    self.data.drop([int(row)], axis=0, inplace=True)
                    self.table.removeRow(items[0].row())
                    self.change_printed_area(0, -1)
                    win.status_table_data(win.data_tabs.currentIndex())
                    self.reset_index()
                    self.update_headers()
            if action == del_rows:
                if len(rows) == len(self.data.index):
                    win.close_data_tab(win.data_tabs.currentIndex())
                else:
                    self.data.drop(rows, axis=0, inplace=True)
                    for i in reversed(rows):
                        self.table.removeRow(i)
                        self.change_printed_area(0, -1)
                        win.status_table_data(win.data_tabs.currentIndex())
                    self.reset_index()
                    self.update_headers()
            if action == copy_row:
                CopyRowDialog(self, rows)
            if action == copy_rows:
                CopyRowDialog(self, rows)
            if action == set_col_labels:
                self.reset_index()
                row = items[0].row()
                new_cols = [str(i) for i in self.data.iloc[row]]
                self.data.columns = new_cols
                self.column_list = new_cols
                self.table.setHorizontalHeaderLabels(new_cols)
                self.data.drop([int(row)], axis=0, inplace=True)
                for col in new_cols:
                    self.column_formatting[col] = {"Red Negatives": False, "Rounding": 3}
                self.table.removeRow(items[0].row())
                self.change_printed_area(0, -1)
                self.data.index = pd.RangeIndex(start=0, stop=len(self.data.index), step=1)
                self.reset_print_area()
            if action == reset_index:
                self.reset_index()
                self.reset_print_area()

    def header_context_menu(self, position):
        """
        Custom context menu for header items on data tables
        :param position: The coordinate position to place the context menu
        """
        items = self.table.selectedIndexes()
        columns = {}
        for item in items:
            if self.table.horizontalHeaderItem(item.column()).text() not in columns:
                columns[self.table.horizontalHeaderItem(item.column()).text()] = item.column()
        if len(items) > 0:
            menu = QMenu()
            col = list(columns.keys())[0]
            copy_column = menu.addAction("Copy {name} To Tab".format(name=col))
            del_column = menu.addAction("Delete Column")
            del_columns = menu.addAction("Delete Columns")
            rename_column = menu.addAction("Rename Column")
            sorting = menu.addMenu("Sorting")
            sort_descending = sorting.addAction("Sort Descending")
            sort_descending.setCheckable(True)
            sort_ascending = sorting.addAction("Sort Ascending")
            sort_ascending.setCheckable(True)
            sort_none = sorting.addAction("No Sorting")
            sort_none.setCheckable(True)
            formatting = menu.addAction("Formatting")
            if len(columns) == 1:
                del_columns.setVisible(False)
                copy_column.setVisible(True)
                del_column.setVisible(True)
                rename_column.setVisible(True)
                sorting.setEnabled(True)
                if col in self.info["Sorting"]:
                    if self.info["Sorting"][col] == 0:
                        sort_none.setChecked(True)
                        sort_descending.setChecked(False)
                        sort_ascending.setChecked(False)
                    if self.info["Sorting"][col] == 1:
                        sort_none.setChecked(False)
                        sort_descending.setChecked(True)
                        sort_ascending.setChecked(False)
                    if self.info["Sorting"][col] == 2:
                        sort_none.setChecked(False)
                        sort_descending.setChecked(False)
                        sort_ascending.setChecked(True)
                else:
                    sort_none.setChecked(True)
                    copy_column.setVisible(True)
            elif len(columns) > 1:
                del_columns.setVisible(True)
                copy_column.setVisible(False)
                del_column.setVisible(False)
                rename_column.setVisible(False)
                sorting.setEnabled(False)
            position.setY(130 + position.y())
            action = menu.exec(win.mapToGlobal(position))
            if action == formatting:
                ColumnFormatting(self, columns)
            if action == copy_column:
                CopyColumnDialog(self, col)
            if action == del_column:
                if len(self.data.columns) == 1:
                    win.close_data_tab(win.data_tabs.currentIndex())
                else:
                    self.data.drop([col], axis=1, inplace=True)
                    self.table.removeColumn(columns[col])
                    self.change_printed_area(1, -1)
                    win.status_table_data(win.data_tabs.currentIndex())
            if action == del_columns:
                if len(columns) == len(self.data.columns):
                    win.close_data_tab(win.data_tabs.currentIndex())
                else:
                    self.data.drop(list(columns.keys()), axis=1, inplace=True)
                    for i in reversed(list(columns.values())):
                        self.table.removeColumn(i)
                        self.change_printed_area(1, -1)
                    self.reset_print_area()
                    win.status_table_data(win.data_tabs.currentIndex())
            if action == rename_column:
                RenameColumn(self, col)
            if action == sort_descending:
                self.info["Sorting"] = {col: 1}
                self.data.sort_values(by=col, ascending=False, inplace=True)
                self.reset_index()
                self.reset_print_area()
            if action == sort_ascending:
                self.info["Sorting"] = {col: 2}
                self.data.sort_values(by=col, inplace=True)
                self.reset_index()
                self.reset_print_area()
            if action == sort_none:
                self.info["Sorting"] = {col: 0}
                self.data.sort_index(inplace=True)
                self.reset_index()
                self.reset_print_area()

    def v_scroll_changed(self, value, scrollbar):
        """
        Function that is triggered when the vertical scrollbar is changed
        :param value: The current position of the scrollbar
        :param scrollbar: The scrollbar object
        """
        if value == scrollbar.maximum():
            self.info["Rows"] += 50
            self.print()

    def h_scroll_changed(self, value, scrollbar):
        """
        Function that is triggered when the horizontal scrollbar is changed
        :param value: The current position of the scrollbar
        :param scrollbar: The scrollbar object
        """
        if value == scrollbar.maximum():
            self.info["Columns"] += 50
            self.print()

    def update_headers(self):
        """
        Function to update the table headers with the visualization names
        """
        h_labels = [str(i) for i in self.data.columns]
        v_labels = [str(i) for i in self.data.index]
        self.table.setHorizontalHeaderLabels(h_labels)
        self.table.setVerticalHeaderLabels(v_labels)

    def reset_index(self):
        """
        Function to reset the dataframe index, prevents issues with setting column labels
        """
        self.data.index = pd.RangeIndex(start=0, stop=len(self.data.index), step=1)

    def reset_print_area(self):
        """
        Resets the printed area so on the next pass of the iterative_print function the table will be printed again
        """
        self.info["Columns"] = 0
        self.info["Printed Columns"] = 0
        self.info["Rows"] = 0
        self.info["Printed Rows"] = 0
        win.status_table_data(win.data_tabs.currentIndex())
        self.table.setParent(None)
        self.reset_table()
        self.print()

    def change_printed_area(self, axis, val):
        """
        Alters the stored values for the data and variable table printed rows
        :param axis: 0 = Row, 1 = Column
        :param val: the amount to increment the printed area by
        """
        if axis == 0:
            self.info["Rows"] += val
            self.info["Printed Rows"] += val
        if axis == 1:
            self.info["Columns"] += val
            self.info["Printed Columns"] += val

    def search(self):
        """
        Function to search the table for the search string
        """
        var = self.search_bar.text().upper()
        if len(var) > 0:
            self.found_items = 0
            self.found_list = []
            self.current_item = 0
            QApplication.setOverrideCursor(Qt.WaitCursor)
            for i in range(len(self.data.columns)):
                for j in range(len(self.data.index)):
                    try:
                        if var.upper() in str(self.data.iat[j, i]).upper():
                            self.found_list.append([j, i])
                    except IndexError:
                        pass
            self.found_items = len(self.found_list)
            QApplication.restoreOverrideCursor()
            if self.found_items == 0:
                self.search_label.setText("No Matches")
                self.search_label.setVisible(True)
                self.search_next.setVisible(False)
                self.search_prev.setVisible(False)
            else:
                label_text = "{count} Matches Found: {item} of {count}".format(
                    count=str(self.found_items), item=str(self.current_item))
                self.search_label.setText(label_text)
                self.search_label.setVisible(True)
                self.search_next.setVisible(True)
                self.search_prev.setVisible(True)
                self.search_increment(1)

    def search_increment(self, val):
        """
        Function to increment the current item counter for navigating through found items
        :param val: 1 or -1 for increment up or down
        """
        if val == 1:
            self.current_item += 1
        if val == -1:
            self.current_item -= 1
        if self.current_item == 1:
            self.search_prev.setVisible(False)
        else:
            self.search_prev.setVisible(True)
        if self.current_item == self.found_items:
            self.search_next.setVisible(False)
        else:
            self.search_next.setVisible(True)
        self.select_item()

    def select_item(self):
        """
        Function to set the current highlighted cell to the current search item
        """
        if self.found_list[self.current_item - 1][0] > self.info["Printed Rows"]:
            self.info["Rows"] = self.found_list[self.current_item - 1][0] + 10
            self.print()
        if self.found_list[self.current_item - 1][1] > self.info["Printed Columns"]:
            self.info["Columns"] = self.found_list[self.current_item - 1][1] + 10
            self.print()
        self.table.setCurrentCell(self.found_list[self.current_item - 1][0], self.found_list[self.current_item - 1][1])
        label_text = "{count} Matches Found: {item} of {count}".format(
            count=str(self.found_items), item=str(self.current_item))
        self.search_label.setText(label_text)


class SampleDialog:
    def __init__(self, tab):
        """
        Initializes a sample dialog
        :param tab: the tab it is being initialized from
        """
        super(SampleDialog, self).__init__()
        self.tab = tab
        self.dialog = QDialog()
        self.dialog.resize(500, 350)
        self.dialog_v_layout = QVBoxLayout(self.dialog)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.dialog_v_layout.addItem(self.v_spacer_1)

        self.input_group = QGroupBox(self.dialog)
        self.input_group.setTitle("Sample")
        self.input_grid = QGridLayout(self.input_group)
        self.input_spin_box = QSpinBox(self.input_group)
        self.input_spin_box.setMaximum(len(self.tab.data.index))
        self.input_grid.addWidget(self.input_spin_box, 0, 0, 1, 3)

        self.start_radio = QRadioButton(self.input_group)
        self.start_radio.setText("Start")
        self.input_grid.addWidget(self.start_radio, 1, 0, 1, 1)

        self.end_radio = QRadioButton(self.input_group)
        self.end_radio.setText("End")
        self.input_grid.addWidget(self.end_radio, 1, 1, 1, 1)

        self.random_radio = QRadioButton(self.input_group)
        self.random_radio.setText("Random")
        self.input_grid.addWidget(self.random_radio, 1, 2, 1, 1)

        self.dialog_v_layout.addWidget(self.input_group)

        self.tab_group = QGroupBox(self.dialog)
        self.tab_layout = QHBoxLayout(self.tab_group)

        self.tab_checkbox = QCheckBox(self.tab_group)
        self.tab_dropdown = QComboBox(self.tab_group)
        self.tab_checkbox.setText("Add To Existing Tab")
        self.tab_layout.addWidget(self.tab_checkbox)
        self.tab_checkbox.stateChanged.connect(
            lambda state: self.tab_dropdown.setEnabled(self.tab_checkbox.isChecked()))
        self.tab_dropdown.addItem("Select...")
        for tab in range(len(win.data_tabs)):
            name = win.data_tabs.widget(tab).name
            self.tab_dropdown.addItem(name)
        self.tab_dropdown.setEnabled(False)
        self.tab_layout.addWidget(self.tab_dropdown)

        self.dialog_v_layout.addWidget(self.tab_group)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.dialog_v_layout.addItem(self.v_spacer_2)

        self.button_layout = QHBoxLayout()

        self.ok_button = QPushButton(self.dialog)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply_sample)

        self.button_layout.addWidget(self.ok_button)

        self.cancel_button = QPushButton(self.dialog)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)

        self.button_layout.addWidget(self.cancel_button)

        self.dialog_v_layout.addLayout(self.button_layout)

        self.dialog.setWindowTitle("Sample")
        self.dialog.exec()

    def apply_sample(self):
        """
        Return the value from the radio buttons and the spin box
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        length = len(self.tab.data.index)
        try:
            if self.input_spin_box.value() > 0:
                if not self.start_radio.isChecked() and not self.end_radio.isChecked() and not self.random_radio.isChecked():
                    warning_dialog("Select Either 'Start', 'End', or 'Random'")
                else:
                    if not self.tab_checkbox.isChecked():
                        tab = CustomTab("Sample", "data")
                        tab.set_data(pd.DataFrame(columns=self.tab.data.columns))
                        if self.start_radio.isChecked():
                            tab.data = self.tab.data.iloc[0:self.input_spin_box.value(), :]
                        if self.end_radio.isChecked():
                            tab.data = self.tab.data.iloc[length - self.input_spin_box.value():length, :]
                    else:
                        tab = win.data_tabs.widget(self.tab_dropdown.currentIndex() - 1)
                        if len(self.tab.data.columns) != len(tab.data.columns):
                            raise ValueError("Destination columns don't match source columns")
                        if self.start_radio.isChecked():
                            for row in range(self.input_spin_box.value()):
                                tab.data = tab.data.append(self.tab.data.iloc[row, :])
                        if self.end_radio.isChecked():
                            for row in range(self.input_spin_box.value()):
                                tab.data = tab.data.append(self.tab.data.iloc[length - 1 - row, :])
                    if self.random_radio.isChecked():
                        variables = sample(range(1, length), self.input_spin_box.value())
                        for row in variables:
                            tab.data = tab.data.append(self.tab.data.iloc[row, :])
                    if tab.info["Rows"] != 0:
                        tab.info["Rows"] += self.input_spin_box.value()
                    tab.reset_index()
                    tab.reset_print_area()
                    self.dialog.accept()
            else:
                warning_dialog("Sample Size must be greater than 0")
        except ValueError as e:
            warning_dialog(str(e))
        QApplication.restoreOverrideCursor()


class ColumnFormatting:
    def __init__(self, tab, columns):
        """
        Dialog to format values in the given dict of columns.
        """
        super(ColumnFormatting, self).__init__()
        self.tab = tab
        self.columns = columns
        self.dialog = QDialog()
        self.dialog.setFixedSize(420, 240)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 400, 240)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_1, 0, 0, 1, 1)

        self.label = QLabel(self.grid_layout_widget)
        self.label.setText("Precision")
        self.grid_layout.addWidget(self.label, 1, 0, 1, 1)

        self.input = QSpinBox(self.grid_layout_widget)
        self.input.setMaximum(6)
        self.input.setMinimum(0)
        self.grid_layout.addWidget(self.input, 2, 0, 1, 2)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_2, 5, 0, 1, 1)

        self.red_negatives = QCheckBox(self.grid_layout_widget)
        self.red_negatives.setText("Red Negatives")
        self.grid_layout.addWidget(self.red_negatives, 7, 1, 1, 1)

        self.v_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_3, 7, 0, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("OK")
        self.grid_layout.addWidget(self.ok_button, 8, 0, 1, 1)
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.grid_layout.addWidget(self.cancel_button, 8, 1, 1, 1)
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)

        self.v_spacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_4, 9, 0, 1, 1)

        self.dialog.setWindowTitle("Column Formatting")

        self.dialog.exec()

    def apply(self):
        """
        Applies the choices from the dialog to the column
        """
        for col in self.columns:
            if self.red_negatives.isChecked():
                self.tab.column_formatting[col]["Red Negatives"] = True
            else:
                self.tab.column_formatting[col]["Red Negatives"] = False
            self.tab.column_formatting[col]["Rounding"] = self.input.value()
        self.tab.reset_print_area()
        self.dialog.accept()


class TableFormatting:
    def __init__(self, tab):
        """
        Dialog to format values in the given table.
        """
        super(TableFormatting, self).__init__()
        self.tab = tab
        self.dialog = QDialog()
        self.dialog.setFixedSize(420, 240)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 400, 240)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_1, 0, 0, 1, 1)

        self.label = QLabel(self.grid_layout_widget)
        self.label.setText("Precision")
        self.grid_layout.addWidget(self.label, 1, 0, 1, 1)

        self.input = QSpinBox(self.grid_layout_widget)
        self.input.setMaximum(6)
        self.input.setMinimum(0)
        self.grid_layout.addWidget(self.input, 2, 0, 1, 2)

        self.label_2 = QLabel(self.grid_layout_widget)
        self.label_2.setText("Row Height")
        self.grid_layout.addWidget(self.label_2, 3, 0, 1, 1)

        self.input_2 = QSpinBox(self.grid_layout_widget)
        self.input_2.setMinimum(0)
        self.input_2.setValue(self.tab.info["Row Height"])
        self.grid_layout.addWidget(self.input_2, 4, 0, 1, 2)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_2, 5, 0, 1, 1)

        self.red_negatives = QCheckBox(self.grid_layout_widget)
        self.red_negatives.setText("Red Negatives")
        self.grid_layout.addWidget(self.red_negatives, 7, 1, 1, 1)

        self.v_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_3, 7, 0, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("OK")
        self.grid_layout.addWidget(self.ok_button, 8, 0, 1, 1)
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.grid_layout.addWidget(self.cancel_button, 8, 1, 1, 1)
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)

        self.v_spacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_4, 9, 0, 1, 1)

        self.dialog.setWindowTitle("Table Formatting")

        self.dialog.exec()

    def apply(self):
        """
        Applies the choices from the dialog to the column
        """
        if self.red_negatives.isChecked():
            for col in self.tab.column_formatting:
                self.tab.column_formatting[col]["Red Negatives"] = True
                self.tab.column_formatting[col]["Rounding"] = self.input.value()
        else:
            for col in self.tab.column_formatting:
                self.tab.column_formatting[col]["Red Negatives"] = False
                self.tab.column_formatting[col]["Rounding"] = self.input.value()
        if self.input_2.value() != self.tab.info["Row Height"]:
            self.tab.info["Row Height"] = self.input_2.value()
        self.tab.reset_print_area()
        self.dialog.accept()


class DescribeDialog:
    def __init__(self, tab):
        """
        Initializes a describe dialog
        :param tab: the tab that it is being initialized from
        """
        super(DescribeDialog, self).__init__()
        self.tab = tab
        self.dialog = QDialog()
        self.dialog.resize(1400, 800)
        self.layout = QGridLayout(self.dialog)

        self.invalid = ["datetime64[ns]", "object", "string"]

        self.variables = []
        self.show_variables = {}

        self.table = QTableWidget(self.dialog)
        self.layout.addWidget(self.table, 0, 0, 2, 1)
        self.table.insertColumn(0)
        self.table.insertColumn(0)
        self.get_variable_names()
        self.setup_table()

        self.group_1 = QGroupBox(self.dialog)
        self.group_1.setTitle("Descriptive Statistics")
        self.group_1.setAlignment(Qt.AlignCenter)

        self.grid_1 = QGridLayout(self.group_1)

        self.line_1 = QFrame(self.group_1)
        self.line_1.setFrameShape(QFrame.HLine)
        self.line_1.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_1, 0, 0, 1, 2)

        self.label_25 = QLabel(self.group_1)
        self.label_25.setText("25%: ")
        self.label_25.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_25, 1, 0, 1, 1)

        self.label_25_data = QLabel(self.group_1)
        self.label_25_data.setText("")
        self.label_25_data.setWordWrap(True)
        self.label_25_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_25_data, 1, 1, 1, 1)

        self.line_2 = QFrame(self.group_1)
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_2, 2, 0, 1, 2)

        self.label_50 = QLabel(self.group_1)
        self.label_50.setText("50%: ")
        self.label_50.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_50, 3, 0, 1, 1)

        self.label_50_data = QLabel(self.group_1)
        self.label_50_data.setText("")
        self.label_50_data.setWordWrap(True)
        self.label_50_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_50_data, 3, 1, 1, 1)

        self.line_3 = QFrame(self.group_1)
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_3, 4, 0, 1, 2)

        self.label_75 = QLabel(self.group_1)
        self.label_75.setText("75%: ")
        self.label_75.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_75, 5, 0, 1, 1)

        self.label_75_data = QLabel(self.group_1)
        self.label_75_data.setText("")
        self.label_75_data.setWordWrap(True)
        self.label_75_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_75_data, 5, 1, 1, 1)

        self.line_4 = QFrame(self.group_1)
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_4, 6, 0, 1, 2)

        self.label_max = QLabel(self.group_1)
        self.label_max.setText("Max: ")
        self.label_max.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_max, 7, 0, 1, 1)

        self.label_max_data = QLabel(self.group_1)
        self.label_max_data.setText("")
        self.label_max_data.setWordWrap(True)
        self.label_max_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_max_data, 7, 1, 1, 1)

        self.line_5 = QFrame(self.group_1)
        self.line_5.setFrameShape(QFrame.HLine)
        self.line_5.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_5, 8, 0, 1, 2)

        self.label_mean = QLabel(self.group_1)
        self.label_mean.setText("Mean: ")
        self.label_mean.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_mean, 9, 0, 1, 1)

        self.label_mean_data = QLabel(self.group_1)
        self.label_mean_data.setText("")
        self.label_mean_data.setWordWrap(True)
        self.label_mean_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_mean_data, 9, 1, 1, 1)

        self.line_6 = QFrame(self.group_1)
        self.line_6.setFrameShape(QFrame.HLine)
        self.line_6.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_6, 10, 0, 1, 2)

        self.label_min = QLabel(self.group_1)
        self.label_min.setText("Min: ")
        self.label_min.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_min, 11, 0, 1, 1)

        self.label_min_data = QLabel(self.group_1)
        self.label_min_data.setText("")
        self.label_min_data.setWordWrap(True)
        self.label_min_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_min_data, 11, 1, 1, 1)

        self.line_7 = QFrame(self.group_1)
        self.line_7.setFrameShape(QFrame.HLine)
        self.line_7.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_7, 12, 0, 1, 2)

        self.label_std = QLabel(self.group_1)
        self.label_std.setText("STD: ")
        self.label_std.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_std, 13, 0, 1, 1)

        self.label_std_data = QLabel(self.group_1)
        self.label_std_data.setText("")
        self.label_std_data.setWordWrap(True)
        self.label_std_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_std_data, 13, 1, 1, 1)

        self.line_8 = QFrame(self.group_1)
        self.line_8.setFrameShape(QFrame.HLine)
        self.line_8.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_8, 14, 0, 1, 2)

        self.label_sum = QLabel(self.group_1)
        self.label_sum.setText("Sum: ")
        self.label_sum.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_sum, 15, 0, 1, 1)

        self.label_sum_data = QLabel(self.group_1)
        self.label_sum_data.setText("")
        self.label_sum_data.setWordWrap(True)
        self.label_sum_data.setAlignment(Qt.AlignCenter)
        self.grid_1.addWidget(self.label_sum_data, 15, 1, 1, 1)

        self.line_9 = QFrame(self.group_1)
        self.line_9.setFrameShape(QFrame.HLine)
        self.line_9.setFrameShadow(QFrame.Sunken)
        self.grid_1.addWidget(self.line_9, 16, 0, 1, 2)

        self.layout.addWidget(self.group_1, 0, 1, 1, 1)

        self.group_2 = QGroupBox(self.dialog)
        self.group_2.setTitle("Descriptive Statistics")
        self.group_2.setAlignment(Qt.AlignCenter)

        self.grid_2 = QGridLayout(self.group_2)

        self.line_10 = QFrame(self.group_2)
        self.line_10.setFrameShape(QFrame.HLine)
        self.line_10.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_10, 0, 0, 1, 2)

        self.label_count = QLabel(self.group_2)
        self.label_count.setText("Count: ")
        self.label_count.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_count, 1, 0, 1, 1)

        self.label_count_data = QLabel(self.group_2)
        self.label_count_data.setText("")
        self.label_count_data.setWordWrap(True)
        self.label_count_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_count_data, 1, 1, 1, 1)

        self.line_11 = QFrame(self.group_2)
        self.line_11.setFrameShape(QFrame.HLine)
        self.line_11.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_11, 2, 0, 1, 2)

        self.label_null = QLabel(self.group_2)
        self.label_null.setText("Null: ")
        self.label_null.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_null, 3, 0, 1, 1)

        self.label_null_data = QLabel(self.group_2)
        self.label_null_data.setText("")
        self.label_null_data.setWordWrap(True)
        self.label_null_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_null_data, 3, 1, 1, 1)

        self.line_12 = QFrame(self.group_2)
        self.line_12.setFrameShape(QFrame.HLine)
        self.line_12.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_12, 4, 0, 1, 2)

        self.label_median = QLabel(self.group_2)
        self.label_median.setText("Median: ")
        self.label_median.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_median, 5, 0, 1, 1)

        self.label_median_data = QLabel(self.group_2)
        self.label_median_data.setText("")
        self.label_median_data.setWordWrap(True)
        self.label_median_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_median_data, 5, 1, 1, 1)

        self.line_13 = QFrame(self.group_2)
        self.line_13.setFrameShape(QFrame.HLine)
        self.line_13.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_13, 6, 0, 1, 2)

        self.label_mode = QLabel(self.group_2)
        self.label_mode.setText("Mode: ")
        self.label_mode.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_mode, 7, 0, 1, 1)

        self.label_mode_data = QLabel(self.group_2)
        self.label_mode_data.setText("")
        self.label_mode_data.setWordWrap(True)
        self.label_mode_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_mode_data, 7, 1, 1, 1)

        self.line_14 = QFrame(self.group_2)
        self.line_14.setFrameShape(QFrame.HLine)
        self.line_14.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_14, 8, 0, 1, 2)

        self.label_unique = QLabel(self.group_2)
        self.label_unique.setText("Unique: ")
        self.label_unique.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_unique, 9, 0, 1, 1)

        self.label_unique_data = QLabel(self.group_2)
        self.label_unique_data.setText("")
        self.label_unique_data.setWordWrap(True)
        self.label_unique_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_unique_data, 9, 1, 1, 1)

        self.line_15 = QFrame(self.group_2)
        self.line_15.setFrameShape(QFrame.HLine)
        self.line_15.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_15, 10, 0, 1, 2)

        self.label_first = QLabel(self.group_2)
        self.label_first.setText("First: ")
        self.label_first.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_first, 11, 0, 1, 1)

        self.label_first_data = QLabel(self.group_2)
        self.label_first_data.setText("")
        self.label_first_data.setWordWrap(True)
        self.label_first_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_first_data, 11, 1, 1, 1)

        self.line_16 = QFrame(self.group_2)
        self.line_16.setFrameShape(QFrame.HLine)
        self.line_16.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_16, 12, 0, 1, 2)

        self.label_last = QLabel(self.group_2)
        self.label_last.setText("Last: ")
        self.label_last.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_last, 13, 0, 1, 1)

        self.label_last_data = QLabel(self.group_2)
        self.label_last_data.setText("")
        self.label_last_data.setWordWrap(True)
        self.label_last_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_last_data, 13, 1, 1, 1)

        self.line_17 = QFrame(self.group_2)
        self.line_17.setFrameShape(QFrame.HLine)
        self.line_17.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_17, 14, 0, 1, 2)

        self.label_low = QLabel(self.group_2)
        self.label_low.setText("Low Outlier Count: ")
        self.label_low.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_low, 15, 0, 1, 1)

        self.label_low_data = QLabel(self.group_2)
        self.label_low_data.setText("")
        self.label_low_data.setWordWrap(True)
        self.label_low_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_low_data, 15, 1, 1, 1)

        self.line_18 = QFrame(self.group_2)
        self.line_18.setFrameShape(QFrame.HLine)
        self.line_18.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_18, 16, 0, 1, 2)

        self.label_high = QLabel(self.group_2)
        self.label_high.setText("High Outlier Count: ")
        self.label_high.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_high, 17, 0, 1, 1)

        self.label_high_data = QLabel(self.group_2)
        self.label_high_data.setText("")
        self.label_high_data.setWordWrap(True)
        self.label_high_data.setAlignment(Qt.AlignCenter)
        self.grid_2.addWidget(self.label_high_data, 17, 1, 1, 1)

        self.line_20 = QFrame(self.group_2)
        self.line_20.setFrameShape(QFrame.HLine)
        self.line_20.setFrameShadow(QFrame.Sunken)
        self.grid_2.addWidget(self.line_20, 18, 0, 1, 2)

        self.layout.addWidget(self.group_2, 0, 2, 1, 1)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.layout.addItem(self.v_spacer_1, 0, 3, 1, 1)

        self.graph_widget = pg.PlotWidget(parent=self.dialog)
        self.graph_widget.setBackground('w')
        self.layout.addWidget(self.graph_widget, 1, 1, 1, 2)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.layout.addItem(self.v_spacer_2, 1, 3, 1, 1)

        self.h_spacer_1 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.layout.addItem(self.h_spacer_1, 2, 1, 1, 1)

        self.h_spacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.layout.addItem(self.h_spacer_2, 2, 2, 1, 1)

        self.dialog.setWindowTitle("Describe")

        self.dialog.exec()

    def get_variable_names(self):
        """
        Gets a list of column names
        """
        for i, name in enumerate(self.tab.data.columns):
            self.variables.append(name)
            self.show_variables[name] = [i, True, 0, self.tab.data.dtypes[name]]

    def setup_table(self):
        """
        Prints the variable names and dtypes to the table
        """
        self.table.insertRow(0)
        search_layout_widget = QWidget()
        self.search_term = ""
        self.description = self.tab.data.describe()
        search_layout = QGridLayout(search_layout_widget)
        self.search_bar = QLineEdit()
        label = QLabel(search_layout_widget)
        label.setText("Search Column Name")
        search_layout.addWidget(label, 0, 0, 1, 1)
        search_layout.addWidget(self.search_bar, 0, 1, 1, 1)
        self.search_bar.textChanged.connect(self.filter)
        self.table.setCellWidget(0, 0, search_layout_widget)
        self.table.setItem(0, 1, QTableWidgetItem("Data Type"))
        self.table.setRowHeight(0, 50)

        self.print_table()

        self.table.horizontalHeader().setVisible(False)
        self.table.verticalHeader().setVisible(False)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.table.resizeColumnsToContents()
        self.table.cellClicked.connect(self.set_focus)

    def print_table(self):
        """
        Prints the table containing the columns and their datatypes
        """
        unsupported_counter = 0
        for i, name in enumerate(self.show_variables):
            self.table.insertRow(i + 1)
            self.table.setItem(i + 1, 0, QTableWidgetItem(name))
            self.table.setItem(i + 1, 1, QTableWidgetItem(str(self.show_variables[name][3])))
            if str(self.show_variables[name][3]) in self.invalid:
                unsupported_counter += 1
                self.show_variables[name][1] = False
            self.show_variables[name][2] = unsupported_counter

    def filter(self, value):
        """
        Filter the table based on the search term
        :param value: input from the search bar
        """
        self.search_term = value
        self.show_variables = {}
        for i, var in enumerate(self.variables):
            if self.search_term.upper() in var.upper():
                self.show_variables[var] = [i, True, 0, self.tab.data.dtypes[var]]
        for row in reversed(range(1, self.table.rowCount())):
            self.table.removeRow(row)
        self.print_table()

    def set_focus(self, value):
        """
        Function to change the data in the right side of the dialog to match the table selection
        :param value: Row from dtype_table that the data belongs
        """
        self.label_25_data.setText("")
        self.label_50_data.setText("")
        self.label_75_data.setText("")
        self.label_max_data.setText("")
        self.label_mean_data.setText("")
        self.label_min_data.setText("")
        self.label_std_data.setText("")
        self.label_sum_data.setText("")

        self.label_count_data.setText("")
        self.label_null_data.setText("")
        self.label_median_data.setText("")
        self.label_mode_data.setText("")
        self.label_unique_data.setText("")
        self.label_first_data.setText("")
        self.label_last_data.setText("")
        self.label_low_data.setText("")
        self.label_high_data.setText("")
        if value != 0:
            name = self.table.item(value, 0).text()
            #  Selects the cell to the right of the selected cell to give the appearance of the whole row being selected
            self.table.item(value, 1).setSelected(True)
            series = self.tab.data[name]
            count = 0
            d25 = 0
            d50 = 0
            d75 = 0
            minimum = 0
            maximum = 0
            try:
                self.label_mode_data.setText(str(series.mode()[0]))
            except IndexError:
                pass
            try:
                if str(series.dtype) not in ["object", "Object", "String", "string"]:
                    self.label_sum_data.setText(str(series.sum()))
            except (ValueError, TypeError):
                pass

            if str(series.dtype) == "datetime64[ns]":
                series = series.dt.year

            describe_data = series.describe()

            for i, index in enumerate(describe_data.index):
                if index == "count":
                    self.label_count_data.setText(str(describe_data.iat[i]))
                    count = int(describe_data.iat[i])
                elif index == "unique":
                    self.label_unique_data.setText(str(describe_data.iat[i]))
                elif index == "mean":
                    self.label_mean_data.setText(str(describe_data.iat[i]))
                elif index == "std":
                    self.label_std_data.setText(str(describe_data.iat[i]))
                elif index == "min":
                    self.label_min_data.setText(str(describe_data.iat[i]))
                    minimum = describe_data.iat[i]
                elif index == "max":
                    self.label_max_data.setText(str(describe_data.iat[i]))
                    maximum = describe_data.iat[i]
                elif index == "25%":
                    self.label_25_data.setText(str(describe_data.iat[i]))
                    d25 = describe_data.iat[i]
                elif index == "50%":
                    self.label_50_data.setText(str(describe_data.iat[i]))
                    d50 = describe_data.iat[i]
                elif index == "75%":
                    self.label_75_data.setText(str(describe_data.iat[i]))
                    d75 = describe_data.iat[i]

            self.label_null_data.setText(str(series.isna().sum()))
            self.label_median_data.setText(str(series.iat[floor(count / 2)]))
            self.label_unique_data.setText(str(series.nunique(dropna=False)))
            self.label_first_data.setText(str(series.iat[0]))
            self.label_last_data.setText(str(series.iat[count - 1]))

            outlier_range = 1.5 * (d75 - d25)
            high_outliers = pd.Series(dtype='float64')
            low_outliers = pd.Series(dtype='float64')
            width = maximum - minimum
            if minimum < d25 - outlier_range:
                minimum = d25 - outlier_range
                low_outliers = series[series < minimum]
                self.label_low_data.setText(str(len(low_outliers.index)))
            else:
                self.label_low_data.setText("0")
            if maximum > d75 + outlier_range:
                maximum = d75 + outlier_range
                high_outliers = series[series > maximum]
                self.label_high_data.setText(str(len(high_outliers.index)))
            else:
                self.label_high_data.setText("0")

            self.graph_widget.clear()
            self.graph_widget.setLabel('left', "<span>{column_name}<span>".format(column_name=name))
            self.graph_widget.getAxis('left').setStyle(showValues=False)
            self.graph_widget.showGrid(y=True)
            try:
                self.box_and_whisker = BoxAndWhiskerPlot(
                    [minimum, d25, d50, d75, maximum, low_outliers, high_outliers, width], QtGui.QColor(0, 0, 255))
                self.graph_widget.addItem(self.box_and_whisker)
            except TypeError:
                #   Type of values passed to function is not int, added because TimeDelta caused a crash
                pass


# noinspection PyUnboundLocalVariable
class CopyColumnDialog:
    def __init__(self, tab, col):
        """
        Initializes a copy column dialog
        :param tab: The tab it is being initialized from
        :param col: the column to copy
        """
        super(CopyColumnDialog, self).__init__()
        self.tab = tab
        self.col = col
        self.dialog = QDialog()
        self.dialog.resize(520, 220)
        self.vertical_layout = QVBoxLayout(self.dialog)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vertical_layout.addItem(self.v_spacer_1)

        self.grid_layout = QGridLayout()

        self.tab_combo = QComboBox(self.dialog)
        self.existing_tab = QCheckBox(self.dialog)
        self.existing_tab.setText("Add to Existing Tab")
        self.grid_layout.addWidget(self.existing_tab, 0, 0, 1, 1)
        self.existing_tab.stateChanged.connect(lambda val: self.tab_combo.setEnabled(self.existing_tab.isChecked()))

        self.tab_combo.addItem("Select...")
        for i in range(len(win.data_tabs)):
            if i != win.data_tabs.currentIndex():
                self.tab_combo.addItem(win.data_tabs.widget(i).name)
        self.tab_combo.setEnabled(False)
        self.grid_layout.addWidget(self.tab_combo, 0, 1, 1, 1)

        self.v_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_3, 1, 0, 1, 1)

        self.label = QLabel(self.dialog)
        self.label.setText("By default clicking 'OK' will add column to a new tab called 'Column'")
        self.label.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.label, 2, 0, 1, 2)

        self.vertical_layout.addLayout(self.grid_layout)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vertical_layout.addItem(self.v_spacer_2)

        self.button_layout = QHBoxLayout()

        self.ok_button = QPushButton(self.dialog)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)
        self.button_layout.addWidget(self.ok_button)

        self.cancel_button = QPushButton(self.dialog)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.button_layout.addWidget(self.cancel_button)

        self.vertical_layout.addLayout(self.button_layout)

        self.dialog.setWindowTitle("Copy Column")

        self.dialog.exec()

    def apply(self):
        """
        Adds the variables to either a new tab or an existing tab
        """
        index = self.tab_combo.currentIndex()
        if not self.existing_tab.isChecked():
            #   Create a new tab
            tab = CustomTab("Column", "data")
            tab.set_data(self.tab.data[self.col].copy().to_frame())
            tab.reset_print_area()
            self.dialog.accept()
        else:
            if index != 0:
                #   Add to existing tab
                for i in range(len(win.data_tabs)):
                    if win.data_tabs.widget(i).name == self.tab_combo.currentText():
                        tab = win.data_tabs.widget(i)
                series = self.tab.data[self.col]
                name = series.name
                tab.data = tab.data.assign(variable_temp_name=series)
                if len(tab.data.index) > len(series.index):
                    warning_dialog(
                        "Row counts don't match. (Source: {source}, Destination: {dest}). Padding source column with nans".format(
                            source=len(series.index), dest=len(tab.data.index)))
                elif len(tab.data.index) < len(series.index):
                    warning_dialog(
                        "Row counts don't match. (Source: {source}, Destination: {dest}). Cropping source column".format(
                            source=len(series.index), dest=len(tab.data.index)))
                if name not in tab.data.columns:
                    tab.data = tab.data.rename(columns={"variable_temp_name": name})
                    tab.column_formatting[name] = {"Rounding": 3, "Red Negatives": False}
                    tab.column_list.append(name)
                else:
                    counter = 0
                    for col in tab.data.columns:
                        if name in col:
                            counter += 1
                    new_name = "{name} ({val})".format(name=name, val=counter)
                    warning_dialog(
                        "Column by name: {name} already in dataframe. Renaming new Column {new_name}.".format(name=name,
                                                                                                              new_name=new_name))
                    tab.data = tab.data.rename(columns={"variable_temp_name": new_name})
                    tab.column_formatting[new_name] = {"Rounding": 3, "Red Negatives": False}
                    tab.column_list.append(new_name)
                tab.info["Columns"] += 1
                tab.reset_print_area()
                self.dialog.accept()
            else:
                warning_dialog("No Tab Selected")


class CopyRowDialog:
    def __init__(self, tab, rows):
        """
        Initializes a copy row dialog
        :param tab: The tab it is being initialized from
        :param rows: the rows to copy
        """
        super(CopyRowDialog, self).__init__()
        self.tab = tab
        self.rows = rows
        self.dialog = QDialog()
        self.dialog.resize(520, 220)
        self.vertical_layout = QVBoxLayout(self.dialog)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vertical_layout.addItem(self.v_spacer_1)

        self.grid_layout = QGridLayout()

        self.tab_combo = QComboBox(self.dialog)
        self.existing_tab = QCheckBox(self.dialog)
        self.existing_tab.setText("Add to Existing Tab")
        self.grid_layout.addWidget(self.existing_tab, 0, 0, 1, 1)
        self.existing_tab.stateChanged.connect(lambda val: self.tab_combo.setEnabled(self.existing_tab.isChecked()))

        self.tab_combo.addItem("Select...")
        for i in range(len(win.data_tabs)):
            self.tab_combo.addItem(win.data_tabs.widget(i).name)
        self.tab_combo.setEnabled(False)
        self.grid_layout.addWidget(self.tab_combo, 0, 1, 1, 1)

        self.v_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_3, 1, 0, 1, 1)

        self.label = QLabel(self.dialog)
        self.label.setText("By default clicking 'OK' will add rows to a new tab called 'Sample'")
        self.label.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.label, 2, 0, 1, 2)

        self.vertical_layout.addLayout(self.grid_layout)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vertical_layout.addItem(self.v_spacer_2)

        self.button_layout = QHBoxLayout()

        self.ok_button = QPushButton(self.dialog)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)
        self.button_layout.addWidget(self.ok_button)

        self.cancel_button = QPushButton(self.dialog)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.button_layout.addWidget(self.cancel_button)

        self.vertical_layout.addLayout(self.button_layout)

        if len(self.rows) == 1:
            self.dialog.setWindowTitle("Copy Row")
        else:
            self.dialog.setWindowTitle("Copy Rows")

        self.dialog.exec()

    def apply(self):
        """
        Adds the variables to either a new tab or an existing tab
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        try:
            if not self.existing_tab.isChecked():
                tab = CustomTab("Sample", "data")
                tab.set_data(pd.DataFrame(columns=self.tab.data.columns))
            else:
                tab = win.data_tabs.widget(self.tab_combo.currentIndex() - 1)
                if len(self.tab.data.columns) != len(tab.data.columns):
                    raise ValueError("Destination columns don't match source columns")
            for row in self.rows:
                tab.data = tab.data.append(self.tab.data.iloc[row, :])
            if tab.info["Rows"] != 0:
                tab.info["Rows"] += len(self.rows)
            tab.reset_index()
            tab.reset_print_area()
            self.dialog.accept()
        except ValueError as e:
            warning_dialog(str(e))
        QApplication.restoreOverrideCursor()


class AddTabDialog:
    def __init__(self):
        """
        Create an add tab dialog
        """
        super(AddTabDialog, self).__init__()
        self.result = []
        self.dialog = QDialog()
        self.dialog.setFixedSize(400, 200)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 380, 180)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer, 0, 0, 1, 1)

        self.label = QLabel(self.grid_layout_widget)
        self.label.setText("Tab Name: ")
        self.label.setAlignment(Qt.AlignRight | Qt.AlignTrailing | Qt.AlignVCenter)
        self.grid_layout.addWidget(self.label, 1, 0, 1, 1)

        self.input = QLineEdit(self.grid_layout_widget)
        self.input.setPlaceholderText("Name...")
        self.input.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.grid_layout.addWidget(self.input, 1, 1, 1, 1)

        self.v_spacer2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer2, 2, 0, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)
        self.grid_layout.addWidget(self.ok_button, 3, 0, 1, 1)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.apply)
        self.grid_layout.addWidget(self.cancel_button, 3, 1, 1, 1)

        self.dialog.setWindowTitle("Add Tab")

        self.dialog.exec()

    def apply(self):
        """
        Sets the input text to the result variable, this is accessed and applied outside this function
        """
        if len(self.input.text()) > 0:
            self.result = [True, self.input.text()]
            self.dialog.accept()
        else:
            self.result = [False, None]
            self.dialog.accept()


class FilterDialog:
    def __init__(self, tab):
        """
        Initializes a filter dialog
        :param tab: tab that it has been initialized from. Needed to access the dataframe
        """
        super(FilterDialog, self).__init__()
        self.tab = tab
        self.filter_count = len(self.tab.active_filters)
        self.dialog = QDialog()
        self.dialog.resize(660, 480)
        self.main_layout = QVBoxLayout(self.dialog)

        self.int_dtypes = ['int8', 'int16', 'int32', 'int64', 'uint8', 'uint16', 'uint32', 'uint64', 'float32',
                           'float64', 'float', 'int', 'Int64', 'Int', 'Float', 'Float64']

        self.comparators = ['Less Than', 'Greater Than', 'Greater Than or Equal To', 'Less Than or Equal To',
                            'Equal To']
        self.date_comparators = ['Before', 'After', 'Between', 'Between Inclusive']
        self.string_comparators = ['Contains', "Doesn't Contain", "Equal To", "Not Equal To", 'Starts With',
                                   'Ends With']

        self.vertical_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_1)

        self.num_radio = QRadioButton(self.dialog)
        self.num_radio.setText("Numbers")
        self.num_radio.toggled.connect(lambda val: self.number_group.setEnabled(val))
        self.num_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.num_radio)

        self.number_group = QGroupBox(self.dialog)
        self.number_group.setTitle("Numbers")
        self.number_group.setEnabled(False)
        self.vertical_layout_num = QVBoxLayout(self.number_group)
        self.number_grid = QGridLayout()
        self.num_col_dropdown = QComboBox(self.number_group)
        self.number_grid.addWidget(self.num_col_dropdown, 0, 0, 1, 1)
        self.num_operators = QComboBox(self.number_group)
        self.num_operators.addItems(self.comparators)
        self.number_grid.addWidget(self.num_operators, 0, 1, 1, 1)
        self.num_input = QLineEdit(self.number_group)
        self.number_grid.addWidget(self.num_input, 0, 2, 1, 1)
        self.num_col_2 = QComboBox(self.number_group)
        self.num_col_2.setEnabled(False)
        self.number_grid.addWidget(self.num_col_2, 1, 2, 1, 1)
        self.num_compare = QCheckBox(self.number_group)
        self.num_compare.setText("Compare to Column")
        self.num_compare.stateChanged.connect(lambda val: self.number_input_swap(val))
        self.number_grid.addWidget(self.num_compare, 1, 1, 1, 1)
        self.num_h_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.number_grid.addItem(self.num_h_spacer, 1, 0, 1, 1)
        self.vertical_layout_num.addLayout(self.number_grid)

        self.main_layout.addWidget(self.number_group)

        self.vertical_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_3)

        self.str_radio = QRadioButton(self.dialog)
        self.str_radio.setText("Strings")
        self.str_radio.toggled.connect(lambda val: self.string_group.setEnabled(val))
        self.str_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.str_radio)

        self.string_group = QGroupBox(self.dialog)
        self.string_group.setTitle("Strings")
        self.string_group.setEnabled(False)
        self.vertical_layout_str = QVBoxLayout(self.string_group)
        self.string_grid = QGridLayout()
        self.string_col_dropdown = QComboBox(self.string_group)
        self.string_grid.addWidget(self.string_col_dropdown, 0, 0, 1, 1)
        self.string_operations = QComboBox(self.string_group)
        self.string_operations.addItems(self.string_comparators)
        self.string_operations.currentTextChanged.connect(self.string_operator_changed)
        self.string_grid.addWidget(self.string_operations, 0, 1, 1, 1)
        self.string_input = QLineEdit(self.string_group)
        self.string_grid.addWidget(self.string_input, 0, 2, 1, 1)
        self.case_checkbox = QCheckBox(self.string_group)
        self.case_checkbox.setText("Case Sensitive")
        self.string_grid.addWidget(self.case_checkbox, 1, 2, 1, 1)
        self.string_h_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.string_grid.addItem(self.string_h_spacer, 1, 0, 1, 1)
        self.vertical_layout_str.addLayout(self.string_grid)

        self.main_layout.addWidget(self.string_group)

        self.vertical_spacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_4)

        self.date_radio = QRadioButton(self.dialog)
        self.date_radio.setText("Dates")
        self.date_radio.toggled.connect(lambda val: self.date_group.setEnabled(val))
        self.date_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.date_radio)

        self.date_group = QGroupBox(self.dialog)
        self.date_group.setTitle("Dates")
        self.date_group.setEnabled(False)
        self.vertical_layout_date = QVBoxLayout(self.date_group)
        self.date_grid = QGridLayout()
        self.date_dropdown = QComboBox(self.date_group)
        self.date_grid.addWidget(self.date_dropdown, 0, 0, 1, 1)
        self.date_operations = QComboBox(self.date_group)
        self.date_operations.addItems(self.date_comparators)
        self.date_operations.currentTextChanged.connect(self.date_between)
        self.date_grid.addWidget(self.date_operations, 0, 1, 1, 1)
        self.date_compare = QDateTimeEdit(self.date_group)
        self.date_compare.dateTimeChanged.connect(self.set_between_date)
        self.date_grid.addWidget(self.date_compare, 0, 2, 1, 1)
        self.date_compare_between = QDateTimeEdit(self.date_group)
        self.date_compare_between.setEnabled(False)
        self.date_grid.addWidget(self.date_compare_between, 1, 2, 1, 1)
        self.vertical_layout_date.addLayout(self.date_grid)

        self.main_layout.addWidget(self.date_group)

        self.vertical_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_2)

        self.active_filter_group = QGroupBox(self.dialog)
        self.active_filter_group.setTitle("Active Filters")
        self.filter_grid = QGridLayout(self.active_filter_group)
        self.print_active_filters()

        self.main_layout.addWidget(self.active_filter_group)

        self.vertical_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_3)

        self.h_layout = QHBoxLayout()

        self.apply_button = QPushButton("Apply")
        self.apply_button.clicked.connect(self.apply)
        self.apply_button.setIcon(app_context.ok_icon)
        self.apply_button.setEnabled(False)
        self.h_layout.addWidget(self.apply_button)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.h_layout.addWidget(self.cancel_button)

        self.main_layout.addLayout(self.h_layout)

        self.fill_comboboxes()

        self.dialog.setWindowTitle("Filter")

        self.dialog.exec()

    def enable_apply_button(self):
        """
        Enables the apply button
        Called when one of the radio buttons has been selected
        """
        self.apply_button.setEnabled(True)

    def print_active_filters(self):
        """
        Prints labels with the current filters in them to the active filter box
        """
        for i, f in enumerate(self.tab.active_filters):
            label = QLabel(str(f))
            label.setAlignment(Qt.AlignCenter)
            self.filter_grid.addWidget(label, i, 0, 1, 1)

    def number_input_swap(self, val):
        """
        Swaps which number input mode is enabled based on if filtering by column instead of value
        :param val: check state of the column checkbox
        """
        self.num_input.setEnabled(not val)
        self.num_col_2.setEnabled(val)

    def set_between_date(self, val):
        """
        Sets the date on the second date input box to the current date in the first
        This prevents filtering between 2 dates when the second is before the first
        :param val: Current date in date input
        """
        if self.date_compare_between.isEnabled():
            self.date_compare_between.setMinimumDateTime(val)

    def string_operator_changed(self, text):
        """
        Disables the case checkbox and sets it to checked if the operation is Ends With or Starts With
        As the pandas functions for the operations don't support case sensitive searching
        :param text: the current text in the operation dropdown
        """
        if "With" in text:
            self.case_checkbox.setChecked(True)
            self.case_checkbox.setEnabled(False)
        else:
            self.case_checkbox.setEnabled(True)

    def fill_comboboxes(self):
        """
        Fills the combo boxes with items that match the specifc types
        """
        for col in self.tab.data.columns:
            dtype = str(self.tab.data[col].dtype)
            if dtype in self.int_dtypes:
                self.num_col_dropdown.addItem(str(col))
                self.num_col_2.addItem(str(col))
            if dtype in ["string", "object", "String", "Object"]:
                self.string_col_dropdown.addItem(str(col))
            if dtype == "datetime64[ns]":
                self.date_dropdown.addItem(str(col))

    def date_between(self, text):
        """
        Enables the second date selection input based on the selected operation
        :param text: The current operation
        """
        if "Between" in text:
            self.date_compare_between.setEnabled(True)
        else:
            self.date_compare_between.setEnabled(False)

    def apply(self):
        """
        Apply the selected filtering to the tabs data
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        #  Filter is being applied to a number column
        if self.filter_count == len(self.tab.active_filters):
            data_source = self.tab.data
        else:
            data_source = self.tab.data_backup
        if self.num_radio.isChecked():
            if len(self.num_col_dropdown) > 0:
                #  Comparing two columns
                if self.num_compare.isChecked():
                    if self.num_operators.currentText() == 'Greater Than':
                        self.filtered_data = data_source[
                            data_source[self.num_col_dropdown.currentText()] > data_source[
                                self.num_col_2.currentText()]]
                    elif self.num_operators.currentText() == 'Less Than':
                        self.filtered_data = data_source[
                            data_source[self.num_col_dropdown.currentText()] < data_source[
                                self.num_col_2.currentText()]]
                    elif self.num_operators.currentText() == 'Greater Than or Equal To':
                        self.filtered_data = data_source[
                            data_source[self.num_col_dropdown.currentText()] >= data_source[
                                self.num_col_2.currentText()]]
                    elif self.num_operators.currentText() == 'Less Than or Equal To':
                        self.filtered_data = data_source[
                            data_source[self.num_col_dropdown.currentText()] <= data_source[
                                self.num_col_2.currentText()]]
                    elif self.num_operators.currentText() == 'Equal To':
                        self.filtered_data = data_source[
                            data_source[self.num_col_dropdown.currentText()] == data_source[
                                self.num_col_2.currentText()]]
                    if self.filtered_data.empty:
                        warning_dialog("Filter Results in Empty DataFrame")
                    else:
                        self.tab.active_filters.append(
                            [self.num_col_dropdown.currentText(), self.num_operators.currentText(),
                             self.num_col_2.currentText()])
                        self.tab.data = self.filtered_data
                        self.tab.reset_index()
                        self.tab.reset_print_area()
                #  Comparing column to value
                else:
                    #  Try block catches ValueErrors when non int values are tried to be cast to ints
                    try:
                        if self.num_operators.currentText() == 'Greater Than':
                            self.filtered_data = data_source[
                                data_source[self.num_col_dropdown.currentText()] > float(self.num_input.text())]
                        elif self.num_operators.currentText() == 'Less Than':
                            self.filtered_data = data_source[
                                data_source[self.num_col_dropdown.currentText()] < float(self.num_input.text())]
                        elif self.num_operators.currentText() == 'Greater Than or Equal To':
                            self.filtered_data = data_source[
                                data_source[self.num_col_dropdown.currentText()] >= float(self.num_input.text())]
                        elif self.num_operators.currentText() == 'Less Than or Equal To':
                            self.filtered_data = data_source[
                                data_source[self.num_col_dropdown.currentText()] >= float(self.num_input.text())]
                        elif self.num_operators.currentText() == 'Equal To':
                            self.filtered_data = data_source[
                                data_source[self.num_col_dropdown.currentText()] == float(self.num_input.text())]
                        if self.filtered_data.empty:
                            warning_dialog("Filter Results in Empty DataFrame")
                        else:
                            self.tab.active_filters.append(
                                [self.num_col_dropdown.currentText(), self.num_operators.currentText(),
                                 self.num_input.text()])
                            self.tab.data = self.filtered_data
                            self.tab.reset_index()
                            self.tab.reset_print_area()
                    except ValueError as e:
                        warning_dialog(str(e))
            else:
                QApplication.restoreOverrideCursor()
                warning_dialog("No Number Columns In Dataframe")
        #  Filter is being applied to a string column
        elif self.str_radio.isChecked():
            if len(self.string_col_dropdown) > 0:
                if self.string_operations.currentText() == "Contains":
                    if self.string_input.text().upper() == "NAN":
                        self.filtered_data = data_source[data_source[self.string_col_dropdown.currentText()].isna()]
                    else:
                        data_source = data_source.dropna()
                        if self.case_checkbox.isChecked():
                            self.filtered_data = data_source[data_source[
                                self.string_col_dropdown.currentText()].str.contains(self.string_input.text(),
                                                                                     case=True)]
                        else:
                            self.filtered_data = data_source[data_source[
                                self.string_col_dropdown.currentText()].str.contains(self.string_input.text(),
                                                                                     case=False)]
                if self.string_operations.currentText() == "Doesn't Contain":
                    if self.string_input.text().upper() == "NAN":
                        self.filtered_data = data_source[data_source[self.string_col_dropdown.currentText()].notna()]
                    else:
                        data_source = data_source.dropna()
                        if self.case_checkbox.isChecked():
                            self.filtered_data = data_source[~data_source[
                                self.string_col_dropdown.currentText()].str.contains(self.string_input.text(),
                                                                                     case=True)]
                        else:
                            self.filtered_data = data_source[~data_source[
                                self.string_col_dropdown.currentText()].str.contains(self.string_input.text(),
                                                                                     case=False)]

                if self.string_operations.currentText() == "Equal To":
                    if self.string_input.text().upper() == "NAN":
                        self.filtered_data = data_source[data_source[self.string_col_dropdown.currentText()].isna()]
                    else:
                        data_source = data_source.dropna()
                        if self.case_checkbox.isChecked():
                            self.filtered_data = data_source[data_source[
                                self.string_col_dropdown.currentText()].str.match(self.string_input.text(),
                                                                                  case=True)]
                        else:
                            self.filtered_data = data_source[data_source[
                                self.string_col_dropdown.currentText()].str.match(self.string_input.text(),
                                                                                  case=False)]
                if self.string_operations.currentText() == "Not Equal To":
                    if self.string_input.text().upper() == "NAN":
                        self.filtered_data = data_source[data_source[self.string_col_dropdown.currentText()].notna()]
                    else:
                        data_source = data_source.dropna()
                        if self.case_checkbox.isChecked():
                            self.filtered_data = data_source[~data_source[
                                self.string_col_dropdown.currentText()].str.match(self.string_input.text(),
                                                                                  case=True)]
                        else:
                            self.filtered_data = data_source[~data_source[
                                self.string_col_dropdown.currentText()].str.match(self.string_input.text(),
                                                                                  case=False)]
                if self.string_operations.currentText() == "Starts With":
                    data_source = data_source.dropna()
                    self.filtered_data = data_source[data_source[
                        self.string_col_dropdown.currentText()].str.startswith(self.string_input.text())]
                if self.string_operations.currentText() == "Ends With":
                    data_source = data_source.dropna()
                    self.filtered_data = data_source[data_source[
                        self.string_col_dropdown.currentText()].str.endswith(self.string_input.text(), na=False)]
                if self.filtered_data.empty:
                    QApplication.restoreOverrideCursor()
                    warning_dialog("Filter Results in Empty DataFrame")
                else:
                    self.tab.active_filters.append(
                        [self.string_col_dropdown.currentText(), self.string_operations.currentText(),
                         self.string_input.text(), self.case_checkbox.isChecked()])
                    self.tab.data = self.filtered_data
                    self.tab.reset_index()
                    self.tab.reset_print_area()
            else:
                QApplication.restoreOverrideCursor()
                warning_dialog("No String Columns in Dataframe")
        #  Filter is being applied to date column
        elif self.date_radio.isChecked():
            if len(self.date_dropdown) > 0:
                if self.date_operations.currentText() == "After":
                    self.filtered_data = data_source[
                        data_source[self.date_dropdown.currentText()] > self.date_compare.dateTime().toPyDateTime()]
                elif self.date_operations.currentText() == "Before":
                    self.filtered_data = data_source[
                        data_source[self.date_dropdown.currentText()] < self.date_compare.dateTime().toPyDateTime()]
                elif self.date_operations.currentText() == "Between":
                    self.filtered_data = data_source[data_source[
                                                         self.date_dropdown.currentText()] > self.date_compare.dateTime().toPyDateTime()]
                    self.filtered_data = self.filtered_data[data_source[
                                                                self.date_dropdown.currentText()] < self.date_compare_between.dateTime().toPyDateTime()]
                elif self.date_operations.currentText() == "Between Inclusive":
                    self.filtered_data = data_source[data_source[
                                                         self.date_dropdown.currentText()] >= self.date_compare.dateTime().toPyDateTime()]
                    self.filtered_data = self.filtered_data[data_source[
                                                                self.date_dropdown.currentText()] <= self.date_compare_between.dateTime().toPyDateTime()]
                if self.filtered_data.empty:
                    warning_dialog("Filter Results in Empty DataFrame")
                else:
                    if self.date_operations.currentText() in ["After", "Before"]:
                        self.tab.active_filters.append(
                            [self.date_dropdown.currentText(), self.date_operations.currentText(),
                             self.date_compare.dateTime().toPyDateTime()])
                    else:
                        self.tab.active_filters.append(
                            [self.date_dropdown.currentText(), self.date_operations.currentText(),
                             self.date_compare.dateTime().toPyDateTime(),
                             self.date_compare_between.dateTime().toPyDateTime()])
                    self.tab.data = self.filtered_data
                    self.tab.reset_index()
                    self.tab.reset_print_area()
            else:
                QApplication.restoreOverrideCursor()
                warning_dialog("No Date Columns in Dataframe")
        QApplication.restoreOverrideCursor()


# noinspection PyUnboundLocalVariable
class CompareCombineDialog:
    def __init__(self, tab):
        """
        Initialise a Compare / Combine Dialog
        :param tab: The tab that the dialog is being called from
        """
        super(CompareCombineDialog, self).__init__()
        self.tab = tab
        self.dialog = QDialog()
        self.dialog.resize(660, 480)
        self.main_layout = QVBoxLayout(self.dialog)

        self.int_dtypes = ['int8', 'int16', 'int32', 'int64', 'uint8', 'uint16', 'uint32', 'uint64', 'float32',
                           'float64', 'float', 'int', 'Int64', 'Int', 'Float', 'Float64']

        self.operations_list = ['+', '-', '/', '*']
        self.date_operations_list = ['Time Between']
        self.string_operations_list = ['Concat', 'Concat With Space Between']

        self.vertical_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_1)

        self.num_radio = QRadioButton(self.dialog)
        self.num_radio.setText("Numbers")
        self.num_radio.toggled.connect(lambda val: self.number_group.setEnabled(val))
        self.num_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.num_radio)

        self.number_group = QGroupBox(self.dialog)
        self.number_group.setTitle("Numbers")
        self.number_group.setEnabled(False)
        self.vertical_layout_num = QVBoxLayout(self.number_group)
        self.number_grid = QGridLayout()
        self.num_col_dropdown = QComboBox(self.number_group)
        self.number_grid.addWidget(self.num_col_dropdown, 0, 0, 1, 1)
        self.num_operators = QComboBox(self.number_group)
        self.num_operators.addItems(self.operations_list)
        self.number_grid.addWidget(self.num_operators, 0, 1, 1, 1)
        self.num_col_2 = QComboBox(self.number_group)
        self.number_grid.addWidget(self.num_col_2, 0, 2, 1, 1)
        self.vertical_layout_num.addLayout(self.number_grid)

        self.main_layout.addWidget(self.number_group)

        self.vertical_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_3)

        self.str_radio = QRadioButton(self.dialog)
        self.str_radio.setText("Strings")
        self.str_radio.toggled.connect(lambda val: self.string_group.setEnabled(val))
        self.str_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.str_radio)

        self.string_group = QGroupBox(self.dialog)
        self.string_group.setTitle("Strings")
        self.string_group.setEnabled(False)
        self.vertical_layout_str = QVBoxLayout(self.string_group)
        self.string_grid = QGridLayout()
        self.string_col_dropdown = QComboBox(self.string_group)
        self.string_grid.addWidget(self.string_col_dropdown, 0, 0, 1, 1)
        self.string_operations = QComboBox(self.string_group)
        self.string_operations.addItems(self.string_operations_list)
        self.string_grid.addWidget(self.string_operations, 0, 1, 1, 1)
        self.string_dropdown_2 = QComboBox(self.string_group)
        self.string_grid.addWidget(self.string_dropdown_2, 0, 2, 1, 1)
        self.vertical_layout_str.addLayout(self.string_grid)

        self.main_layout.addWidget(self.string_group)

        self.vertical_spacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_4)

        self.date_radio = QRadioButton(self.dialog)
        self.date_radio.setText("Dates")
        self.date_radio.toggled.connect(lambda val: self.date_group.setEnabled(val))
        self.date_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.date_radio)

        self.date_group = QGroupBox(self.dialog)
        self.date_group.setTitle("Dates")
        self.date_group.setEnabled(False)
        self.vertical_layout_date = QVBoxLayout(self.date_group)
        self.date_grid = QGridLayout()
        self.date_dropdown = QComboBox(self.date_group)
        self.date_grid.addWidget(self.date_dropdown, 0, 0, 1, 1)
        self.date_operations = QComboBox(self.date_group)
        self.date_operations.addItems(self.date_operations_list)
        self.date_grid.addWidget(self.date_operations, 0, 1, 1, 1)
        self.date_dropdown_2 = QComboBox(self.date_group)
        self.date_grid.addWidget(self.date_dropdown_2, 0, 2, 1, 1)
        self.vertical_layout_date.addLayout(self.date_grid)

        self.main_layout.addWidget(self.date_group)

        self.vertical_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_2)

        self.tab_group = QGroupBox(self.dialog)
        self.tab_group.setTitle("Add to tab")
        self.vertical_layout_tab = QVBoxLayout(self.tab_group)
        self.tab_grid = QGridLayout()
        self.tab_dropdown = QComboBox(self.tab_group)
        self.tab_grid.addWidget(self.tab_dropdown, 0, 1, 1, 1)
        self.tab_checkbox = QCheckBox(self.tab_group)
        self.tab_checkbox.setText("Add to Existing Tab")
        self.tab_checkbox.stateChanged.connect(lambda val: self.tab_dropdown.setEnabled(self.tab_checkbox.isChecked()))
        self.tab_checkbox.setChecked(True)
        self.tab_grid.addWidget(self.tab_checkbox, 0, 0, 1, 1)
        self.vertical_layout_tab.addLayout(self.tab_grid)

        self.main_layout.addWidget(self.tab_group)

        self.vertical_spacer_5 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_5)

        self.h_layout = QHBoxLayout()

        self.apply_button = QPushButton("Apply")
        self.apply_button.clicked.connect(self.apply)
        self.apply_button.setIcon(app_context.ok_icon)
        self.apply_button.setEnabled(False)
        self.h_layout.addWidget(self.apply_button)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.h_layout.addWidget(self.cancel_button)

        self.main_layout.addLayout(self.h_layout)

        self.fill_comboboxes()

        for i in range(len(win.data_tabs)):
            self.tab_dropdown.addItem(win.data_tabs.widget(i).name)
        self.tab_dropdown.setCurrentIndex(win.data_tabs.currentIndex())

        self.dialog.setWindowTitle("Compare  / Combine")

        self.dialog.exec()

    def enable_apply_button(self):
        """
        When a radio button has been clicked this will be called to enable the apply button
        """
        self.apply_button.setEnabled(True)

    def fill_comboboxes(self):
        """
        Function to add items into the combo boxes belonging to their datatypes
        """
        for col in self.tab.data.columns:
            dtype = str(self.tab.data[col].dtype)
            if dtype in self.int_dtypes:
                self.num_col_dropdown.addItem(str(col))
                self.num_col_2.addItem(str(col))
            if dtype in ["string", "object", "String", "object"]:
                self.string_col_dropdown.addItem(str(col))
                self.string_dropdown_2.addItem(str(col))
            if dtype == "datetime64[ns]":
                self.date_dropdown.addItem(str(col))
                self.date_dropdown_2.addItem(str(col))

    def apply(self):
        """
        Apply the selected operations
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self.num_radio.isChecked():
            col_1 = self.tab.data[self.num_col_dropdown.currentText()]
            col_2 = self.tab.data[self.num_col_2.currentText()]
            if self.num_operators.currentText() == '+':
                string = "{col_1} + {col_2}".format(col_1=self.num_col_dropdown.currentText(),
                                                    col_2=self.num_col_2.currentText())
                series = col_1 + col_2
            elif self.num_operators.currentText() == '-':
                string = "{col_1} - {col_2}".format(col_1=self.num_col_dropdown.currentText(),
                                                    col_2=self.num_col_2.currentText())
                series = col_1 - col_2
            elif self.num_operators.currentText() == '/':
                string = "{col_1} / {col_2}".format(col_1=self.num_col_dropdown.currentText(),
                                                    col_2=self.num_col_2.currentText())
                series = col_1 / col_2
            elif self.num_operators.currentText() == '*':
                string = "{col_1} * {col_2}".format(col_1=self.num_col_dropdown.currentText(),
                                                    col_2=self.num_col_2.currentText())
                series = col_1 * col_2
        elif self.str_radio.isChecked():
            string = "{col_1} & {col_2}".format(col_1=self.string_col_dropdown.currentText(),
                                                col_2=self.string_dropdown_2.currentText())
            col_1 = self.tab.data[self.string_col_dropdown.currentText()].apply(str)
            col_2 = self.tab.data[self.string_dropdown_2.currentText()].apply(str)
            if self.string_operations.currentText() == 'Concat':
                series = col_1 + col_2
            if self.string_operations.currentText() == 'Concat With Space Between':
                space_series = pd.Series(" ", index=col_1.index)
                series = col_1 + space_series + col_2
        elif self.date_radio.isChecked():
            if self.date_operations.currentText() == "Time Between":
                col_1 = pd.to_datetime(self.tab.data[self.date_dropdown.currentText()])
                col_2 = pd.to_datetime(self.tab.data[self.date_dropdown_2.currentText()])
                series = col_2 - col_1
                string = "Time Between {col_1} and {col_2}".format(col_1=self.date_dropdown.currentText(),
                                                                   col_2=self.date_dropdown_2.currentText())
        if not self.tab_checkbox.isChecked():
            tab = CustomTab("Column", "data")
            tab.set_data(series.to_frame())
            tab.reset_print_area()
        else:
            tab = win.data_tabs.widget(self.tab_dropdown.currentIndex())
            tab.data = tab.data.assign(operation_temp_name=series)
            if string not in tab.data.columns:
                tab.data = tab.data.rename(columns={"operation_temp_name": string})
            else:
                counter = 0
                for col in tab.data.columns:
                    if string in col:
                        counter += 1
                new_string = "{string} ({val})".format(string=string, val=counter)
                warning_dialog("Existing Column has name {string}. Naming it {new_string}.".format(string=string,
                                                                                                   new_string=new_string))
                tab.data = tab.data.rename(columns={"operation_temp_name": new_string})
            tab.column_formatting[string] = {"Red Negatives": False, "Rounding": 3}
            tab.column_list.append(string)
            tab.reset_print_area()
        QApplication.restoreOverrideCursor()


class TrimDialog:
    def __init__(self, tab):
        """
        Initializes a Trim dialog, used to round/trim/remove/replace/convert values
        :param tab:
        """
        super(TrimDialog, self).__init__()
        self.tab = tab
        self.dialog = QDialog()
        self.dialog.resize(660, 480)
        self.main_layout = QVBoxLayout(self.dialog)

        self.int_dtypes = ['int8', 'int16', 'int32', 'int64', 'uint8', 'uint16', 'uint32', 'uint64', 'float32',
                           'float64', 'float', 'int', 'Int64', 'Int', 'Float', 'Float64']

        self.num_op_list = ['Round', 'Round Up', 'Round Down']
        self.str_op_list = ['Trim', 'Remove', 'Replace']
        self.date_op_list = ['Trim', 'Convert']
        self.date_trim_list = ['DateTime', 'Year', 'Month', 'Day', 'Time', 'Hour', 'Minute', 'Second']
        self.date_convert_list = ['Week of The Year', 'Day of The Year', 'Day of Week', 'Quarter']

        self.vertical_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_1)

        self.num_radio = QRadioButton(self.dialog)
        self.num_radio.setText("Numbers")
        self.num_radio.toggled.connect(lambda val: self.number_group.setEnabled(val))
        self.num_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.num_radio)

        self.number_group = QGroupBox(self.dialog)
        self.number_group.setTitle("Numbers")
        self.number_group.setEnabled(False)
        self.vertical_layout_num = QVBoxLayout(self.number_group)
        self.number_grid = QGridLayout()
        self.num_col_dropdown = QComboBox(self.number_group)
        self.number_grid.addWidget(self.num_col_dropdown, 0, 0, 1, 1)
        self.num_operators = QComboBox(self.number_group)
        self.num_operators.addItems(self.num_op_list)
        self.num_operators.currentIndexChanged.connect(self.disable_num_spinbox)
        self.number_grid.addWidget(self.num_operators, 0, 1, 1, 1)
        self.num_input = QSpinBox(self.number_group)
        self.num_input.setMinimum(0)
        self.num_input.setMaximum(6)
        self.num_input.setToolTip("Decimal Places")
        self.number_grid.addWidget(self.num_input, 0, 2, 1, 1)
        self.num_apply_to_all = QCheckBox(self.number_group)
        self.num_apply_to_all.setText("Apply to All")
        self.num_apply_to_all.clicked.connect(lambda checked: self.num_col_dropdown.setEnabled(not checked))
        self.number_grid.addWidget(self.num_apply_to_all, 1, 0, 1, 1)
        self.decmial_places_label = QLabel(self.number_group)
        self.decmial_places_label.setText("Decimal Places")
        self.number_grid.addWidget(self.decmial_places_label, 1, 2, 1, 1)
        self.num_h_spacer = QSpacerItem(40, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.number_grid.addItem(self.num_h_spacer, 1, 2, 1, 1)

        self.num_h_spacer_2 = QSpacerItem(40, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.number_grid.addItem(self.num_h_spacer_2, 1, 1, 1, 1)

        self.num_h_spacer_3 = QSpacerItem(40, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.number_grid.addItem(self.num_h_spacer_3, 1, 0, 1, 1)
        self.vertical_layout_num.addLayout(self.number_grid)

        self.main_layout.addWidget(self.number_group)

        self.vertical_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_3)

        self.str_radio = QRadioButton(self.dialog)
        self.str_radio.setText("Strings")
        self.str_radio.toggled.connect(lambda val: self.string_group.setEnabled(val))
        self.str_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.str_radio)

        self.string_group = QGroupBox(self.dialog)
        self.string_group.setTitle("Strings")
        self.string_group.setEnabled(False)
        self.vertical_layout_str = QVBoxLayout(self.string_group)
        self.string_grid = QGridLayout()
        self.string_col_dropdown = QComboBox(self.string_group)
        self.string_grid.addWidget(self.string_col_dropdown, 0, 0, 1, 1)
        self.string_operations = QComboBox(self.string_group)
        self.string_operations.addItems(self.str_op_list)
        self.string_operations.currentIndexChanged.connect(self.change_string_input)
        self.string_grid.addWidget(self.string_operations, 0, 1, 1, 1)
        self.string_input = QLineEdit(self.string_group)
        self.string_input.setPlaceholderText("start:end eg. 0:3")
        self.string_grid.addWidget(self.string_input, 0, 2, 1, 1)
        self.string_replace = QLineEdit(self.string_group)
        self.string_grid.addWidget(self.string_replace, 1, 2, 1, 1)
        self.string_replace.setEnabled(False)
        self.string_apply_to_all = QCheckBox(self.string_group)
        self.string_apply_to_all.setText("Apply to All")
        self.string_apply_to_all.clicked.connect(lambda checked: self.string_col_dropdown.setEnabled(not checked))
        self.string_grid.addWidget(self.string_apply_to_all, 1, 0, 1, 1)
        self.string_h_spacer = QSpacerItem(40, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.string_grid.addItem(self.string_h_spacer, 2, 0, 1, 1)
        self.string_h_spacer2 = QSpacerItem(40, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.string_grid.addItem(self.string_h_spacer2, 2, 1, 1, 1)
        self.string_h_spacer3 = QSpacerItem(40, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.string_grid.addItem(self.string_h_spacer3, 2, 2, 1, 1)
        self.vertical_layout_str.addLayout(self.string_grid)

        self.main_layout.addWidget(self.string_group)

        self.vertical_spacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_4)

        self.date_radio = QRadioButton(self.dialog)
        self.date_radio.setText("Dates")
        self.date_radio.toggled.connect(lambda val: self.date_group.setEnabled(val))
        self.date_radio.toggled.connect(self.enable_apply_button)
        self.main_layout.addWidget(self.date_radio)

        self.date_group = QGroupBox(self.dialog)
        self.date_group.setTitle("Dates")
        self.date_group.setEnabled(False)
        self.vertical_layout_date = QVBoxLayout(self.date_group)
        self.date_grid = QGridLayout()
        self.date_dropdown = QComboBox(self.date_group)
        self.date_grid.addWidget(self.date_dropdown, 0, 0, 1, 1)
        self.date_operations = QComboBox(self.date_group)
        self.date_operations.addItems(self.date_op_list)
        self.date_operations.currentIndexChanged.connect(self.change_date_dropdowns)
        self.date_grid.addWidget(self.date_operations, 0, 1, 1, 1)
        self.date_compare = QComboBox(self.date_group)
        self.date_compare.addItems(self.date_trim_list)
        self.date_grid.addWidget(self.date_compare, 0, 2, 1, 1)
        self.vertical_layout_date.addLayout(self.date_grid)
        self.date_apply_to_all = QCheckBox(self.date_group)
        self.date_apply_to_all.setText("Apply to All")
        self.date_apply_to_all.clicked.connect(lambda checked: self.date_dropdown.setEnabled(not checked))
        self.date_grid.addWidget(self.date_apply_to_all, 1, 0, 1, 1)

        self.main_layout.addWidget(self.date_group)

        self.vertical_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.vertical_spacer_2)

        self.h_layout = QHBoxLayout()

        self.apply_button = QPushButton("Apply")
        self.apply_button.clicked.connect(self.apply)
        self.apply_button.setIcon(app_context.ok_icon)
        self.apply_button.setEnabled(False)
        self.h_layout.addWidget(self.apply_button)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.h_layout.addWidget(self.cancel_button)

        self.main_layout.addLayout(self.h_layout)

        self.fill_comboboxes()

        self.dialog.setWindowTitle("Trim")

        self.dialog.exec()

    def change_string_input(self):
        """
        Changes the placeholder and enables/disables the replace input based on the selected operation
        """
        if self.string_operations.currentText() == "Trim":
            self.string_input.setText("")
            self.string_input.setPlaceholderText("start:end eg. 0:3")
            self.string_replace.setEnabled(False)
            self.string_replace.setText("")
            self.string_replace.setPlaceholderText("")
        if self.string_operations.currentText() == "Remove":
            self.string_input.setText("")
            self.string_input.setPlaceholderText("Value to remove")
            self.string_replace.setEnabled(False)
            self.string_replace.setText("")
            self.string_replace.setPlaceholderText("")
        if self.string_operations.currentText() == "Replace":
            self.string_input.setText("")
            self.string_input.setPlaceholderText("Value to replace")
            self.string_replace.setEnabled(True)
            self.string_replace.setText("")
            self.string_replace.setPlaceholderText("Value to replace with")

    def disable_num_spinbox(self):
        """
        Enables or Disables the num spinbox based on if the current operation is round or not
        """
        if self.num_operators.currentText() == "Round":
            self.num_input.setEnabled(True)
            self.decmial_places_label.setEnabled(True)
        else:
            self.num_input.setEnabled(False)
            self.decmial_places_label.setEnabled(False)

    def enable_apply_button(self):
        """
        Enables the apply button.
        Called when one of the radio buttons has been selected
        """
        self.apply_button.setEnabled(True)

    def fill_comboboxes(self):
        """
        Empties and fills the combo boxes with columns of the matching datatypes
        """
        for item in reversed(range(len(self.num_col_dropdown))):
            self.num_col_dropdown.removeItem(item)
        for item in reversed(range(len(self.string_col_dropdown))):
            self.string_col_dropdown.removeItem(item)
        for item in reversed(range(len(self.date_dropdown))):
            self.date_dropdown.removeItem(item)
        for col in self.tab.data.columns:
            dtype = str(self.tab.data[col].dtype)
            if dtype in self.int_dtypes:
                self.num_col_dropdown.addItem(str(col))
            if dtype in ["string", "object", "String", "Object"]:
                self.string_col_dropdown.addItem(str(col))
            if dtype == "datetime64[ns]":
                self.date_dropdown.addItem(str(col))

    def change_date_dropdowns(self):
        """
        Changes the options in the third date dropdown based on the selection in the second
        """
        if self.date_operations.currentText() == "Trim":
            for item in reversed(range(len(self.date_compare))):
                self.date_compare.removeItem(item)
            self.date_compare.addItems(self.date_trim_list)
        elif self.date_operations.currentText() == "Convert":
            for item in reversed(range(len(self.date_compare))):
                self.date_compare.removeItem(item)
            self.date_compare.addItems(self.date_convert_list)

    def apply(self):
        """
        Apply the selected trimming to the tabs data
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self.num_radio.isChecked():
            if len(self.num_col_dropdown) > 0:
                if self.num_apply_to_all.isChecked():
                    for col in self.tab.data.columns:
                        if str(self.tab.data[col].dtype) in self.int_dtypes:
                            self.apply_number(col)
                else:
                    self.apply_number(self.num_col_dropdown.currentText())
                self.tab.reset_print_area()
            else:
                QApplication.restoreOverrideCursor()
                warning_dialog("No Number Columns In Dataframe")
        elif self.str_radio.isChecked():
            if len(self.string_col_dropdown) > 0:
                if self.string_apply_to_all.isChecked():
                    for col in self.tab.data.columns:
                        if str(self.tab.data[col].dtype) in ["string", "String", "Object", "object"]:
                            try:
                                self.apply_string(col)
                            except ValueError as e:
                                QApplication.restoreOverrideCursor()
                                if "invalid literal" in str(e):
                                    warning_dialog("Invalid Input. Format is: Start:End e.g 0:3 for first 4 digits.")
                                else:
                                    warning_dialog(str(e))
                                break
                else:
                    try:
                        self.apply_string(self.string_col_dropdown.currentText())
                    except ValueError as e:
                        QApplication.restoreOverrideCursor()
                        if "invalid literal" in str(e):
                            warning_dialog("Invalid Input. Format is: Start:End e.g 0:3 for first 4 digits.")
                        else:
                            warning_dialog(str(e))
                self.tab.reset_print_area()
            else:
                QApplication.restoreOverrideCursor()
                warning_dialog("No String Columns In Dataframe")
        elif self.date_radio.isChecked():
            if len(self.date_dropdown) > 0:
                if self.date_apply_to_all.isChecked():
                    for col in self.tab.data.columns:
                        if str(self.tab.data[col].dtype) == "datetime64[ns]":
                            self.apply_date(col)
                else:
                    self.apply_date(self.num_col_dropdown.currentText())
                self.tab.reset_print_area()
            else:
                QApplication.restoreOverrideCursor()
                warning_dialog("No Date Columns In Dataframe")
        self.fill_comboboxes()
        QApplication.restoreOverrideCursor()

    def apply_number(self, col):
        """
        Applies the number operations
        :param col: The column to apply the operations on
        """
        if self.num_operators.currentText() == 'Round':
            self.tab.data[col] = self.tab.data[col].round(self.num_input.value())
            self.tab.column_formatting[col]["Rounding"] = self.num_input.value()
        elif self.num_operators.currentText() == 'Round Up':
            for i, val in enumerate(self.tab.data[col]):
                self.tab.data[col].iat[i] = ceil(val)
            self.tab.column_formatting[col]["Rounding"] = 0
        elif self.num_operators.currentText() == 'Round Down':
            for i, val in enumerate(self.tab.data[col]):
                self.tab.data[col].iat[i] = floor(val)
            self.tab.column_formatting[col]["Rounding"] = 0

    def apply_string(self, col):
        """
        Applies the string operations
        :param col: The column to apply the operations on
        """
        if self.string_operations.currentText() == "Trim":
            split_input = self.string_input.text().split(":")
            start = int(split_input[0])
            end = int(split_input[1])
            if start < 0 or end < 0:
                raise ValueError("Input can't be less than 0")
            elif end < start:
                raise ValueError("End can't be less than Start")
            else:
                self.tab.data[col] = self.tab.data[col].str.slice(start=start, stop=end)
        elif self.string_operations.currentText() == "Remove":
            self.tab.data[col] = self.tab.data[col].str.replace(self.string_input.text(), '', regex=False)

        elif self.string_operations.currentText() == "Replace":
            self.tab.data[col] = self.tab.data[col].str.replace(self.string_input.text(),
                                                                self.string_replace.text(), regex=False)

    def apply_date(self, col):
        """
        Applies the date operations
        :param col: The column to apply the operations on
        """
        if self.date_operations.currentText() == "Trim":
            if self.date_compare.currentText() == "DateTime":
                self.tab.data[col] = pd.to_datetime(self.tab.data[col])
            if self.date_compare.currentText() == "Year":
                self.tab.data[col] = self.tab.data[col].dt.year
                self.tab.column_formatting[col]["Rounding"] = 0
            if self.date_compare.currentText() == "Month":
                self.tab.data[col] = self.tab.data[col].dt.month
                self.tab.column_formatting[col]["Rounding"] = 0
            if self.date_compare.currentText() == "Day":
                self.tab.data[col] = self.tab.data[col].dt.day
                self.tab.column_formatting[col]["Rounding"] = 0
            if self.date_compare.currentText() == "Time":
                self.tab.data[col] = self.tab.data[col].dt.time
            if self.date_compare.currentText() == "Hour":
                self.tab.data[col] = self.tab.data[col].dt.hour
            if self.date_compare.currentText() == "Minute":
                self.tab.data[col] = self.tab.data[col].dt.minute
            if self.date_compare.currentText() == "Second":
                self.tab.data[col] = self.tab.data[col].dt.second
        elif self.date_operations.currentText() == "Convert":
            if self.date_compare.currentText() == "Week of The Year":
                self.tab.data[col] = self.tab.data[col].dt.weekofyear
                self.tab.column_formatting[col]["Rounding"] = 0
            if self.date_compare.currentText() == "Day of The Year":
                self.tab.data[col] = self.tab.data[col].dt.dayofyear
                self.tab.column_formatting[col]["Rounding"] = 0
            if self.date_compare.currentText() == "Day of Week":
                self.tab.data[col] = self.tab.data[col].dt.dayofweek
                self.tab.column_formatting[col]["Rounding"] = 0
            if self.date_compare.currentText() == "Quarter":
                self.tab.data[col] = self.tab.data[col].dt.quarter
                self.tab.column_formatting[col]["Rounding"] = 0


class ConvertDialog:
    def __init__(self, tab):
        """
        Initializes a dialog used to convert datatypes in the dataframe
        :param tab:
        """
        super(ConvertDialog, self).__init__()
        self.tab = tab
        self.variables = []
        self.dialog = QDialog()
        self.dialog.resize(700, 640)
        self.main_layout = QVBoxLayout(self.dialog)

        self.grid_layout = QGridLayout()

        self.table = QTableWidget(self.dialog)
        self.grid_layout.addWidget(self.table, 0, 0, 7, 1)

        self.mode_group = QGroupBox(self.dialog)
        self.mode_group.setTitle("Mode")
        self.mode_group.setAlignment(Qt.AlignCenter)
        self.mode_grid = QGridLayout(self.mode_group)
        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.mode_grid.addItem(self.v_spacer_1, 0, 0, 1, 1)

        self.var_radio = QRadioButton(self.mode_group)
        self.var_radio.clicked.connect(self.mode_change)
        self.var_radio.setText("Variable")
        self.mode_grid.addWidget(self.var_radio, 1, 0, 1, 1)

        self.df_radio = QRadioButton(self.mode_group)
        self.df_radio.setText("DataFrame")
        self.df_radio.clicked.connect(self.mode_change)
        self.df_radio.setChecked(True)
        self.mode_grid.addWidget(self.df_radio, 1, 1, 1, 1)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.mode_grid.addItem(self.v_spacer_2, 2, 0, 1, 1)

        self.grid_layout.addWidget(self.mode_group, 0, 1, 1, 1)

        self.selected_variables = QTextBrowser(self.dialog)
        self.selected_variables.setText("")
        self.selected_variables.setVisible(False)
        self.selected_variables.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.selected_variables, 1, 1, 2, 1)

        self.v_spacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_3, 2, 1, 1, 1)

        self.convert_dropdown = QComboBox(self.dialog)
        self.convert_dropdown.addItems(["float64", "int64", "datetime64[ns]", "string"])
        self.convert_dropdown.setVisible(False)
        self.grid_layout.addWidget(self.convert_dropdown, 3, 1, 1, 1)

        self.convert_button = QPushButton(self.dialog)
        self.convert_button.setText("Convert")
        self.convert_button.setIcon(app_context.convert_icon)
        self.convert_button.setToolTip("Change DataTypes To Inferred DataTypes")
        self.convert_button.clicked.connect(self.convert)
        self.grid_layout.addWidget(self.convert_button, 4, 1, 1, 1)

        self.h_spacer_1 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.grid_layout.addItem(self.h_spacer_1, 5, 1, 1, 1)

        self.v_spacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_4, 6, 1, 1, 1)

        self.main_layout.addLayout(self.grid_layout)

        self.h_layout = QHBoxLayout()

        self.ok_button = QPushButton("OK")
        self.ok_button.clicked.connect(self.dialog.accept)
        self.ok_button.setIcon(app_context.ok_icon)
        self.h_layout.addWidget(self.ok_button)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.h_layout.addWidget(self.cancel_button)

        self.main_layout.addLayout(self.h_layout)

        self.dialog.setWindowTitle("Convert Data Types")

        self.get_variable_names()
        self.setup_table()

        self.dialog.exec()

    def mode_change(self):
        """
        Changes the convert mode from automatically converting the whole dataframe to individually selected variables
        """
        if self.var_radio.isChecked():
            self.convert_dropdown.setVisible(True)
            self.selected_variables.setVisible(True)
        elif self.df_radio.isChecked():
            self.convert_dropdown.setVisible(False)
            self.selected_variables.setVisible(False)

    def get_variable_names(self):
        """
        Puts the variables and their dtypes in a list
        """
        for name in self.tab.data.columns:
            self.variables.append(name)

    def setup_table(self):
        """
        Prints the variable names and dtypes to the dtype_table
        """
        self.table.insertColumn(0)
        self.table.insertColumn(0)
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem("Variable"))
        self.table.setHorizontalHeaderItem(1, QTableWidgetItem("DType"))
        self.table.setRowHeight(0, 60)

        self.print_table()

        self.table.verticalHeader().setVisible(False)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.table.itemSelectionChanged.connect(self.selection_changed)

    def print_table(self):
        """
        Fills the table with the variable/column names and their respective dtypes
        """
        for i, name in enumerate(self.variables):
            dtype = self.tab.data.dtypes[name]
            self.table.insertRow(i)
            self.table.setItem(i, 0, QTableWidgetItem(str(name)))
            self.table.setItem(i, 1, QTableWidgetItem(str(dtype)))
            self.table.item(i, 0).setTextAlignment(Qt.AlignCenter)
            self.table.item(i, 1).setTextAlignment(Qt.AlignCenter)
        self.table.resizeColumnsToContents()

    def selection_changed(self):
        """
        Changes the label to the selected variable names
        """
        selected = []
        for i in self.table.selectedItems():
            text = self.table.item(i.row(), 0).text()
            if text not in selected:
                selected.append(text)
        self.selected_variables.setText(str(selected))

    def convert(self):
        """
        Automatically convert all variables/columns to the inferred datatype
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self.var_radio.isChecked():
            selected = []
            for i in self.table.selectedItems():
                text = self.table.item(i.row(), 0).text()
                if text not in selected:
                    selected.append(text)
            for var in selected:
                try:
                    if self.convert_dropdown.currentText() == "string":
                        self.tab.data = self.tab.data.astype({var: 'object'})
                    else:
                        self.tab.data = self.tab.data.astype({var: self.convert_dropdown.currentText()})
                except (ValueError, TypeError):
                    warning_dialog("Cannot convert {variable} to selected data type".format(variable=var))
                    break
            for row in reversed(range(0, self.table.rowCount())):
                self.table.removeRow(row)
            self.print_table()
        elif self.df_radio.isChecked():
            try:
                self.tab.data = self.tab.data.convert_dtypes()
            except UnicodeEncodeError:
                pass
            for row in reversed(range(0, self.table.rowCount())):
                self.table.removeRow(row)
            self.print_table()
        QApplication.restoreOverrideCursor()


class HelpDialog:
    def __init__(self):
        """
        Creates a new help dialog to display the applications documentation
        """
        super(HelpDialog, self).__init__()
        self.dialog = QDialog()
        self.dialog.resize(1000, 700)
        self.main_layout = QHBoxLayout(self.dialog)
        self.grid_layout = QGridLayout()

        self.dialog.setWindowTitle("Help - Documentation")

        self.help_browser = QTextBrowser(self.dialog)
        self.help_browser.setSource(win.context.main_help_html)
        self.grid_layout.addWidget(self.help_browser, 0, 2, 1, 1)

        self.link_tree = QTreeWidget(self.dialog)
        self.link_tree.setSortingEnabled(False)
        self.link_tree.itemClicked.connect(self.change_help_html)
        self.link_tree.headerItem().setText(0, "Help")

        self.get_started = QTreeWidgetItem(self.link_tree)
        self.get_started.setText(0, "Getting Started")

        self.welcome_screen = QTreeWidgetItem(self.link_tree)
        self.welcome_screen.setText(0, "Welcome Screen")

        self.table_screen = QTreeWidgetItem(self.link_tree)
        self.table_screen.setText(0, "Table Screen")

        self.data_types = QTreeWidgetItem(self.link_tree)
        self.data_types.setText(0, "Data Types")

        self.describe = QTreeWidgetItem(self.link_tree)
        self.describe.setText(0, "Describe")

        self.filter = QTreeWidgetItem(self.link_tree)
        self.filter.setText(0, "Filter")

        self.trim = QTreeWidgetItem(self.link_tree)
        self.trim.setText(0, "Trim")

        self.sample = QTreeWidgetItem(self.link_tree)
        self.sample.setText(0, "Sample")

        self.formatting = QTreeWidgetItem(self.link_tree)
        self.formatting.setText(0, "Formatting")

        self.tabs = QTreeWidgetItem(self.link_tree)
        self.tabs.setText(0, "Tabs")

        self.create_columns = QTreeWidgetItem(self.link_tree)
        self.create_columns.setText(0, "Creating Columns")

        self.visualization = QTreeWidgetItem(self.link_tree)
        self.visualization.setText(0, "Visualizations")

        self.grid_layout.addWidget(self.link_tree, 0, 0, 1, 2)

        self.main_layout.addLayout(self.grid_layout)

        self.dialog.exec()

    def change_help_html(self, item):
        t = item.text(0)
        if t == "Getting Started":
            self.help_browser.setSource(win.context.main_help_html)
            self.get_started.setExpanded(True)
        elif t == "Welcome Screen":
            self.help_browser.setSource(win.context.welcome_help_html)
        elif t == "Table Screen":
            self.help_browser.setSource(win.context.table_help_html)
        elif t == "Data Types":
            self.help_browser.setSource(win.context.datatypes_help_html)
        elif t == "Describe":
            self.help_browser.setSource(win.context.describe_help_html)
        elif t == "Tabs":
            self.help_browser.setSource(win.context.tabs_help_html)
        elif t == "Formatting":
            self.help_browser.setSource(win.context.formatting_help_html)
        elif t == "Sample":
            self.help_browser.setSource(win.context.sample_help_html)
        elif t == "Filter":
            self.help_browser.setSource(win.context.filter_help_html)
        elif t == "Trim":
            self.help_browser.setSource(win.context.trimming_help_html)
        elif t == "Creating Columns":
            self.help_browser.setSource(win.context.columns_help_html)
        elif t == "Visualizations":
            self.help_browser.setSource(win.context.visualizations_help_html)


class VisualizationDialog:
    def __init__(self):
        """
        Creates a new visualization dialog
        """
        super(VisualizationDialog, self).__init__()
        self.dialog = QDialog()
        self.dialog.resize(1400, 600)
        self.v_layout = QVBoxLayout(self.dialog)
        self.h_layout = QHBoxLayout()
        self.type = "Dialog"

        self.menu_layout = QHBoxLayout()
        self.menu_dropdown = QComboBox()
        self.menu_dropdown.addItem("Menu")
        self.menu_dropdown.setItemIcon(0, app_context.menu_icon)
        self.menu_dropdown.addItem("Print")
        self.menu_dropdown.setItemIcon(1, app_context.print_icon)
        self.menu_dropdown.addItem("Change Row Heights")
        self.menu_dropdown.setItemIcon(2, app_context.slider_icon)
        self.menu_dropdown.addItem("Change Colours")
        self.menu_dropdown.setItemIcon(3, app_context.formatting_icon)
        self.menu_dropdown.currentIndexChanged.connect(self.menu_controller)

        self.h_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.menu_layout.addWidget(self.menu_dropdown)
        self.menu_layout.addItem(self.h_spacer)

        self.v_layout.addLayout(self.menu_layout)

        self.containers = []

        font = QFont()
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(50)

        self.add_button = QPushButton("+")
        self.add_button.setFixedSize(75, 75)
        self.add_button.setFont(font)
        self.add_button.clicked.connect(self.add_vis_container)

        self.add_vis_container()

        self.v_layout.addLayout(self.h_layout)

        self.dialog.setWindowTitle("Visualization")

        self.dialog.exec()

    def add_vis_container(self):
        """
        Adds a container which contains a table and the dropdown menus to select the data/visualization
        """
        self.h_layout.removeWidget(self.add_button)
        container = VisualizationContainer(self)
        container.index = len(self.containers)
        self.containers.append(container)
        self.h_layout.addLayout(container.grid_layout)
        self.h_layout.addWidget(self.add_button)

    def menu_controller(self, index):
        """
        Function to run the functions requested by the menu
        :param index: the index of the action requested
        """
        if index == 0:
            pass
        elif index == 1:
            self.print_multiple()
            self.menu_dropdown.setCurrentIndex(0)
        elif index == 2:
            RowHeightDialog(self.containers)
            self.menu_dropdown.setCurrentIndex(0)
        elif index == 3:
            fg_cache = self.containers[0].fg_colour
            bg_cache = self.containers[0].bg_colour
            VisualizationColourDialog(self.containers)
            if self.containers[0].fg_colour != fg_cache or self.containers[0].bg_colour != bg_cache:
                for container in self.containers:
                    container.refresh()
            self.menu_dropdown.setCurrentIndex(0)

    def print_multiple(self):
        """
        Prints the data from all of the vis_tables to a single pixmap
        """
        if len(self.containers) > 0:
            images = []
            width = 0
            height = 0
            filepath, _ = QFileDialog.getSaveFileName(None, "Print Visualizations", "", "Image Files (*.png)")
            if len(filepath) > 0:
                if ".PNG" not in filepath.upper():
                    filepath = "{fp}.png".format(fp=filepath)
                for container in self.containers:
                    image = container.get_pixmap()
                    width += image.width()
                    height = image.height() if image.height() > height else height
                    images.append(image)
                final_image = QPixmap(width, height)
                x = 0
                painter = QPainter(final_image)
                for image in images:
                    painter.drawPixmap(x, 0, image)
                    x += image.width()
                del painter
                final_image.save(filepath, "PNG", -1)


# noinspection PyArgumentList
class VisualizationContainer:
    def __init__(self, parent):
        """
        Creates a container that holds the visualization table and its dropdown buttons
        :param dialog:
        """
        super(VisualizationContainer, self).__init__()
        self.parent = parent
        self.grid_layout = QGridLayout()
        self.type = "Container"
        self.row_height = 75
        self.show_bottom_axis = True
        self.show_left_axis = True
        self.bins = 10
        self.fg_colour = QtGui.QColor(0, 0, 255)
        self.bg_colour = QtGui.QColor(255, 255, 255)

        self.vis_list = ["Distribution Histogram", "Whole Value Distribution", "First Value Distribution",
                         "Box and Whisker w/ Outliers", "Box and Whisker No Outliers", "Line", "% Complete", "% Missing", "% Unique", "Value Lengths", "Character Type"]

        self.tab_dropdown = QComboBox()
        self.tab_dropdown.addItem("Select Tab...")
        for i in range(len(win.data_tabs)):
            name = win.data_tabs.widget(i).name
            self.tab_dropdown.addItem(name)
        self.tab_dropdown.currentIndexChanged.connect(self.change_tab)

        self.vis_dropdown = QComboBox()
        self.vis_dropdown.addItem("Select Visualization...")
        for item in self.vis_list:
            self.vis_dropdown.addItem(item)
        self.vis_dropdown.currentIndexChanged.connect(self.vis_controller)
        self.vis_dropdown.setEnabled(False)

        self.menu_dropdown = QComboBox()
        self.menu_dropdown.addItem("Visualization Menu")
        self.menu_dropdown.setItemIcon(0, app_context.menu_icon)

        self.menu_dropdown.addItem("Print")
        self.menu_dropdown.setItemIcon(1, app_context.print_icon)

        self.menu_dropdown.addItem("Histogram Bins")
        self.menu_dropdown.setItemIcon(2, app_context.filter_icon)

        self.menu_dropdown.addItem("Change Row Height")
        self.menu_dropdown.setItemIcon(3, app_context.slider_icon)

        self.menu_dropdown.addItem("Change Colours")
        self.menu_dropdown.setItemIcon(4, app_context.formatting_icon)

        self.menu_dropdown.addItem("Set Axis Ranges")
        self.menu_dropdown.setItemIcon(5, app_context.bar_chart_icon)

        self.menu_dropdown.setEnabled(False)
        self.menu_dropdown.currentIndexChanged.connect(self.menu_controller)

        self.grid_layout.addWidget(self.vis_dropdown, 0, 1, 1, 1)

        self.grid_layout.addWidget(self.tab_dropdown, 0, 0, 1, 1)

        self.grid_layout.addWidget(self.menu_dropdown, 0, 2, 1, 1)

        self.table = QTableWidget()
        self.grid_layout.addWidget(self.table, 2, 0, 1, 3)

    def change_tab(self, index):
        """
        Processes to take place when the tab is changed in the dropdown
        :param index: The index of the item in the tab dropdown
        """
        if index == 0:
            self.vis_dropdown.setEnabled(False)
            self.menu_dropdown.setEnabled(False)
            if self.table.columnCount() > 0:
                for col in reversed(range(self.table.columnCount())):
                    self.table.removeColumn(col)
            if self.table.rowCount() > 0:
                for row in reversed(range(self.table.rowCount())):
                    self.table.removeRow(row)
        else:
            self.vis_dropdown.setEnabled(True)
            self.menu_dropdown.setEnabled(True)
            if self.table.columnCount() > 0:
                for col in reversed(range(self.table.columnCount())):
                    self.table.removeColumn(col)
            if self.table.rowCount() > 0:
                for row in reversed(range(self.table.rowCount())):
                    self.table.removeRow(row)
            self.tab = win.data_tabs.widget(index - 1)
            self.setup_table()

    def setup_table(self):
        """
        Function to set up the visualization table
        """
        self.table.insertRow(0)
        self.table.verticalHeader().setDefaultSectionSize(self.row_height)
        self.table.setColumnWidth(0, 200)
        self.table.verticalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
        self.table.verticalHeader().doubleClicked.connect(self.vertical_header_context_menu)
        self.table.verticalHeader().customContextMenuRequested.connect(self.vertical_header_context_menu)

        self.table.setRowCount(len(self.tab.data.columns))
        for i, col in enumerate(self.tab.data.columns):
            self.table.setVerticalHeaderItem(i, QTableWidgetItem(str(col)))
            self.table.verticalHeaderItem(i).setTextAlignment(Qt.AlignCenter)

        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        #  494 is the minimum size that the buttons in the container will resize to
        self.table.horizontalHeader().setDefaultSectionSize(494 - (self.table.verticalHeader().sectionSize(0) + 15))

    def vertical_header_context_menu(self, position):
        """
        Context menu for the visualization table
        :param position: The position to place the context menu
        """
        items = self.table.selectedIndexes()
        if len(items) > 0:
            menu = QMenu()
            del_vis = menu.addAction("Delete Visualization")
            show_l_axis = menu.addAction("Toggle Left Axis")
            show_b_axis = menu.addAction("Toggle Bottom Axis")
            if self.vis_dropdown.currentText() == "Character Type":
                show_b_axis.setVisible(False)
                show_l_axis.setVisible(False)
            else:
                show_b_axis.setVisible(True)
                show_l_axis.setVisible(True)
            position.setX(position.x() - self.table.verticalHeader().size().width())
            action = menu.exec(self.table.viewport().mapToGlobal(position))
            if action == del_vis:
                self.table.setCellWidget(items[0].row(), 0, None)
            if action == show_b_axis:
                self.show_bottom_axis = not self.show_bottom_axis
                for item in items:
                    self.table.cellWidget(item.row(), 0).showAxis("bottom", show=self.show_bottom_axis)
            if action == show_l_axis:
                self.show_left_axis = not self.show_left_axis
                for item in items:
                    self.table.cellWidget(item.row(), 0).showAxis("left", show=self.show_left_axis)

    def menu_controller(self, index):
        """
        Function to run the functions requested by the menu
        :param index: the index of the action requested
        """
        if index == 0:
            pass
        elif index == 1:
            self.print_table()
            self.menu_dropdown.setCurrentIndex(0)
        elif index == 2:
            HistogramBinsDialog(self, len(self.tab.data.index))
            self.menu_dropdown.setCurrentIndex(0)
        elif index == 3:
            RowHeightDialog([self])
            self.menu_dropdown.setCurrentIndex(0)
        elif index == 4:
            fg_cache = self.fg_colour
            bg_cache = self.bg_colour
            VisualizationColourDialog([self])
            if self.fg_colour != fg_cache or self.bg_colour != bg_cache:
                self.refresh()
            self.menu_dropdown.setCurrentIndex(0)
        elif index == 5:
            VisualizationSetAxisDialog(self)
            self.menu_dropdown.setCurrentIndex(0)

    def vis_controller(self, index):
        """
        Function to control which visualizations are created on the table
        :param index: The index of the selection in the dropdown menu
        """
        self.show_bottom_axis = True
        self.show_left_axis = True
        if index != 0 and self.table.columnCount() == 0:
            self.table.insertColumn(0)
        if index == 0:
            self.table.removeColumn(0)
        if index == 1:
            self.distribution()
        if index == 2:
            self.bar("whole")
        if index == 3:
            self.bar("first")
        if index == 4:
            self.box_and_whisker("Outlier")
        if index == 5:
            self.box_and_whisker("None")
        if index == 6:
            self.line()
        if index == 7:
            self.bars_and_dots("complete")
        if index == 8:
            self.bars_and_dots("missing")
        if index == 9:
            self.bars_and_dots("unique")
        if index == 10:
            self.value_lengths()
        if index == 11:
            self.character_type()

    def get_pixmap(self):
        """
        Returns a pixmap image of the table and its contents
        :return: Pixmap containing image of the table
        """
        pre_size = self.parent.dialog.geometry()
        width = self.table.verticalHeader().width() + 4
        for i in range(self.table.columnCount()):
            width += self.table.columnWidth(i)
        height = self.table.horizontalHeader().height() + 4
        for i in range(self.table.rowCount()):
            height += self.table.rowHeight(i)
        self.parent.dialog.setGeometry(0, 0, (8 * width), (8 * height))
        self.image = QPixmap(QSize(width, height))
        self.table.render(self.image)
        self.parent.dialog.setGeometry(pre_size)
        return self.image

    def print_table(self):
        """
        Gets a pixmap image of the table and saves it to file
        """
        pre_size = self.parent.dialog.geometry()
        width = self.table.verticalHeader().width() + 4
        for i in range(self.table.columnCount()):
            width += self.table.columnWidth(i)
        height = self.table.horizontalHeader().height() + 4
        for i in range(self.table.rowCount()):
            height += self.table.rowHeight(i)
        filepath, _ = QFileDialog.getSaveFileName(None, "Print Visualization", "", "Image Files (*.png)")
        if len(filepath) > 0:
            self.parent.dialog.setGeometry(0, 0, (4 * width), (4 * height))
            self.image = QPixmap(QSize(width, height))
            self.table.render(self.image)
            self.image.save(filepath, "PNG", -1)
            self.parent.dialog.setGeometry(pre_size)

    def refresh(self):
        """
        Calls the visualization again, used when the colour has been changed
        """
        self.vis_controller(self.vis_dropdown.currentIndex())

    def bars_and_dots(self, style):
        """
        Function to print a bar and dots style visualization of completeness or uniqueness
        :param style: complete, missing, or unique represent the different styles of bar and dots visualization
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if style == "complete":
            string = "{tab}  -  % Complete".format(tab=str(self.tab_dropdown.currentText()))
            self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        elif style == "missing":
            string = "{tab}  -  % Missing".format(tab=str(self.tab_dropdown.currentText()))
            self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        elif style == "unique":
            string = "{tab}  -  % Unique".format(tab=str(self.tab_dropdown.currentText()))
            self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        brush = pg.mkBrush(self.fg_colour)
        for i, name in enumerate(self.tab.data.columns):
            data_col = self.tab.data[name]
            length = len(data_col.index)
            missing = pd.isna(data_col).sum()
            unique = length - data_col.nunique()
            complete = length - missing
            percentage = 0
            if style == "complete":
                percentage = (1 - (missing / length)) * 100
            elif style == "missing":
                percentage = (1 - (complete / length)) * 100
            elif style == "unique":
                percentage = (1 - (unique / length)) * 100
            #  Perceptual discontinuity
            if 5 >= percentage > 0:
                percentage = 5
            if 95 <= percentage < 100:
                percentage = 95
            graph = pg.PlotWidget()
            graph.setMenuEnabled(False)
            graph.setMouseEnabled(x=False, y=False)
            graph.plotItem.getAxis('left').setStyle(showValues=False)
            graph.setBackground(self.bg_colour)
            graph.showGrid(x=True, y=True)
            graph.setMouseEnabled(x=False, y=False)
            graph.setXRange(0, 100, padding=0.035)
            graph.setYRange(-5, 15, padding=0)
            if percentage == 0 or percentage == 100:
                #  plot a symbol
                plt = pg.GraphItem()
                if percentage == 0:
                    pos = [[0, 5]]
                    symbol_brush = (255, 0, 0)
                    symbol_pen = (0, 0, 0)
                    symbol = ['o']
                else:
                    pos = [[100, 5]]
                    symbol_brush = (255, 0, 0)
                    symbol_pen = self.fg_colour
                    symbol = ['x']
                plt.setData(pos=pos, symbolBrush=symbol_brush, symbolPen=symbol_pen, size=10, symbol=symbol)
                graph.addItem(plt)
                self.table.setCellWidget(i, 0, graph)
            if 5 <= percentage <= 95:
                #  plot bar
                plt = pg.BarGraphItem(x=[0 + (percentage / 2)], height=[10], width=percentage, brush=brush)
                graph.addItem(plt)
                self.table.setCellWidget(i, 0, graph)
        QApplication.restoreOverrideCursor()

    def line(self):
        """
        Function to print a line visualization of the data
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        string = "{tab}  -  Line".format(tab=str(self.tab_dropdown.currentText()))
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        for i, name in enumerate(self.tab.data.columns):
            data_col = self.tab.data[name]
            if str(data_col.dtype) == "datetime64[ns]":
                data_col = data_col.dt.year
            graph = pg.PlotWidget()
            graph.setMenuEnabled(False)
            graph.setMouseEnabled(x=False, y=False)
            graph.showGrid(x=True, y=True)
            graph.setBackground(self.bg_colour)
            if data_col.isnull().sum() != len(data_col):
                data_col.dropna(inplace=True)
                if str(data_col.dtype) not in ["Object", "object", "string", "String"]:
                    try:
                        plt = graph.plot(data_col, pen=self.fg_colour)
                        graph.addItem(plt)
                    except (TypeError, ValueError, KeyError):
                        pass
            else:
                error = "Column {col_name} is all NaN values, cannot create a line visualization".format(
                    col_name=name)
                warning_dialog(error)
            self.table.setCellWidget(i, 0, graph)
        QApplication.restoreOverrideCursor()

    def bar(self, var):
        """
        Function to print a distribution visualization of the data
        :param var: whole or first digit
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        string = "{tab}  -  Distribution".format(tab=str(self.tab_dropdown.currentText()))
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        brush = pg.mkBrush(self.fg_colour)
        for i, name in enumerate(self.tab.data.columns):
            data_col = self.tab.data[name]
            if str(data_col.dtype) == "datetime64[ns]":
                data_col = data_col.dt.year if var == "whole" else data_col.dt.dayofyear
            graph = pg.PlotWidget()
            graph.setMenuEnabled(False)
            graph.setMouseEnabled(x=False, y=False)
            graph.showGrid(x=False, y=True)
            graph.setBackground(self.bg_colour)
            if data_col.isnull().sum() != len(data_col):
                data_col.dropna(inplace=True)
                if var == "whole":
                    count = data_col.value_counts()
                    if len(count) < self.bins:
                        plt = pg.BarGraphItem(x=range(len(count)), height=count, width=1, brush=brush)
                        ax = graph.getAxis("bottom")
                        ticks = dict(enumerate(count.iloc[0:self.bins].index.astype(str).to_list()))
                        ax.setTicks([ticks.items()])
                    else:
                        plt = pg.BarGraphItem(x=range(self.bins), height=count, width=1, brush=brush)
                        ax = graph.getAxis("bottom")
                        ticks = dict(enumerate(count.iloc[0:self.bins].index.astype(str).to_list()))
                        ax.setTicks([ticks.items()])
                    graph.addItem(plt)
                    self.table.setCellWidget(i, 0, graph)
                else:
                    first_digit = data_col.astype(str).str[0]
                    count = first_digit.value_counts()
                    if len(count) < self.bins:
                        plt = pg.BarGraphItem(x=range(len(count)), height=count, width=1, brush=brush)
                    else:
                        plt = pg.BarGraphItem(x=range(self.bins), height=count, width=1, brush=brush)
                    ax = graph.getAxis("bottom")
                    ticks = dict(enumerate(count.iloc[0:self.bins].index.astype(str).to_list()))
                    ax.setTicks([ticks.items()])
                    graph.addItem(plt)
                    graph.setXRange(0, self.bins)
                    self.table.setCellWidget(i, 0, graph)
            else:
                error = "Column {col_name} is all NaN values, cannot create a distribution graph".format(col_name=name)
                warning_dialog(error)
        QApplication.restoreOverrideCursor()

    def distribution(self):
        """
        Function to print a distribution visualization of the data
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        string = "{tab}  -  Distribution".format(tab=str(self.tab_dropdown.currentText()))
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        brush = pg.mkBrush(self.fg_colour)
        for i, name in enumerate(self.tab.data.columns):
            data_col = self.tab.data[name]
            if str(data_col.dtype) == "datetime64[ns]":
                data_col = data_col.dt.year
            graph = pg.PlotWidget()
            graph.setMenuEnabled(False)
            graph.setMouseEnabled(x=False, y=False)
            graph.showGrid(x=False, y=True)
            graph.setBackground(self.bg_colour)
            if data_col.isnull().sum() != len(data_col):
                data_col.dropna(inplace=True)
                try:
                    count, division = histogram(data_col, bins=self.bins)
                    plt = graph.plot(division, count, stepMode=True, fillLevel=0, brush=brush)
                    graph.addItem(plt)
                except (TypeError, ValueError):
                    pass
            else:
                error = "Column {col_name} is all NaN values, cannot create a distribution histogram".format(
                    col_name=name)
                warning_dialog(error)
            self.table.setCellWidget(i, 0, graph)
        QApplication.restoreOverrideCursor()

    def character_type(self):
        """
        Function to print a character type visualization of the data
        A, 0, ., -, /, +, !, *, #, ], \
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        string = "{tab}  -  Character Type".format(tab=str(self.tab_dropdown.currentText()))
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        for i, name in enumerate(self.tab.data.columns):
            data_col = self.tab.data[name]
            #          [A,     0,     .,     -,     /,     +,     !,     *,     #,     ]]
            contains = [False, False, False, False, False, False, False, False, False, False]
            for j in data_col:
                j = str(j)
                for k in j:
                    if k in ascii_letters:
                        contains[0] = True
                    if k in digits:
                        contains[1] = True
                    if k == '.':
                        contains[2] = True
                    if k == '-':
                        contains[3] = True
                    if k == '/':
                        contains[4] = True
                    if k == '+':
                        contains[5] = True
                    if k == '!':
                        contains[6] = True
                    if k == '*':
                        contains[7] = True
                    if k == '#':
                        contains[8] = True
                    if k in ['[', ']', '(', ')']:
                        contains[9] = True
            char_table = CharacterTypeTable()
            for k in range(char_table.columnCount()):
                if contains[k]:
                    char_table.item(0, k).setBackground(self.fg_colour)
                    char_table.item(0, k).setForeground(self.bg_colour)
                else:
                    char_table.item(0, k).setBackground(self.bg_colour)
                    char_table.item(0, k).setForeground(self.bg_colour)
            char_table.setRowHeight(0, self.row_height)
            self.table.setCellWidget(i, 0, char_table)
        QApplication.restoreOverrideCursor()

    def box_and_whisker(self, mode):
        """
        Function to print box and whisker plots for all of the variables
        :param mode: "Outlier" or "None" for whether to show outliers
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        string = "{tab}  -  Box and Whisker Distribution".format(tab=str(self.tab_dropdown.currentText()))
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        for i, col in enumerate(self.tab.data.columns):
            data = self.tab.data[col]
            if str(data.dtype) == "datetime64[ns]":
                data = data.dt.year
            elif str(data.dtype) == "timedelta64[ns]":
                data = data.dt.days
            describe = data.describe()
            describe_dtype = describe.dtype
            describe = describe.to_list()
            if str(describe_dtype) not in ["object", "datetime64[ns]", "string"]:
                graph = pg.PlotWidget()
                graph.setMenuEnabled(False)
                graph.setMouseEnabled(x=False, y=False)
                graph.getAxis('left').setStyle(showValues=False)
                graph.showGrid(y=True)
                try:
                    outlier_range = 1.5 * (describe[6] - describe[4])
                    high_outliers = pd.Series(dtype='float64')
                    low_outliers = pd.Series(dtype='float64')
                    minimum = describe[3]
                    maximum = describe[7]
                    if minimum < describe[4] - outlier_range:
                        minimum = describe[4] - outlier_range
                        low_outliers = data[data < minimum]
                    if maximum > describe[6] + outlier_range:
                        maximum = describe[6] + outlier_range
                        high_outliers = data[data > maximum]
                    width = describe[7] - describe[3]
                    #  [minimum, 25%, 50%, 75%, maximum, low_outliers, high_outliers]
                    if mode == "Outlier":
                        plot = BoxAndWhiskerPlot(
                            [minimum, describe[4], describe[5], describe[6], maximum, low_outliers, high_outliers, width],
                            self.fg_colour)
                    else:
                        plot = BoxAndWhiskerPlotNoOutliers(
                            [describe[3], describe[4], describe[5], describe[6], describe[7]], self.fg_colour)
                    graph.addItem(plot)
                except (ValueError, IndexError):
                    #  Catch a specific pandas error
                    pass
                graph.setBackground(self.bg_colour)
                self.table.setCellWidget(i, 0, graph)
            else:
                self.table.setCellWidget(i, 0, None)
        QApplication.restoreOverrideCursor()

    def value_lengths(self):
        """
        Value length visualization
        """
        QApplication.setOverrideCursor(Qt.WaitCursor)
        string = "{tab}  -  Value Lengths".format(tab=str(self.tab_dropdown.currentText()))
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem(string, 0))
        graphs = []
        maximum = 0
        for i, name in enumerate(self.tab.data.columns):
            data = self.tab.data[name].dropna()
            lengths = []
            if data.empty:
                lengths = [0, 0]
            else:
                for j in data:
                    length = len(str(j))
                    if length not in lengths:
                        lengths.append(length)
            local_maximum = max(lengths)
            plot = ValueLengthPlot([min(lengths), local_maximum], self.fg_colour)
            if local_maximum > maximum:
                maximum = local_maximum
            graph = pg.PlotWidget()
            graph.setMenuEnabled(False)
            graph.setMouseEnabled(x=False, y=False)
            graph.showGrid(x=True, y=True)
            if min(lengths) == local_maximum:
                graph.plot(x=[lengths[0]], y=[3.5], pen=None, symbol='o', symbolBrush=pg.mkColor(255, 0, 0))
            graph.getAxis('left').setStyle(showValues=False)
            graph.showGrid(y=True)
            graph.addItem(plot)
            graph.setBackground(self.bg_colour)
            graphs.append(graph)
            self.table.setCellWidget(i, 0, graph)
        for graph in graphs:
            graph.setXRange(-0.1, maximum + 0.5, padding=0)
            graph.setYRange(0, 7, padding=0)
        QApplication.restoreOverrideCursor()


class BoxAndWhiskerPlot(pg.GraphicsObject):
    def __init__(self, data, fg_colour):
        """
        Create a box and whisker plot
        :param data: The plotting data. Must have min, 25%, 50%, 75%, max, low_outliers, high_outliers, width
        """
        super(BoxAndWhiskerPlot, self).__init__()
        self.data = data
        self.fg_colour = fg_colour
        self.generate_picture()

    def generate_picture(self):
        """
        Pre computes the graph to make the describe dialog run faster
        self.data = [minimum, d25, d50, d75, maximum, low_outliers, high_outliers, width]
        """
        self.picture = QtGui.QPicture()
        p = QtGui.QPainter(self.picture)
        p.setPen(pg.mkPen(self.fg_colour))
        if self.fg_colour.red() > self.fg_colour.blue() and self.fg_colour.red() > self.fg_colour.green():
            p.setBrush(pg.mkBrush(color=(self.fg_colour.red(), self.fg_colour.red() * 0.7, self.fg_colour.red() * 0.7)))
        if self.fg_colour.blue() > self.fg_colour.red() and self.fg_colour.blue() > self.fg_colour.green():
            p.setBrush(
                pg.mkBrush(color=(self.fg_colour.blue() * 0.7, self.fg_colour.blue() * 0.7, self.fg_colour.blue())))
        if self.fg_colour.green() > self.fg_colour.red() and self.fg_colour.green() > self.fg_colour.blue():
            p.setBrush(
                pg.mkBrush(color=(self.fg_colour.green() * 0.7, self.fg_colour.green(), self.fg_colour.green() * 0.7)))
        if self.data[0] != self.data[4]:
            p.drawLine(QtCore.QPointF(self.data[0], 3.5), QtCore.QPointF(self.data[4], 3.5))
            p.drawLine(QtCore.QPointF(self.data[0], 1), QtCore.QPointF(self.data[0], 6))
            p.drawLine(QtCore.QPointF(self.data[4], 1), QtCore.QPointF(self.data[4], 6))
            p.drawLine(QtCore.QPointF(self.data[1], 1), QtCore.QPointF(self.data[1], 6))
            p.drawLine(QtCore.QPointF(self.data[3], 1), QtCore.QPointF(self.data[3], 6))
            p.drawLine(QtCore.QPointF(self.data[1], 1), QtCore.QPointF(self.data[3], 1))
            p.drawLine(QtCore.QPointF(self.data[1], 6), QtCore.QPointF(self.data[3], 6))
            p.drawRect(QtCore.QRectF(self.data[1], 1., self.data[3] - self.data[1], 5.))
            p.drawLine(QtCore.QPointF(self.data[2], 1), QtCore.QPointF(self.data[2], 6))
            p.setPen(pg.mkPen(color=(0, 0, 0)))
            p.setBrush(pg.mkBrush(color=(255, 0, 0)))
            if not self.data[5].empty:
                for i in range(len(self.data[5].index)):
                    p.drawEllipse(QtCore.QPointF(self.data[5].iloc[i], 3.5), self.data[7] / 100, 0.2)
            if not self.data[6].empty:
                for i in range(len(self.data[6].index)):
                    p.drawEllipse(QtCore.QPointF(self.data[6].iloc[i], 3.5), self.data[7] / 100, 0.2)

    def paint(self, p, *args):
        """
        Draws the picture to the area
        :param p: The painter
        :param args: other arguments
        """
        p.drawPicture(0, 0, self.picture)

    def boundingRect(self):
        """
        Function to define the area to draw on
        :return: The QRectF representing the area to draw on
        """
        return QtCore.QRectF(self.picture.boundingRect())


class BoxAndWhiskerPlotNoOutliers(pg.GraphicsObject):
    def __init__(self, data, fg_colour):
        """
        Create a box and whisker plot
        :param data: The plotting data. Must have min, 25%, 50%, 75%, max
        """
        super(BoxAndWhiskerPlotNoOutliers, self).__init__()
        self.data = data
        self.fg_colour = fg_colour
        self.generate_picture()

    def generate_picture(self):
        """
        Pre computes the graph to make the describe dialog run faster
        self.data = [minimum, d25, d50, d75, maximum]
        """
        self.picture = QtGui.QPicture()
        p = QtGui.QPainter(self.picture)
        p.setPen(pg.mkPen(self.fg_colour))
        if self.fg_colour.red() > self.fg_colour.blue() and self.fg_colour.red() > self.fg_colour.green():
            p.setBrush(pg.mkBrush(color=(self.fg_colour.red(), self.fg_colour.red() * 0.7, self.fg_colour.red() * 0.7)))
        if self.fg_colour.blue() > self.fg_colour.red() and self.fg_colour.blue() > self.fg_colour.green():
            p.setBrush(
                pg.mkBrush(color=(self.fg_colour.blue() * 0.7, self.fg_colour.blue() * 0.7, self.fg_colour.blue())))
        if self.fg_colour.green() > self.fg_colour.red() and self.fg_colour.green() > self.fg_colour.blue():
            p.setBrush(
                pg.mkBrush(color=(self.fg_colour.green() * 0.7, self.fg_colour.green(), self.fg_colour.green() * 0.7)))
        if self.data[0] != self.data[4]:
            p.drawLine(QtCore.QPointF(self.data[0], 3.5), QtCore.QPointF(self.data[4], 3.5))
            p.drawLine(QtCore.QPointF(self.data[0], 1), QtCore.QPointF(self.data[0], 6))
            p.drawLine(QtCore.QPointF(self.data[4], 1), QtCore.QPointF(self.data[4], 6))
            p.drawLine(QtCore.QPointF(self.data[1], 1), QtCore.QPointF(self.data[1], 6))
            p.drawLine(QtCore.QPointF(self.data[3], 1), QtCore.QPointF(self.data[3], 6))
            p.drawLine(QtCore.QPointF(self.data[1], 1), QtCore.QPointF(self.data[3], 1))
            p.drawLine(QtCore.QPointF(self.data[1], 6), QtCore.QPointF(self.data[3], 6))
            p.drawRect(QtCore.QRectF(self.data[1], 1., self.data[3] - self.data[1], 5.))
            p.drawLine(QtCore.QPointF(self.data[2], 1), QtCore.QPointF(self.data[2], 6))
            p.setPen(pg.mkPen(color=(0, 0, 0)))
            p.setBrush(pg.mkBrush(color=(255, 0, 0)))

    def paint(self, p, *args):
        """
        Draws the picture to the area
        :param p: The painter
        :param args: other arguments
        """
        p.drawPicture(0, 0, self.picture)

    def boundingRect(self):
        """
        Function to define the area to draw on
        :return: The QRectF representing the area to draw on
        """
        return QtCore.QRectF(self.picture.boundingRect())


class ValueLengthPlot(pg.GraphicsObject):
    def __init__(self, data, fg_colour):
        """
        Create a plot for value lengths
        :param data: The plotting data. Must have min and max
        :param fg_colour: The foreground colour to draw the plot in
        """
        super(ValueLengthPlot, self).__init__()
        self.data = data
        self.fg_colour = fg_colour
        self.generate_picture()

    def generate_picture(self):
        """
        Pre computes the graph to make the visualization faster
        self.data = [minimum, maximum]
        """
        self.picture = QtGui.QPicture()
        p = QtGui.QPainter(self.picture)
        p.setPen(pg.mkPen(self.fg_colour))
        if self.data[0] != self.data[1]:
            p.drawLine(QtCore.QPointF(self.data[0], 3.5), QtCore.QPointF(self.data[1], 3.5))
            p.drawLine(QtCore.QPointF(self.data[0], 1), QtCore.QPointF(self.data[0], 6))
            p.drawLine(QtCore.QPointF(self.data[1], 1), QtCore.QPointF(self.data[1], 6))

    def paint(self, p, *args):
        """
        Draws the picture to the area
        :param p: The painter
        :param args: other arguments
        """
        p.drawPicture(0, 0, self.picture)

    def boundingRect(self):
        """
        Function to define the area to draw on
        :return: The QRectF representing the area to draw on
        """
        return QtCore.QRectF(self.picture.boundingRect())


class CharacterTypeTable(QTableWidget):
    def __init__(self):
        """
        Initialise a new table for the character type visualization
        """
        super(CharacterTypeTable, self).__init__()
        black = QtGui.QColor(0, 0, 0, 255)
        self.setRowCount(1)
        self.setColumnCount(10)
        self.verticalHeader().setDefaultSectionSize(60)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.font = QFont('Helvetica', 14, 30)
        self.setFont(self.font)

        self.setItem(0, 0, QTableWidgetItem("A"))
        self.item(0, 0).setTextAlignment(Qt.AlignCenter)
        self.item(0, 0).setToolTip("Alpha")
        self.item(0, 0).setForeground(black)

        self.setItem(0, 1, QTableWidgetItem("0"))
        self.item(0, 1).setTextAlignment(Qt.AlignCenter)
        self.item(0, 1).setToolTip("Numeric")
        self.item(0, 1).setForeground(black)

        self.setItem(0, 2, QTableWidgetItem("."))
        self.item(0, 2).setTextAlignment(Qt.AlignCenter)
        self.item(0, 2).setToolTip("Full Stop")
        self.item(0, 2).setForeground(black)

        self.setItem(0, 3, QTableWidgetItem("-"))
        self.item(0, 3).setTextAlignment(Qt.AlignCenter)
        self.item(0, 3).setToolTip("Hyphen")
        self.item(0, 3).setForeground(black)

        self.setItem(0, 4, QTableWidgetItem("/"))
        self.item(0, 4).setTextAlignment(Qt.AlignCenter)
        self.item(0, 4).setToolTip("Slashes")
        self.item(0, 4).setForeground(black)

        self.setItem(0, 5, QTableWidgetItem("+"))
        self.item(0, 5).setTextAlignment(Qt.AlignCenter)
        self.item(0, 5).setToolTip("Plus")
        self.item(0, 5).setForeground(black)

        self.setItem(0, 6, QTableWidgetItem("!"))
        self.item(0, 6).setTextAlignment(Qt.AlignCenter)
        self.item(0, 6).setToolTip("Exclamation Mark")
        self.item(0, 6).setForeground(black)

        self.setItem(0, 7, QTableWidgetItem("*"))
        self.item(0, 7).setTextAlignment(Qt.AlignCenter)
        self.item(0, 7).setToolTip("Asterisk")
        self.item(0, 7).setForeground(black)

        self.setItem(0, 8, QTableWidgetItem("#"))
        self.item(0, 8).setTextAlignment(Qt.AlignCenter)
        self.item(0, 8).setToolTip("Hash")
        self.item(0, 8).setForeground(black)

        self.setItem(0, 9, QTableWidgetItem("[ ]"))
        self.item(0, 9).setTextAlignment(Qt.AlignCenter)
        self.item(0, 9).setToolTip("Brackets")
        self.item(0, 9).setForeground(black)

        self.horizontalHeader().setDefaultSectionSize(41)
        self.setShowGrid(False)
        self.horizontalHeader().setVisible(False)
        self.verticalHeader().setVisible(False)
        self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)


class RowHeightDialog:
    def __init__(self, containers):
        """
        Creates a new row height dialog
        :param parent: the tab or dialog that it is being called from
        """
        super(RowHeightDialog, self).__init__()
        self.dialog = QDialog()
        self.containers = containers
        self.parent = containers[0]
        self.dialog.setFixedSize(430, 260)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 410, 240)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_1, 0, 0, 1, 1)

        self.label = QLabel("Row Height")
        self.label.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.label, 1, 0, 1, 1)

        self.input = QSpinBox()
        self.input.setMinimum(1)
        self.input.setMaximum(1000)
        self.input.setValue(self.parent.table.rowHeight(0))
        self.grid_layout.addWidget(self.input, 2, 0, 1, 2)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_2, 3, 0, 1, 1)

        self.ok_button = QPushButton("OK")
        self.ok_button.clicked.connect(self.apply)
        self.grid_layout.addWidget(self.ok_button, 4, 0, 1, 1)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.grid_layout.addWidget(self.cancel_button, 4, 1, 1, 1)

        self.dialog.setWindowTitle("Change Row Height")

        self.dialog.exec()

    def apply(self):
        """
        Update the row height
        """
        for container in self.containers:
            container.row_height = self.input.value()
            for row in range(container.table.rowCount()):
                container.table.setRowHeight(row, self.input.value())
                if container.vis_dropdown.currentText() == "Character Type":
                    try:
                        container.table.cellWidget(row, 0).setRowHeight(0, self.input.value())
                    except AttributeError:
                        #  Character Type table has been deleted
                        pass
        self.dialog.accept()


class HistogramBinsDialog:
    def __init__(self, vis, maximum):
        """
        Initialises a dialog to change the number of histogram bins
        :param vis: The visualization container
        :param maximum: The length of the index of the dataframe. Not advised to actually increase bin number to this value.
        """
        super(HistogramBinsDialog, self).__init__()
        self.parent = vis
        self.dialog = QDialog()
        self.dialog.setFixedSize(400, 220)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 380, 200)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_1, 0, 0, 1, 1)

        self.input = QSpinBox(self.grid_layout_widget)
        self.input.setMaximum(maximum)
        self.input.setValue(self.parent.bins)
        self.grid_layout.addWidget(self.input, 1, 0, 1, 2)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_2, 2, 0, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("Ok")
        self.ok_button.setIcon(app_context.ok_icon)
        self.grid_layout.addWidget(self.ok_button, 3, 0, 1, 1)
        self.ok_button.clicked.connect(self.apply)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.grid_layout.addWidget(self.cancel_button, 3, 1, 1, 1)
        self.cancel_button.clicked.connect(self.dialog.reject)

        self.dialog.setWindowTitle("Change Histogram Bin Number")

        self.dialog.exec()

    def apply(self):
        """
        Adjusts the number of bins on the histogram visualizations
        """
        self.parent.bins = self.input.value()
        if self.parent.vis_dropdown.currentText() == "Distribution Histogram":
            self.parent.distribution()
        self.dialog.accept()


class VisualizationColourDialog:
    def __init__(self, containers):
        """
        Initialises a colour selection dialog for the visualizations
        :param parent: Visualization Container
        """
        super(VisualizationColourDialog, self).__init__()
        self.containers = containers
        self.parent = containers[0]
        self.dialog = QDialog()
        self.dialog.setFixedSize(350, 150)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 330, 130)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_1, 0, 0, 1, 1)

        self.f_label = QLabel("Foreground Colour")
        self.f_label.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.f_label, 1, 0, 1, 1)

        self.b_label = QLabel("Background Colour")
        self.b_label.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.b_label, 1, 1, 1, 1)

        self.foreground_colour_select = QPushButton(self.grid_layout_widget)
        self.grid_layout.addWidget(self.foreground_colour_select, 2, 0, 1, 1)
        width = self.foreground_colour_select.size().width() - 10
        height = self.foreground_colour_select.size().height() - 10
        self.fg_pixmap = QPixmap(width, height)
        self.fg_pixmap.fill(self.parent.fg_colour)
        self.fg_icon = QIcon(self.fg_pixmap)
        self.foreground_colour_select.setIcon(self.fg_icon)
        self.foreground_colour_select.setIconSize(QSize(width, height))
        self.foreground_colour_select.clicked.connect(self.set_fg_colour)

        self.background_colour_select = QPushButton(self.grid_layout_widget)
        self.grid_layout.addWidget(self.background_colour_select, 2, 1, 1, 1)
        width = self.background_colour_select.size().width() - 10
        height = self.background_colour_select.size().height() - 10
        self.bg_pixmap = QPixmap(width, height)
        self.bg_pixmap.fill(self.parent.bg_colour)
        self.bg_icon = QIcon(self.bg_pixmap)
        self.background_colour_select.setIcon(self.bg_icon)
        self.background_colour_select.setIconSize(QSize(width, height))
        self.background_colour_select.clicked.connect(self.set_bg_colour)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_2, 3, 0, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.dialog.accept)
        self.grid_layout.addWidget(self.ok_button, 4, 0, 1, 1)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.grid_layout.addWidget(self.cancel_button, 4, 1, 1, 1)

        self.dialog.setWindowTitle("Change Visualization Colours")
        self.dialog.exec()

    def set_bg_colour(self):
        """
        Sets the background colour of the parent to the selected value
        Also updates the colour on the dialog buttons
        """
        dialog = QColorDialog()
        colour = dialog.getColor()
        self.bg_pixmap.fill(colour)
        self.bg_icon = QIcon(self.bg_pixmap)
        self.background_colour_select.setIcon(self.bg_icon)
        for container in self.containers:
            container.bg_colour = colour

    def set_fg_colour(self):
        """
        Sets the foreground colour of the parent to the selected value
        Also updates the colour on the dialog buttons
        """
        dialog = QColorDialog()
        colour = dialog.getColor()
        self.fg_pixmap.fill(colour)
        self.fg_icon = QIcon(self.fg_pixmap)
        self.foreground_colour_select.setIcon(self.fg_icon)
        for container in self.containers:
            container.fg_colour = colour


class VisualizationSetAxisDialog:
    def __init__(self, parent):
        """
        Dialog used to change the visualization axis ranges
        :param parent: where it's being called from
        """
        super(VisualizationSetAxisDialog, self).__init__()
        self.parent = parent
        self.data = self.parent.tab.data
        self.dialog = QDialog()
        self.min_x = 0
        self.max_x = 0
        self.min_y = 0
        self.max_y = 0
        self.dialog.resize(600, 550)
        self.main_layout = QVBoxLayout(self.dialog)
        self.grid_layout = QGridLayout()

        self.table = QTableWidget(self.dialog)
        self.grid_layout.addWidget(self.table, 0, 0, 9, 1)

        self.selected_variables = QTextBrowser(self.dialog)
        self.selected_variables.setText("")
        self.selected_variables.setAlignment(Qt.AlignCenter)
        self.grid_layout.addWidget(self.selected_variables, 0, 1, 1, 2)

        self.v_spacer_1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_1, 1, 1, 1, 1)

        self.xstart_label = QLabel("X Start")
        self.grid_layout.addWidget(self.xstart_label, 2, 1, 1, 1)

        self.xend_label = QLabel("X End")
        self.grid_layout.addWidget(self.xend_label, 2, 2, 1, 1)

        self.xstart_in = QLineEdit(self.dialog)
        self.grid_layout.addWidget(self.xstart_in, 3, 1, 1, 1)
        self.xstart_in.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.xend_in = QLineEdit(self.dialog)
        self.grid_layout.addWidget(self.xend_in, 3, 2, 1, 1)
        self.xend_in.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.ystart_label = QLabel("Y Start")
        self.grid_layout.addWidget(self.ystart_label, 4, 1, 1, 1)

        self.yend_label = QLabel("Y End")
        self.grid_layout.addWidget(self.yend_label, 4, 2, 1, 1)

        self.ystart_in = QLineEdit(self.dialog)
        self.grid_layout.addWidget(self.ystart_in, 5, 1, 1, 1)
        self.ystart_in.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.yend_in = QLineEdit(self.dialog)
        self.grid_layout.addWidget(self.yend_in, 5, 2, 1, 1)
        self.yend_in.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.apply_button = QPushButton(self.dialog)
        self.apply_button.setText("Apply")
        self.grid_layout.addWidget(self.apply_button, 6, 1, 1, 2)
        self.apply_button.clicked.connect(self.apply)

        self.v_spacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer_2, 7, 1, 1, 1)

        self.ok_button = QPushButton(self.dialog)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.dialog.accept)
        self.grid_layout.addWidget(self.ok_button, 8, 1, 1, 1)

        self.cancel_button = QPushButton(self.dialog)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.grid_layout.addWidget(self.cancel_button, 8, 2, 1, 1)

        self.main_layout.addLayout(self.grid_layout)

        self.dialog.setWindowTitle("Set Visualization Axis")

        self.setup_table()

        self.dialog.exec()

    def apply(self):
        """
        Applies the changes to the axis ranges
        """
        rows = []
        for item in self.table.selectedIndexes():
            if item.row() not in rows:
                rows.append(item.row())
        for row in rows:
            try:
                self.parent.table.cellWidget(row, 0).setXRange(min=float(self.xstart_in.text()),
                                                               max=float(self.xend_in.text()), padding=0)
                self.parent.table.cellWidget(row, 0).setYRange(min=float(self.ystart_in.text()),
                                                               max=float(self.yend_in.text()), padding=0)
                x_str = "{start} : {end}".format(start=self.xstart_in.text(), end=self.xend_in.text())
                y_str = "{start} : {end}".format(start=self.ystart_in.text(), end=self.yend_in.text())
                self.table.setItem(row, 1, QTableWidgetItem(x_str))
                self.table.setItem(row, 2, QTableWidgetItem(y_str))
                self.table.item(row, 1).setTextAlignment(Qt.AlignCenter)
                self.table.item(row, 2).setTextAlignment(Qt.AlignCenter)
            except (ValueError, AttributeError) as e:
                warning_dialog(str(e))
                break

    def setup_table(self):
        """
        Prints the variable names and dtypes to the dtype_table
        """
        for i in range(3):
            self.table.insertColumn(0)
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem("Variable"))
        self.table.setHorizontalHeaderItem(1, QTableWidgetItem("X"))
        self.table.setHorizontalHeaderItem(2, QTableWidgetItem("Y"))
        self.table.setRowHeight(0, 50)

        self.print_table()

        self.table.verticalHeader().setVisible(False)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.table.resizeColumnsToContents()
        self.table.itemSelectionChanged.connect(self.selection_changed)

    def print_table(self):
        """
        Fills the table with the variable/column names and their respective dtypes
        """
        for i, name in enumerate(self.data.columns):
            self.table.insertRow(i)
            self.table.setItem(i, 0, QTableWidgetItem(str(name)))
            try:
                xax = self.parent.table.cellWidget(i, 0).getAxis("bottom").range
                yax = self.parent.table.cellWidget(i, 0).getAxis("left").range
                if xax[0] < self.min_x:
                    self.min_x = xax[0]
                if xax[1] < self.max_x:
                    self.max_x = xax[1]
                if yax[0] < self.min_y:
                    self.min_y = yax[0]
                if yax[1] < self.max_y:
                    self.max_y = yax[1]
                x_str = "{start} : {end}".format(start=round(xax[0], 3), end=round(xax[1], 3))
                y_str = "{start} : {end}".format(start=round(yax[0], 3), end=round(yax[1], 3))
                self.table.setItem(i, 1, QTableWidgetItem(x_str))
                self.table.setItem(i, 2, QTableWidgetItem(y_str))
                self.table.item(i, 1).setTextAlignment(Qt.AlignCenter)
                self.table.item(i, 2).setTextAlignment(Qt.AlignCenter)
            except AttributeError:
                #  If there is no plot in the cell
                self.table.setItem(i, 1, QTableWidgetItem("0:0"))
                self.table.setItem(i, 2, QTableWidgetItem("0:0"))
            self.table.item(i, 0).setTextAlignment(Qt.AlignCenter)
        self.table.resizeColumnsToContents()

    def selection_changed(self):
        """
        Changes the label to the variable name
        """
        selected = []
        for i in self.table.selectedItems():
            row = i.row()
            text = self.table.item(row, 0).text()
            if text not in selected:
                selected.append(text)
                self.xstart_in.setText(self.table.item(row, 1).text().split(":")[0])
                self.xend_in.setText(self.table.item(row, 1).text().split(":")[1])
                self.ystart_in.setText(self.table.item(row, 2).text().split(":")[0])
                self.yend_in.setText(self.table.item(row, 2).text().split(":")[1])
        self.selected_variables.setText(str(selected))


class RenameTab:
    def __init__(self, tab):
        """
        Creates the Renaming dialog
        :param tab: The tab to rename
        """
        super(RenameTab, self).__init__()
        self.tab = tab
        self.dialog = QDialog()
        self.dialog.setFixedSize(350, 150)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 330, 130)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer, 0, 1, 1, 1)

        self.input = QLineEdit(self.grid_layout_widget)
        self.input.setPlaceholderText("New Name")
        self.grid_layout.addWidget(self.input, 1, 0, 1, 4)

        self.v_spacer2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer2, 2, 1, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)
        self.grid_layout.addWidget(self.ok_button, 3, 0, 1, 2)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.grid_layout.addWidget(self.cancel_button, 3, 2, 1, 2)

        self.dialog.setWindowTitle("Rename Tab")

        self.dialog.exec()

    def apply(self):
        """
        Applies the renaming operation to the desired tab
        """
        self.tab.name = self.input.text()
        win.data_tabs.setTabText(win.data_tabs.currentIndex(), self.tab.name)
        self.dialog.accept()


class RenameColumn:
    def __init__(self, tab, column):
        """
        Creates the Renaming dialog
        :param tab: The tab that the column belongs to
        :param column: The column to rename
        """
        super(RenameColumn, self).__init__()
        self.dialog = QDialog()
        self.tab = tab
        self.column = column
        self.dialog.setFixedSize(350, 150)
        self.grid_layout_widget = QWidget(self.dialog)
        self.grid_layout_widget.setGeometry(10, 10, 330, 130)
        self.grid_layout = QGridLayout(self.grid_layout_widget)
        self.grid_layout.setContentsMargins(0, 0, 0, 0)

        self.v_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer, 0, 1, 1, 1)

        self.input = QLineEdit(self.grid_layout_widget)
        self.input.setPlaceholderText("New Name")
        self.grid_layout.addWidget(self.input, 1, 0, 1, 4)

        self.v_spacer2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_layout.addItem(self.v_spacer2, 2, 1, 1, 1)

        self.ok_button = QPushButton(self.grid_layout_widget)
        self.ok_button.setText("OK")
        self.ok_button.setIcon(app_context.ok_icon)
        self.ok_button.clicked.connect(self.apply)
        self.grid_layout.addWidget(self.ok_button, 3, 0, 1, 2)

        self.cancel_button = QPushButton(self.grid_layout_widget)
        self.cancel_button.setText("Cancel")
        self.cancel_button.setIcon(app_context.cancel_icon)
        self.cancel_button.clicked.connect(self.dialog.reject)
        self.grid_layout.addWidget(self.cancel_button, 3, 2, 1, 2)

        self.dialog.setWindowTitle("Rename Column")

        self.dialog.exec()

    def apply(self):
        """
        Applies the renaming operation to the desired Column
        """
        if self.input.text() not in self.tab.data.columns:
            self.tab.data = self.tab.data.rename(columns={self.column: self.input.text()})
            formatting = self.tab.column_formatting[self.column]
            self.tab.column_formatting[self.input.text()] = formatting
            self.tab.reset_print_area()
            self.dialog.accept()
        else:
            warning_dialog("Column Name {name} Already Exists".format(name=self.input.text()))


def warning_dialog(error):
    """
    Custom popup warning dialog box
    :param error: a string error message to display on the QMessageBox()
    """
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Warning)
    msg_box.setText(error)
    msg_box.setWindowTitle("Warning")
    msg_box.setStandardButtons(QMessageBox.Ok)
    msg_box.buttonClicked.connect(msg_box.close)
    msg_box.exec()


class AppContext(ApplicationContext):
    def __init__(self):
        """
        Initialise the application
        """
        super(AppContext, self).__init__()
        self.win = MainWindow(self)

    def run(self):
        """
        Run the application
        :return: Errors
        """
        self.win.show()
        return self.app.exec_()

    def win(self):
        """
        Create a mainwindow instance
        :return: MainWindow
        """
        return MainWindow(self)

    @cached_property
    def quit_img(self):
        """
        Function to return the Quit image for the start buttons
        :return: QIcon representing the quit button image
        """
        return QIcon(self.get_resource("images/Quit.png"))

    @cached_property
    def open_img(self):
        """
        Function to return the Open File image for the start buttons
        :return: QIcon representing the open file button image
        """
        return QIcon(self.get_resource("images/OpenFile.png"))

    @cached_property
    def help_img(self):
        """
        Function to return the help image for the start buttons
        :return: QIcon representing the help button image
        """
        return QIcon(self.get_resource("images/Help.png"))

    @cached_property
    def bar_chart_icon(self):
        """
        Function to get the icon for the visualization menu button
        :return: QIcon representing the bar chart icon
        """
        return QIcon(self.get_resource("Icons/bar-chart-2.svg"))

    @cached_property
    def descending_icon(self):
        """
        Function to get the icon for columns sorted by descending
        :return: QIcon representing the descending icon
        """
        return QIcon(self.get_resource("Icons/chevron-down.svg"))

    @cached_property
    def ascending_icon(self):
        """
        Function to get the icon for columns sorted by ascending
        :return: QIcon representing the ascending icon
        """
        return QIcon(self.get_resource("Icons/chevron-up.svg"))

    @cached_property
    def menu_icon(self):
        """
        Function to get the icon for the menu button
        :return: QIcon representing the menu icon
        """
        return QIcon(self.get_resource("Icons/menu.svg"))

    @cached_property
    def describe_icon(self):
        """
        Function to get the icon for the describe menu button
        :return: QIcon representing the describe icon
        """
        return QIcon(self.get_resource("Icons/eye.svg"))

    @cached_property
    def search_icon(self):
        """
        Function to get the icon for the search button
        :return: QIcon representing the search icon
        """
        return QIcon(self.get_resource("Icons/search.svg"))

    @cached_property
    def filter_icon(self):
        """
        Function to get the icon for the filter menu button
        :return: QIcon representing the filter icon
        """
        return QIcon(self.get_resource("Icons/filter.svg"))

    @cached_property
    def rename_icon(self):
        """
        Function to get the icon for the rename menu button
        :return: QIcon representing the rename icon
        """
        return QIcon(self.get_resource("Icons/edit-3.svg"))

    @cached_property
    def layers_icon(self):
        """
        Function to get the icon for the compare/combine menu button
        :return: QIcon representing the layers icon
        """
        return QIcon(self.get_resource("Icons/layers.svg"))

    @cached_property
    def trim_icon(self):
        """
        Function to get the scissors icon used for the trim menu button
        :return: QIcon representing the trim icon
        """
        return QIcon(self.get_resource("Icons/scissors.svg"))

    @cached_property
    def clipboard_icon(self):
        """
        Function to get the clipboard icon used for the sample menu item
        :return: QIcon representing the sample icon
        """
        return QIcon(self.get_resource("Icons/clipboard.svg"))

    @cached_property
    def convert_icon(self):
        """
        Function to get the icon for the convert data types menu button
        :return: QIcon representing the convert icon
        """
        return QIcon(self.get_resource("Icons/shuffle.svg"))

    @cached_property
    def formatting_icon(self):
        """
        Function to get the icon for the formatting menu button
        :return: QIcon representing the settins icon
        """
        return QIcon(self.get_resource("Icons/settings.svg"))

    @cached_property
    def print_icon(self):
        """
        Function to get the icon for the print visualization menu button
        :return: QIcon representing the print icon
        """
        return QIcon(self.get_resource("Icons/printer.svg"))

    @cached_property
    def slider_icon(self):
        """
        Function to get the icon for the alternate settings button
        :return: QIcon representing the sliders icon
        """
        return QIcon(self.get_resource("Icons/sliders.svg"))

    @cached_property
    def ok_icon(self):
        """
        Function to get the icon for the ok buttons
        :return: QIcon representing the ok icon
        """
        return QIcon(self.get_resource("Icons/check.svg"))

    @cached_property
    def cancel_icon(self):
        """
        Function to get the icon for the cancel buttons
        :return: QIcon representing the cancel icon
        """
        return QIcon(self.get_resource("Icons/x.svg"))

    @cached_property
    def refresh_icon(self):
        """
        Function to get the refresh icon
        :return: QIcon representing the refresh icon
        """
        return QIcon(self.get_resource("Icons/refresh-cw.svg"))

    @cached_property
    def copy_icon(self):
        """
        Function to get the copy icon
        :return: QIcon representing the copy icon
        """
        return QIcon(self.get_resource("Icons/copy.svg"))

    @cached_property
    def main_help_html(self):
        """
        Function to get the html file for the getting started help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/MainHelp.html"))

    @cached_property
    def filter_help_html(self):
        """
        Function to get the html file for the filtering help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Filtering.html"))

    @cached_property
    def datatypes_help_html(self):
        """
        Function to get the html file for the datatypes help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/DataTypes.html"))

    @cached_property
    def columns_help_html(self):
        """
        Function to get the html file for the columns help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Columns.html"))

    @cached_property
    def trimming_help_html(self):
        """
        Function to get the html file for the trimming help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Trimming.html"))

    @cached_property
    def visualizations_help_html(self):
        """
        Function to get the html file for the visualizations help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Visualizations.html"))

    @cached_property
    def welcome_help_html(self):
        """
        Function to get the html file for the welcome screen help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Welcome.html"))

    @cached_property
    def table_help_html(self):
        """
        Function to get the html file for the table screen help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Table.html"))

    @cached_property
    def describe_help_html(self):
        """
        Function to get the html file for the describe dialog help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Describe.html"))

    @cached_property
    def tabs_help_html(self):
        """
        Function to get the html file for the tab help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Tabs.html"))

    @cached_property
    def sample_help_html(self):
        """
        Function to get the html file for the sampling dialiog help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Sampling.html"))

    @cached_property
    def formatting_help_html(self):
        """
        Function to get the html file for the formatting help documentation
        :return: a URL to the help html file that can be loaded into a QTextBrowser
        """
        return QtCore.QUrl(self.get_resource("html/Formatting.html"))


if __name__ == '__main__':
    app_context = AppContext()
    win = app_context.win
    win.enable_menu_items(False)
    win.show()
    exit_code = app_context.run()
    exit(exit_code)
